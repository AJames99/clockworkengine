package com.clockwork.scene;

import com.clockwork.bounding.*;
import com.clockwork.material.*;
import com.clockwork.math.*;
import static org.easymock.EasyMock.*;
import org.junit.*;
import static org.junit.Assert.*;

public class GeometryTest {

    @Test
    public void testConstructorNameNull() {
        GeometrySpatial geom = new GeometrySpatial(null);
        assertNull(geom.getName());
    }

    @Test
    public void testConstructorName() {
        GeometrySpatial geom = new GeometrySpatial("TestGeometry");
        assertEquals("TestGeometry", geom.getName());
    }

    @Test(expected = NullPointerException.class)
    public void testConstructorNameMeshNullMesh() {
        GeometrySpatial geom = new GeometrySpatial("TestGeometry", null);
    }

    @Test
    public void testConstructorNameMesh() {
        MeshGeometry m = new MeshGeometry();
        GeometrySpatial geom = new GeometrySpatial("TestGeometry", m);
        assertEquals("TestGeometry", geom.getName());
        assertEquals(m, geom.getMesh());
    }

    @Test(expected = IllegalStateException.class)
    public void testSetLodLevelMeshLodZero() {
        MeshGeometry m = new MeshGeometry();
        GeometrySpatial geom = new GeometrySpatial("TestGeometry", m);
        geom.setLodLevel(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetLodLevelLodLessZero() {
        MeshGeometry m = new MeshGeometry();
        VertexBuffer lodLevels = new VertexBuffer(VertexBuffer.Type.Size);
        m.setLodLevels(new VertexBuffer[]{lodLevels});
        GeometrySpatial geom = new GeometrySpatial("TestGeometry", m);
        geom.setLodLevel(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetLodLevelLodGreaterMeshLod() {
        MeshGeometry m = new MeshGeometry();
        VertexBuffer lodLevel = new VertexBuffer(VertexBuffer.Type.Size);
        m.setLodLevels(new VertexBuffer[]{lodLevel});
        GeometrySpatial geom = new GeometrySpatial("TestGeometry", m);
        geom.setLodLevel(5);
    }

    @Test
    public void testSetLodLevel() {
        MeshGeometry m = new MeshGeometry();
        VertexBuffer lodLevel = new VertexBuffer(VertexBuffer.Type.Size);
        m.setLodLevels(new VertexBuffer[]{lodLevel, lodLevel, lodLevel});
        GeometrySpatial geom = new GeometrySpatial("TestGeometry", m);
        geom.setLodLevel(2);
        assertEquals(2, geom.getLodLevel());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetMeshNull() {
        GeometrySpatial geom = new GeometrySpatial();
        geom.setMesh(null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetMeshBatched() {
        MeshGeometry m = new MeshGeometry();
        GeometrySpatial geom = new GeometrySpatial();
        BatchNode bn = new BatchNode();
        geom.batch(bn, 1);
        geom.setMesh(m);
    }

    @Test
    public void testSetMesh() {
        MeshGeometry m = new MeshGeometry();
        GeometrySpatial geom = new GeometrySpatial();
        geom.setMesh(m);
        assertEquals(m, geom.getMesh());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSetMaterialBatched() {
        Material m = new Material();
        GeometrySpatial geom = new GeometrySpatial();
        BatchNode bn = new BatchNode();
        geom.batch(bn, 1);
        geom.setMaterial(m);
    }

    @Test
    public void testSetMaterial() {
        Material m = new Material();
        GeometrySpatial geom = new GeometrySpatial();
        geom.setMaterial(m);
        assertEquals(m, geom.getMaterial());
    }

    @Test
    public void testUpdateModelBound() {
        MeshGeometry mockedMesh = createMock(MeshGeometry.class);
        mockedMesh.updateBound();
        expectLastCall();

        replay(mockedMesh);
        GeometrySpatial geom = new GeometrySpatial();
        geom.setMesh(mockedMesh);
        geom.updateModelBound();

        verify(mockedMesh);
    }

    @Test(expected = NullPointerException.class)
    public void testUpdateWorldBoundNoMesh() {
        GeometrySpatial geom = new GeometrySpatial();
        geom.updateWorldBound();
    }

    @Test
    public void testUpdateWorlBoundNoBoundingVolume() {
        MeshGeometry mockedMesh = createMock(MeshGeometry.class);
        expect(mockedMesh.getBound()).andReturn(null);
        replay(mockedMesh);

        GeometrySpatial geom = new GeometrySpatial();
        geom.setMesh(mockedMesh);
        geom.updateWorldBound();

        verify(mockedMesh);
    }

    @Test
    public void testUpdateWorlBoundIgnoreTransform() {
        MeshGeometry mockedMesh = createMock(MeshGeometry.class);
        BoundingVolume mockedBoundingVolume = createMock(BoundingVolume.class);
        expect(mockedMesh.getBound()).andReturn(mockedBoundingVolume).times(2);
        expect(mockedBoundingVolume.clone(null)).andReturn(null);
        replay(mockedMesh, mockedBoundingVolume);

        GeometrySpatial geom = new GeometrySpatial();
        geom.setMesh(mockedMesh);
        geom.setIgnoreTransform(true);
        geom.updateWorldBound();

        verify(mockedMesh, mockedBoundingVolume);
    }

    @Test
    public void testUpdateWorlBoundTransform() {
        MeshGeometry mockedMesh = createMock(MeshGeometry.class);
        BoundingVolume mockedBoundingVolume = createMock(BoundingVolume.class);
        expect(mockedMesh.getBound()).andReturn(mockedBoundingVolume).times(2);
        expect(mockedBoundingVolume.transform(anyObject(Transform.class), same((BoundingVolume) null))).andReturn(null);
        replay(mockedMesh, mockedBoundingVolume);

        GeometrySpatial geom = new GeometrySpatial();
        geom.setMesh(mockedMesh);
        geom.setIgnoreTransform(false);
        geom.updateWorldBound();

        verify(mockedMesh, mockedBoundingVolume);
    }
}
