package com.clockwork.demo;

import com.clockwork.bullet.collision.shapes.CollisionShape;
import com.clockwork.bullet.control.RigidBodyControl;
import com.clockwork.math.Quaternion;
import com.clockwork.math.Vector3f;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.SceneNode;
import com.clockwork.scene.Spatial;
// placeholder for now, not actually fixed.
public class MissileFixed extends RigidBodyControl {

    GeometrySpatial rocketModel;
    SceneNode rocketSpatial;
    Vector3f forward = new Vector3f();
    Spatial target;
    
    public MissileFixed(CollisionShape shape, float mass) {
        super(shape, mass);
    }
    
    public void setTarget(SceneNode target){
        this.target = target;
    }
    
    private Vector3f getForward(){
        forward.set(getPhysicsRotation().mult(Vector3f.UNIT_Z));
        return forward;
    }
    
    private Vector3f getForward(Vector3f store){
        store.set(getForward());
        return store;
    }
    
    private void thrustForward(){
        this.applyCentralForce(getForward().mult(50));
    }
    
    private void pointAtTarget(){
        Quaternion missile_rotation = new Quaternion();
        missile_rotation.lookAt(forward, Vector3f.UNIT_Y);
    }
    
    @Override
    public void update(float tpf){
        
    }
}
