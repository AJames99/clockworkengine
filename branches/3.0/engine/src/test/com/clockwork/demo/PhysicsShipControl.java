package com.clockwork.demo;

import com.clockwork.assets.AssetHandler;
import com.clockwork.audio.*;
import com.clockwork.audio.AudioSource.Status;
import com.clockwork.bullet.PhysicsSpace;
import com.clockwork.bullet.PhysicsTickListener;
import com.clockwork.bullet.collision.*;
import com.clockwork.bullet.collision.shapes.*;
import com.clockwork.bullet.control.*;
import com.clockwork.bullet.joints.*;
import com.clockwork.bullet.objects.*;
import com.clockwork.effect.*;
import com.clockwork.effect.ParticleMesh.Type;
import com.clockwork.export.InputCapsule;
import com.clockwork.export.CWExporter;
import com.clockwork.export.CWImporter;
import com.clockwork.export.OutputCapsule;
import com.clockwork.demo.Missile.TeamTag;
import com.clockwork.material.*;
import com.clockwork.math.*;
import com.clockwork.renderer.RenderingManager;
import com.clockwork.renderer.Viewport;
import com.clockwork.renderer.queue.RenderOrder.Bucket;
import com.clockwork.scene.*;
import com.clockwork.scene.control.Control;
import com.clockwork.scene.shape.*;
import java.io.IOException;
import java.util.*;

// See bottom of document for how ship mechanics work.
// TODO Easy:
//      - Boosting should be additive to fuel use - punishes boosting to take-off and after gliding, but rewards in-combat maneouvres.
//      - Add defense cannons - fire 3 shots in burst? Maybe automatic? each allied-turret on ship/terrain targets a specific missile. 
//          Cannons should only target the same missile if there's no other missiles in range.
//      - Add hangar for ship, with opening doors.
//      - Add cargo that ship goes to pick up and drop into bay next to ship hangar - have doors close after cargo is dropped in.
//          Missiles can damage cargo clamps, causing cargo to drop after too much dmg. Heat too high will damage clamps gradually, impacts can shake cargo loose.
//      - Make turrets heat-dependent tracking - Longer launch  range, faster, quicker turning, less aim-noise when ship is hot.
//
// TODO add particle inheritance
// TODO make missile tracking heat-dependent


// Weapons should heat ship up, maybe use fuel?
//      Big weapons should damage our ship when we fire, and add a large reverse-force
//            ________    ___    ____
//          /  __   __| /  _  \ |  _ \
//     _____> \  | |   |   _   ||    /_________________________________
//   /  ______/  |_|   |__| |__||__|\________________________________  \
// /  /                                                              \  \
// |  |       "Not to worry, we are still flying half a ship"         |  |
//  \  \___________________________    __     ___     ______     _____/  /
//   \__________________________   |  |  |  /  _  \  |   _  \  /   _____/
//                              |  |/\|  | |   _   | |   _  /  >  \
//                              \___/\__/  |__| |__| |__|\__\ |___/
//                               
public class PhysicsShipControl extends PhysicsRigidBody implements PhysicsControl, PhysicsTickListener {

    protected float rollValue;
    protected float yawValue;
    protected float pitchValue;
    protected float thrustValue;
    protected float verticalThrustValue;
    protected boolean boosting = false;
    protected float boostTime = 0;
    protected Vector3f forward = new Vector3f();

    protected float engineStrength = 200f;//200f; //100f; //150f; //Divided up amongst 'modules' (Main thruster, lat/vert thrusters, maneouvering thrusters)
    // Used to calculate distribution of engineStrength
    // Angular thrusters are affected a lot less by the distribution, but still drain as much as they should.
    protected final int mainThrusterRatio = 6;
    protected final int verticalThrusterRatio = 3;
    protected final int angularThrusterRatio = 2;
    protected float mainThrusterSupply = 1;
    protected float verticalThrusterSupply = 1;
    protected float angularThrusterSupply = 1;
    protected float angularThrusterSupplyAdjusted = 1;

    protected float thrusterWarmup = 0f; //Increases from 0 to 1 over 1.5 seconds to give full booster power for all types of linear thrust (vertical, lateral, forward)
    protected float liftAmount = 0f;
    protected float liftFactor = 65f;

    protected float engineHeat = 0f;
    protected final float fuelCapacity = 1000f;// 500f;
    protected final float armourCapacity = 100f;
    protected final float armourDisableThreshold = 10f;
    protected float fuel = fuelCapacity;
    protected float armour = armourCapacity;
    // Fuel usage at max eff.: 1 / second
    // Fuel usage at high heat, low eff: 20 / sec
    protected float fuelRate = 0.1f;
    protected boolean repairing = false;

    protected boolean carryingCargo = false;
    
    float particlesPerSec = 50f;

    private final float defaultVolume = 0.55f;
    // Remove if this isn't the associated test.
    // This is risky, and only done for minor consistency.
    private final static float WATER_HEIGHT = TestFullShip.WATER_HEIGHT;
    private boolean underwater;
    private AudioNode[] thrusterSFXList;
    private AudioNode thrusterSFX;
    private AudioNode hoverSFX;

    protected Spatial spatial;
    protected boolean enabled = true;
    protected PhysicsSpace space = null;
    protected Vector3f tempVect1 = new Vector3f(0, 0, 0);
    protected Vector3f tempVect2 = new Vector3f(0, 0, 0);
    protected Vector3f tempVect3 = new Vector3f(0, 0, 0);
    private AssetHandler assetHandler;

    protected RigidBodyControl cargoHolder;
    
    protected ParticleEmitter[] emitters;

    public PhysicsShipControl() {
    }

    /**
     * Creates a new PhysicsNode with the supplied collision shape
     *
     * @param shape
     */
    public PhysicsShipControl(CollisionShape shape) {
        super(shape);
    }

    public PhysicsShipControl(CollisionShape shape, float mass) {
        super(shape, mass);
    }

    public Control cloneForSpatial(Spatial spatial) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setSpatial(Spatial spatial) {
        this.spatial = spatial;
        setUserObject(spatial);
        if (spatial == null) {
            return;
        }
        setPhysicsLocation(spatial.getWorldTranslation());
        setPhysicsRotation(spatial.getWorldRotation().toRotationMatrix());
    }
    public Spatial getSpatial(){
        return this.spatial;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }
    
    public void setAssetHandler(AssetHandler manager){
        assetHandler = manager;
    }

    private void calculateRatios(boolean main, boolean vertical, boolean angular) {
        int sum = ((main ? mainThrusterRatio : 0) + (vertical ? verticalThrusterRatio : 0) + (angular ? angularThrusterRatio : 0));
        mainThrusterSupply = mainThrusterRatio * (engineStrength / sum);
        verticalThrusterSupply = verticalThrusterRatio * (engineStrength / sum);
        angularThrusterSupply = angularThrusterRatio * (engineStrength / sum);
    }

    Vector3f up = new Vector3f(0, 1, 0);
    Vector3f right = new Vector3f(1, 0, 0);
    float interval = 0.5f;
    float offset;
    float rayLength = 12;
    
    public void prePhysicsTick(PhysicsSpace space, float f) {
        getForwardVector(forward);
        getSideVector(right);
        getUpVector(up);

        if (this.getAngularVelocity().length() > 0.1f || this.getLinearVelocity().length() > 0.1f) {
            this.setAngularVelocity(this.getAngularVelocity().divide(1.01f));
            //this.setLinearVelocity(this.getLinearVelocity().divide(1.001f));
        }

        calculateRatios(thrustValue != 0, verticalThrustValue != 0, pitchValue != 0 || yawValue != 0 || rollValue != 0);

        //With the usual ratios, angular thrusters are painfull slow when also main-thrusting - this boosts their strength, while keeping their 1:X ratio drain.
        angularThrusterSupplyAdjusted = Math.min(angularThrusterSupply * 1.5f, engineStrength);

        if (canFly()) {
            if (thrustValue != 0) {
                this.applyCentralForce(forward.mult(thrustValue * verticalThrusterSupply * thrusterWarmup));
            }
            // 1 Second into the boost, give a huge continuous push of force
            if(boosting && boostTime > 1f){
                this.applyCentralForce(forward.mult(9000));
            }
            // If travelling too fast upward, don't apply vertical thrust. This restricts vertical thrusters to minor maneouvring and
            // Take-off/landing.
            if (verticalThrustValue != 0 && getLinearVelocity().y < 2) {
                this.applyCentralForce(up.mult(verticalThrustValue * verticalThrusterSupply * thrusterWarmup));
            }
        }
        // If we can fly, rotate as usual. Otherwise, modify limit angular thrusting to a 1/10th of the value.
        if (pitchValue != 0) {
            this.applyTorque(right.mult(pitchValue * angularThrusterSupplyAdjusted * (canFly() ? 1 : 0.1f)));
        }
        if (yawValue != 0) {
            this.applyTorque(up.mult(yawValue * angularThrusterSupplyAdjusted * (canFly() ? 1 : 0.1f)));
        }
        if (rollValue != 0) {
            this.applyTorque(forward.mult(rollValue * angularThrusterSupplyAdjusted * (canFly() ? 1 : 0.1f)));
        }
        
        // Upthrust from wings, but only slight
        // Dot gets the cosine of the angle between the two vectors
        // Clamping that to the 0 to 1 range ensures that when we travel backward, we don't get negative lift.
        // Alternatively, just use the absolute function (Math.Abs()) to force it to be positive
        // This means that 45 degrees forward is the same as 45 degrees backward,
        // and we get full lift going backward, as we would going forward
        // By clamping with the maximum function, we totally eliminate the thrust supplied
        // when travelling more than 90 degrees off course
        // 23/03/2017
        //liftFactor = Math.max(getLinearVelocity().dot(forward), 0);
        liftAmount = getLinearVelocity().dot(forward);
        if (liftAmount > 0) {
            // 50 being a modifiable lift amount. Lift factor is calculated as a 0 to 1 scale of our aerodynamics.
            this.applyCentralForce(up.mult(liftAmount * liftFactor)); //50
        }
        setLinearVelocity(turnShip(getLinearVelocity()));

        if (!carryingCargo) {
            interval += f;
            if (interval > 0.5f) {
                if (getUserObject() instanceof Spatial) {
                    List<PhysicsRayTestResult> rayTest = getPhysicsSpace().rayTest(getPhysicsLocation(), getPhysicsLocation().add(forward.mult(rayLength)));
                    if (rayTest.size() > 0) {
                        PhysicsRayTestResult get = rayTest.get(0);
                        PhysicsCollisionObject collisionObject = get.getCollisionObject();
                        if ("CARGO".equals(((SceneNode) collisionObject.getUserObject()).getName())) {
                            offset = rayTest.get(0).getHitFraction();
                            //if(offset > 0.2f){
                                attachCargo((SceneNode) collisionObject.getUserObject(), offset * rayLength);
                            //}
                            //TODO use GhostControl
                        }
                    }
                }
                interval = 0;
            }
        }
        else {
            this.detachAllCargo();
        }
    }

    public Vector3f getForwardVector() {
        return this.getPhysicsRotation().mult(new Vector3f(0, 0, 1));
    }

    public Vector3f getForwardVector(Vector3f input) {
        input.set(getForwardVector());
        return input;
    }

    public Vector3f getSideVector(Vector3f input) {
        input.set(this.getPhysicsRotation().mult(new Vector3f(1, 0, 0)));
        return input;
    }

    public Vector3f getUpVector() {
        return this.getPhysicsRotation().mult(new Vector3f(0, 1, 0));
    }

    public Vector3f getUpVector(Vector3f input) {
        input.set(getUpVector());
        return input;
    }

    // The below should function properly with a += because
    // Inputs are only 'triggered' on initially press and release.
    // This allows cancelling Q with E, for example.
    // This also means, cancelling a direction with its counterpart, 
    // then releasing the counterpart key will steer in the original direction, 
    // which is the desired effect.
    public void yaw(float direction) {
        // if +ve, go CW
        // if -ve, go ACW
        yawValue += direction;
    }

    public void roll(float direction) {
        // if +ve, go CW
        // if -ve, go ACW
        rollValue += direction;
    }

    public void pitch(float direction) {
        // if +ve, go up
        // if -ve, go down
        pitchValue += direction;
    }

    public void thrust(float amount) {
        thrustValue += amount;
        for (ParticleEmitter emitter : emitters) {
            if ("main".equals(emitter.getName())) {
                emitter.setEmitting(amount > 0);
            }
        }
    }

    public void setThrust(float amount) {
        thrustValue = amount;
        for (ParticleEmitter emitter : emitters) {
            if ("main".equals(emitter.getName())) {
                emitter.setEmitting(amount > 0);
            }
        }
    }
    
    public void setBoost(boolean boosting) {
        this.boosting = boosting;
        for (ParticleEmitter emitter : emitters) {
            if ("boost".equals(emitter.getName())) {
                emitter.setEmitting(boosting);
            }
        }
    }

    public void setVerticalThrust(float amount) {
        verticalThrustValue = amount;
        for (ParticleEmitter emitter : emitters) {
            if ("vertical".equals(emitter.getName())) {
                emitter.setEmitting(amount != 0);
            }
        }

    }
    
    SliderJoint connectingJoint;
    //Point2PointJoint connectingJoint2;
    
    Vector3f upwardVector;    
    public void attachCargo(Spatial cargo, float distance) {
        if (!cargo.getControl(CargoControl.class).isConnected()) {
            // Attach the holder and the cargo to eachother
            
            //upwardVector = new Vector3f(0, 0, -5);
            upwardVector = new Vector3f(0, -5, 0);
            
            cargo.getControl(CargoControl.class).setPhysicsLocation(getPhysicsLocation().add(forward.mult(-5)));
            cargo.getControl(CargoControl.class).setPhysicsRotation(getPhysicsRotation());

            connectingJoint = new SliderJoint(
                    this,
                    cargo.getControl(CargoControl.class),
                    Vector3f.ZERO,//new Vector3f(0,-3,0),
                    Vector3f.ZERO,
                    true
            );
            
            connectingJoint.setUpperLinLimit(0.4f);
            connectingJoint.setLowerLinLimit(-0.4f);
            //connectingJoint.setDampingDirLin(0.5f);

            connectingJoint.setSoftnessDirLin(1);//
            
            //connectingJoint.setDamping(0.5f);
            //connectingJoint.setCollisionBetweenLinkedBodys(true);
            connectingJoint.setCollisionBetweenLinkedBodys(false);

            //connectingJoint2.setCollisionBetweenLinkedBodys(true);

            getPhysicsSpace().add(connectingJoint);
            //getPhysicsSpace().add(connectingJoint2);
            cargo.getControl(CargoControl.class).setConnected(true);
            carryingCargo = true;  
        }
    }
    
    RigidBodyControl[] list;
    public void setupChain(SceneNode rootnode){
        Vector3f vec = new Vector3f(0,1,0);
        Material mat = new Material(assetHandler, "Common/MatDefs/Misc/Unshaded.mdef");
        /*RigidBodyControl[] */list = new RigidBodyControl[10];
        for(int i = 0; i < 10; i++){
            GeometrySpatial geom = new GeometrySpatial("box"+i, new Box(.1f,.1f,.1f));
            RigidBodyControl control = new RigidBodyControl(new BoxCollisionShape(new Vector3f(.1f, .1f, .1f)), 1);
            geom.addControl(control);
            list[i] = control;
            control.setPhysicsLocation(this.getPhysicsLocation().add(new Vector3f(0,i/2,0)));
            geom.setMaterial(mat);
            if(i == 0){
                Point2PointJoint joint = new Point2PointJoint(
                        this,
                        list[i],
                        new Vector3f(0,-1,0),
                        vec
                );
                //joint.setDamping(0.5f);
                joint.setCollisionBetweenLinkedBodys(false);
                getPhysicsSpace().add(joint);
            } else {
                Point2PointJoint joint = new Point2PointJoint(
                        list[i-1],
                        list[i],
                        Vector3f.ZERO,
                        vec
                );
                //joint.setDamping(0.5f);
                joint.setCollisionBetweenLinkedBodys(false);
                getPhysicsSpace().add(joint);
            }
            getPhysicsSpace().add(control);
            rootnode.attachChild(geom);
        }
    }
    
    public void detachAllCargo(){
        //this.removeJoint(connectingJoint);
        //connectingJoint.setTau(0);
        //connectingJoint.destroy();
    }

    // Rotates the velocity as we rotate the ship, but decay some of it - akin to a real plane
    // TODO :
    // Faster velocities should be more preserved
    // The greater the difference in the angle, the worse the retention;
    // this should stop gliding when flat and falling, and instead encourage
    // the pilot to rotate from nose down to flat in order to translate some of the free-fall
    // into horizontal motion
    private final float speedRetention = 0.98f;//0.999f;
    private final float dragEffectiveness = 0.007f;

    public Vector3f turnShip(Vector3f velocity) {
        // Get the difference between the two vectors.
        // The smaller the difference, the closer to 100% of the velocity is conserved.
        // If 90 degrees (flat), then it should preserve <1%
        // If 0 or 180 degrees to the velocity (facing it, or backward) then it should preserve 100%
        // ABS is to ensure that backward and forward give the same velocity preservation
        float difference = Math.abs(velocity.normalize().dot(forward.normalize()));
        // Eliminate tiny differences to 0, resulting in maximum drag
        difference = (Math.abs(difference) < 0.0001f) ? 0 : difference;
        difference *= speedRetention;
        velocity.interpolate(forward.mult(velocity.length() * difference), dragEffectiveness);
        return velocity;
    }
    
    public void fireCannon(){
    
    }
    
    Spatial missile;
    /*public void fireMissile(SceneNode root) {
        Vector3f position = getPhysicsLocation();
        SceneNode target = null;

        class Traverser implements SceneGraphTraverser {

            float minDist = 1000;
            Spatial missile_final;

            @Override
            public void visit(Spatial spatial) {
                Missile missile_control = spatial.getControl(Missile.class);
                if (missile_control != null) {
                    if (missile_control.getPhysicsLocation().distance(position) < minDist && missile_control.tag != TeamTag.ALLIED) {
                        minDist = missile_control.getPhysicsLocation().distance(position);
                        missile_final = spatial;
                    }
                }
            }

            public Spatial getTarget() {
                return missile_final;
            }
        }
        Traverser traverser = new Traverser();

        root.breadthFirstTraversal(traverser);
        if (traverser.getTarget() != null) {
            target = (SceneNode) traverser.getTarget();
        }

        Missile missileInstance = new Missile(missile, assetHandler, target, (SceneNode) spatial, new GeometrySpatial("box", new Box(Vector3f.ZERO, 2, 2, 2)), getPhysicsSpace());
        missileInstance.setPhysicsLocation(getPhysicsLocation().add(getUpVector().mult(5)));
        //missileInstance.setPhysicsRotation(getPhysicsRotation());
        //missileInstance.setLinearVelocity(getLinearVelocity());
        //missileInstance.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_01);
        missileInstance.tag = TeamTag.ENEMY; //allied
        missileInstance.setMissileForce(10f);
    }*/
    
        // Missiles are parented to this Turret object, not turretSpat
    public void fireMissile(SceneNode target) {
        Missile missileInstance = new Missile(missile, assetHandler, null, (SceneNode) spatial, new GeometrySpatial("box", new Box(Vector3f.ZERO, 2,2,2)), getPhysicsSpace());        
        missileInstance.setPhysicsLocation(this.getPhysicsLocation().add(0, 10, 0));
        //missileInstance.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_03);
        missileInstance.tag = TeamTag.ENEMY;
        missileInstance.setMissileForce(100);
        missileInstance.setMaxTime(60f);
    }

    public void physicsTick(PhysicsSpace space, float f) {
    }

    @Override
    public void update(float tpf) {
        if (enabled && spatial != null) {
            getMotionState().applyTransform(spatial);
        }
    }

    public float getFuel() {
        return fuel;
    }

    public float getSpeed() {
        return getLinearVelocity().length();
    }

    public float getArmour() {
        return armour;
    }

    public float getHeat() {
        return engineHeat;
    }

    public boolean isRepairing() {
        return repairing;
    }

    public void setRepairStatus(boolean status) {
        repairing = status;
    }

    // Damages or repairs the ship.
    public void modifyShipArmour(float modifier) {
        armour += modifier;
    }

    public void setupEmitters(AssetHandler assetHandler) {
        //Setting up the engine's emitters
        Material smoke_mat = new Material(assetHandler, "Common/MatDefs/Misc/Particle.mdef");
        smoke_mat.getAdditionalRenderState().setColorWrite(true);
        smoke_mat.getAdditionalRenderState().setDepthTest(true);
        smoke_mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
        smoke_mat.setTexture("Texture", assetHandler.loadTexture("Effects/Smoke/Smoke_faint.png"));

        Material fire_mat = new Material(assetHandler, "Common/MatDefs/Misc/Particle.mdef");
        fire_mat.getAdditionalRenderState().setColorWrite(true);
        fire_mat.getAdditionalRenderState().setDepthTest(true);
        fire_mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.AlphaAdditive);
        fire_mat.setTexture("Texture", assetHandler.loadTexture("Effects/Explosion/flame.png"));

        Material energy_mat = new Material(assetHandler, "Common/MatDefs/Misc/Particle.mdef");
        energy_mat.getAdditionalRenderState().setColorWrite(true);
        energy_mat.getAdditionalRenderState().setDepthTest(true);
        energy_mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.AlphaAdditive);
        energy_mat.setTexture("Texture", assetHandler.loadTexture("Effects/Explosion/shockwave.png"));

        ParticleEmitter emit_vertical_L = new ParticleEmitter("vertical", Type.Triangle, 300);
        emit_vertical_L.setGravity(0, 0, 0);
        emit_vertical_L.getParticleInfluencer().setVelocityVariation(0.01f);
        emit_vertical_L.setStartSize(0.1f);
        emit_vertical_L.setEndSize(0.01f);
        emit_vertical_L.setStartColor(ColorRGBA.White);
        emit_vertical_L.setEndColor(ColorRGBA.Cyan);
        emit_vertical_L.setLowLife(0.1f);
        emit_vertical_L.setHighLife(0.12f);
        emit_vertical_L.getParticleInfluencer().setInitialVelocity(new Vector3f(0, -4f, 0f));
        emit_vertical_L.setMaterial(energy_mat);
        emit_vertical_L.setParticlesPerSec(200);
        emit_vertical_L.setLocalTranslation(4.55f, -1.7f, 2);

        ParticleEmitter emit_vertical_R = new ParticleEmitter("vertical", Type.Triangle, 300);
        emit_vertical_R.setGravity(0, 0, 0);
        emit_vertical_R.getParticleInfluencer().setVelocityVariation(0.01f);
        emit_vertical_R.setStartSize(0.1f);
        emit_vertical_R.setEndSize(0.01f);
        emit_vertical_R.setStartColor(ColorRGBA.White);
        emit_vertical_R.setEndColor(ColorRGBA.Cyan);
        emit_vertical_R.setLowLife(0.1f);
        emit_vertical_R.setHighLife(0.12f);
        emit_vertical_R.getParticleInfluencer().setInitialVelocity(new Vector3f(0, -4f, 0f));
        emit_vertical_R.setMaterial(energy_mat);
        emit_vertical_R.setParticlesPerSec(200);
        emit_vertical_R.setLocalTranslation(-4.55f, -1.7f, 2);

        ParticleEmitter emit_smoke_L = new ParticleEmitter("main", Type.Triangle, 300);
        emit_smoke_L.setGravity(0, 0, 0);
        emit_smoke_L.getParticleInfluencer().setVelocityVariation(0.05f);
        emit_smoke_L.setStartSize(0.4f);
        emit_smoke_L.setEndSize(0.5f);
        emit_smoke_L.setStartColor(ColorRGBA.Brown);
        emit_smoke_L.setEndColor(ColorRGBA.Gray);
        emit_smoke_L.setLowLife(1f);
        emit_smoke_L.setHighLife(1f);
        emit_smoke_L.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0f, -2f));
        emit_smoke_L.setImagesX(15);
        emit_smoke_L.setMaterial(smoke_mat);
        emit_smoke_L.setParticlesPerSec(50);
        emit_smoke_L.setLocalTranslation(2.65f, 1.5f, -6);

        ParticleEmitter emit_smoke_R = new ParticleEmitter("main", Type.Triangle, 300);
        emit_smoke_R.setGravity(0, 0, 0);
        emit_smoke_R.getParticleInfluencer().setVelocityVariation(0.05f);
        emit_smoke_R.setStartSize(0.4f);
        emit_smoke_R.setEndSize(0.5f);
        emit_smoke_R.setStartColor(ColorRGBA.Brown);
        emit_smoke_R.setEndColor(ColorRGBA.Gray);
        emit_smoke_R.setLowLife(1f);
        emit_smoke_R.setHighLife(1f);
        emit_smoke_R.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0f, -2f));
        emit_smoke_R.setImagesX(15);
        emit_smoke_R.setMaterial(smoke_mat);
        emit_smoke_R.setParticlesPerSec(50);
        emit_smoke_R.setLocalTranslation(-2.65f, 1.5f, -6);

        ParticleEmitter emit_boost_L = new ParticleEmitter("boost", Type.Triangle, 300);
        emit_boost_L.setGravity(0, 0, 0);
        emit_boost_L.getParticleInfluencer().setVelocityVariation(1f);
        emit_boost_L.setStartSize(0.15f);
        emit_boost_L.setEndSize(0.05f);
        emit_boost_L.setStartColor(ColorRGBA.Yellow);
        emit_boost_L.setEndColor(ColorRGBA.Orange);
        emit_boost_L.setLowLife(0.05f);
        emit_boost_L.setHighLife(0.2f);
        emit_boost_L.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0f, -2f));
        emit_boost_L.setImagesX(2);
        emit_boost_L.setImagesY(2);
        emit_boost_L.setMaterial(fire_mat);
        emit_boost_L.setParticlesPerSec(400);
        emit_boost_L.setRandomAngle(true);
        emit_boost_L.setLocalTranslation(2.55f, 0.70f, -6);

        ParticleEmitter emit_boost_R = new ParticleEmitter("boost", Type.Triangle, 300);
        emit_boost_R.setGravity(0, 0, 0);
        emit_boost_R.getParticleInfluencer().setVelocityVariation(1f);
        emit_boost_R.setStartSize(0.15f);
        emit_boost_R.setEndSize(0.05f);
        emit_boost_R.setStartColor(ColorRGBA.Yellow);
        emit_boost_R.setEndColor(ColorRGBA.Orange);
        emit_boost_R.setLowLife(0.05f);
        emit_boost_R.setHighLife(0.2f);
        emit_boost_R.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0f, -2f));
        emit_boost_R.setImagesX(2);
        emit_boost_R.setImagesY(2);
        emit_boost_R.setMaterial(fire_mat);
        emit_boost_R.setParticlesPerSec(400);
        emit_boost_R.setRandomAngle(true);
        emit_boost_R.setLocalTranslation(-2.55f, 0.70f, -6);
        
        ParticleEmitter emit_thruster_L = new ParticleEmitter("main", Type.Triangle, 300);
        emit_thruster_L.setGravity(0, 0, 0);
        emit_thruster_L.getParticleInfluencer().setVelocityVariation(0f);
        emit_thruster_L.setStartSize(0.2f);
        emit_thruster_L.setEndSize(0.0f);
        emit_thruster_L.setStartColor(new ColorRGBA(0.88f, 1, 1, 1)); //Light cyan
        emit_thruster_L.setEndColor(new ColorRGBA(0.39f, 0.58f, 0.93f, 1f)); //Cornflower blue
        emit_thruster_L.setLowLife(0.05f);
        emit_thruster_L.setHighLife(0.2f);
        //emit_sparks_R.setShape(new EmitterSphereShape(Vector3f.ZERO, 2f));
        emit_thruster_L.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0f, -1f));
        emit_thruster_L.setImagesX(2);
        emit_thruster_L.setImagesY(2);
        emit_thruster_L.setMaterial(fire_mat);
        emit_thruster_L.setParticlesPerSec(250);
        //emit_sparks_R.setFacingVelocity(true);
        emit_thruster_L.setRandomAngle(true);
        emit_thruster_L.setLocalTranslation(2.55f, 0.70f, -6);
        
        ParticleEmitter emit_thruster_R = new ParticleEmitter("main", Type.Triangle, 300);
        emit_thruster_R.setGravity(0, 0, 0);
        emit_thruster_R.getParticleInfluencer().setVelocityVariation(0f);
        emit_thruster_R.setStartSize(0.2f);
        emit_thruster_R.setEndSize(0.0f);
        emit_thruster_R.setStartColor(new ColorRGBA(0.88f, 1, 1, 1)); //Light cyan
        emit_thruster_R.setEndColor(new ColorRGBA(0.39f, 0.58f, 0.93f, 1f)); //Cornflower blue
        emit_thruster_R.setLowLife(0.05f);
        emit_thruster_R.setHighLife(0.2f);
        //emit_sparks_R.setShape(new EmitterSphereShape(Vector3f.ZERO, 2f));
        emit_thruster_R.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0f, -1f));
        emit_thruster_R.setImagesX(2);
        emit_thruster_R.setImagesY(2);
        emit_thruster_R.setMaterial(fire_mat);
        emit_thruster_R.setParticlesPerSec(250);
        //emit_sparks_R.setFacingVelocity(true);
        emit_thruster_R.setRandomAngle(true);
        emit_thruster_R.setLocalTranslation(-2.55f, 0.70f, -6);

        emitters = new ParticleEmitter[]{emit_vertical_L, emit_vertical_R, emit_smoke_L, emit_smoke_R, emit_thruster_L, emit_thruster_R, emit_boost_L, emit_boost_R};
        for (ParticleEmitter emitter : emitters) {
            emitter.setEmitting(false);
            emitter.setQueueBucket(Bucket.Translucent);
            ((SceneNode) spatial).attachChild(emitter);
        }
    }

    public void setupAudio(AssetHandler assetHandler) {
        thrusterSFXList = new AudioNode[5];

        thrusterSFXList[0] = new AudioNode(assetHandler, "Sound/Elite/thruster_start.wav", false);
        thrusterSFXList[0].setVolume(defaultVolume);
        thrusterSFXList[0].setLooping(false);
        thrusterSFXList[0].setName("Starting");

        thrusterSFXList[1] = new AudioNode(assetHandler, "Sound/Elite/thruster_loop.wav", false);
        thrusterSFXList[1].setVolume(defaultVolume);
        thrusterSFXList[1].setLooping(true);
        thrusterSFXList[1].setName("Looping");

        thrusterSFXList[2] = new AudioNode(assetHandler, "Sound/Elite/thruster_end.wav", false);
        thrusterSFXList[2].setVolume(defaultVolume);
        thrusterSFXList[2].setLooping(false);
        thrusterSFXList[2].setName("Ending");

        thrusterSFXList[3] = new AudioNode(assetHandler, "Sound/Elite/thruster_boost.wav", false);
        thrusterSFXList[3].setVolume(defaultVolume);
        thrusterSFXList[3].setLooping(false);
        thrusterSFXList[3].setName("Boosting");

        thrusterSFXList[4] = new AudioNode(assetHandler, "Sound/Elite/thruster_idle.wav", false);
        thrusterSFXList[4].setVolume(defaultVolume / 32);
        thrusterSFXList[4].setLooping(true);
        thrusterSFXList[4].setName("Idling");
        
        for(AudioNode node : thrusterSFXList){
            node.setPositional(false);
            //node.setRefDistance(10f);
            //node.setMaxDistance(50f);
            node.setReverbEnabled(true);
        }
        
        hoverSFX = new AudioNode(assetHandler, "Sound/vertical_thrust_loop.wav", false);
        hoverSFX.setVolume(0.25f);
        hoverSFX.setPositional(true);
        hoverSFX.setRefDistance(10f);
        hoverSFX.setMaxDistance(50f);
        hoverSFX.setLooping(true);

        thrusterSFX = thrusterSFXList[4];
        //thrusterSFX.play();
        
        ((SceneNode) spatial).attachChild(thrusterSFX);
        ((SceneNode) spatial).attachChild(hoverSFX);
    }

    private void underwaterAudio(boolean submerged) {
        for (Spatial node : ((SceneNode) spatial).getChildren()) {
            if (node instanceof AudioNode) {
                ((AudioNode) node).setDryFilter(submerged ? new LowPassFilter(0.15f * defaultVolume, 0.05f * defaultVolume) : null);
            }
        }
    }

    public void render(RenderingManager rm, Viewport vp) {
    }

    public void setPhysicsSpace(PhysicsSpace space) {
        if (space == null) {
            if (this.space != null) {
                this.space.removeCollisionObject(this);
                this.space.removeTickListener(this);
            }
            this.space = space;
        } else {
            space.addCollisionObject(this);
            space.addTickListener(this);
        }
        this.space = space;
    }

    float consistency;
    float timeSinceLastThrust = 0;

    public enum thrustStatus {
        INCREASING, DECREASING, IDLE, IDLEZERO, BOOST
    };
    private thrustStatus thrustStatusCurrent;

    public void updateShip(float tpf) {
        consistency = tpf * 60;

        // Calculating thruster warm-up rates
        if (thrustValue > 0 || verticalThrustValue > 0) {
            if (thrusterWarmup < 1) {
                if (thrustValue > 0) {
                    // For every second of thrusting forward, add 1 warm-up unit
                    thrusterWarmup += tpf * 1.2f;
                }
                if (verticalThrustValue > 0) {
                    // For every second of thrusting vertically, add 0.5 warm-up units
                    thrusterWarmup += tpf * 1.2f;
                }
                thrustStatusCurrent = thrustStatus.INCREASING;
                // If using both thrusters, powerup is quicker but distribution of engine power is shared over both thrusters.
            }
            else if (thrusterWarmup > 0){
                thrustStatusCurrent = thrustStatus.IDLE;
            }
            timeSinceLastThrust = 0;
        } else {
            // If we thrust at least once every 1 second, don't decay the warm-up at all.
            if (timeSinceLastThrust < 1) {
                timeSinceLastThrust += tpf;
            }
            // If using no thrusters, decrease over time
            if (timeSinceLastThrust >= 1) {
                if (thrusterWarmup > 0) {
                    thrusterWarmup -= tpf * 0.2f;
                    thrustStatusCurrent = thrustStatus.DECREASING;
                } else {
                    thrustStatusCurrent = thrustStatus.IDLEZERO;
                }
            }
        }
        
        if(boosting){
            thrustStatusCurrent = thrustStatus.BOOST;
            thrusterWarmup = 1;
            boostTime+=tpf;
        }
        if(boostTime >= 5){
            setBoost(false);
            boostTime = 0;
            //thrustStatusCurrent = thrustStatus.IDLE;
        }

        // Clamping values
        thrusterWarmup = Math.min(1f, thrusterWarmup);
        thrusterWarmup = Math.max(0f, thrusterWarmup);

        switch (thrustStatusCurrent) {
            case INCREASING: //GOTO Starting if we're not the same sound effect, and if we're either on slowing, idling, or not playing at  all
                if((thrusterSFX.getStatus() != Status.Playing && !"Starting".equals(thrusterSFX.getName())) || "Boosting".equals(thrusterSFX.getName()) || "Idling".equals(thrusterSFX.getName()) || ("Ending".equals(thrusterSFX.getName()) && thrusterWarmup <= 0.05f)){
                    thrusterSFX.stop();
                    thrusterSFX = thrusterSFXList[0];
                    thrusterSFX.play();
                }
                else if ("Ending".equals(thrusterSFX.getName())) {
                    thrusterSFX.stop();
                    thrusterSFX = thrusterSFXList[1];
                    thrusterSFX.play();
                }
                break;

            case DECREASING: //GOTO Decreasing thrust sound only if nothing else is playing or if we're going to interrupt looping/starting
                if((thrusterSFX.getStatus() != Status.Playing && "Ending".equals(thrusterSFX.getName())) || "Looping".equals(thrusterSFX.getName()) || "Starting".equals(thrusterSFX.getName()) || "Boosting".equals(thrusterSFX.getName())){
                    thrusterSFX.stop();
                    thrusterSFX = thrusterSFXList[2];
                    thrusterSFX.play();
                }
                break;

            case IDLE: //GOTO Looping thrust sound if we've just finished starting or we're currently slowing
                if((thrusterSFX.getStatus() != Status.Playing && "Starting".equals(thrusterSFX.getName()) ) || "Boosting".equals(thrusterSFX.getName()) || "Ending".equals(thrusterSFX.getName()) ){
                    thrusterSFX.stop();
                    thrusterSFX = thrusterSFXList[1];
                    thrusterSFX.play();
                }
                break;
            case IDLEZERO: //GOTO engine hum noise if we're coming from starting/slowing or nothing is playing
                if (thrusterSFX.getStatus() != Status.Playing && "Ending".equals(thrusterSFX.getName())) {
                    thrusterSFX.stop();
                    thrusterSFX = thrusterSFXList[4];
                    //thrusterSFX.play(); //This is disabled - it sounds abrupt and ugly.
                }
                break;
            case BOOST:
                if (!("Boosting".equals(thrusterSFX.getName()) && thrusterSFX.getStatus() == Status.Playing)) {
                    thrusterSFX.stop();
                    thrusterSFX = thrusterSFXList[3];
                    thrusterSFX.play(); //This is disabled - it sounds abrupt and ugly.
                }
                break;
        }

        // Engine Heat
        if (thrustValue > 0 || verticalThrustValue > 0 || boosting) {
            // Heat should give curve: (110/(1+e^(-1((t-30)/10))))
            // Function here is differential of that ^
            // (11e^(x/10 + 3)) / ((e^(x/10) + e^3)^2)
            engineHeat += (
                    (11 * FastMath.exp(3 + engineHeat/10))
                                    /
                    FastMath.pow(FastMath.exp(engineHeat/10) + FastMath.exp(3), 2)
                    )
                    * tpf * ((thrustValue>0 ? 1 : 0) + (verticalThrustValue>0 ? 0.35f : 0) + (boosting ? 0.7f : 0));
            // After it's been through the function, multiply it by the respective factors for thrusting, vert thrusting, and boosting.
            // Different types of thrusting affect heat generation. Given how much fuel boosting uses, it doesn't need to increase the heat so much, 
            // as it's supposed to present short term benefits and downsides
            
        } else if (engineHeat > 1) {
            engineHeat -= 0.005 * consistency;
            engineHeat = (engineHeat < 0) ? 0 : engineHeat;
        }

        if (fuel > 0 && (thrustValue > 0 || verticalThrustValue > 0 || boosting)) {
            //fuel -= ((boosting ? 3 : 1) * fuelRate * (engineHeat / 30) * consistency); //OLD
            
            // (e^((x/30)^1.3) / 20)
            // Plot in a graphing tool to visualise.
            // Thrust/Vert Thrust/Boost have multipliers that sum together to multiply the function
            // Thrust = 1, Vert Thrust = 0.35, Boost = 0.7
            // TODO MOVE THESE TO GLOBAL FINALS
            fuel -= (
                    ((FastMath.exp(FastMath.pow(engineHeat / 30, 1.3f)) / 20f)) * ((thrustValue>0 ? 1 : 0) + (verticalThrustValue>0 ? 0.35f : 0) + (boosting ? 0.7f : 0))
                    ) * consistency;
            fuel = (fuel < 0) ? 0 : fuel;
        }

        if (repairing && armour < armourCapacity && fuel > 0) {
            armour += 1 / 15f * consistency;
            armour = (armour > armourCapacity) ? armourCapacity : armour;
            engineHeat += 0.15f * consistency;
            fuel -= 0.2f * consistency;
        }
        armour = (armour < 0) ? 0 : armour;

        if (!underwater && getPhysicsLocation().y < WATER_HEIGHT) {
            underwater = true;
            underwaterAudio(underwater);
        } else if (underwater && getPhysicsLocation().y > WATER_HEIGHT) {
            underwater = false;
            underwaterAudio(underwater);
        }
    }

    public float[] getShipData() {
        return new float[]{fuel, engineHeat, armour, getSpeed()};
    }

    // If lacking fuel, too damaged, repairing, or too hot - don't allow thrust or rotation
    public boolean canFly() {
        return (armour > armourDisableThreshold && fuel > 0 && engineHeat < 110 && !repairing);
    }

    // Modifies cooling. Use negative values to heat it up.
    public void waterCool(float coolingVal) {
        if (engineHeat > 10) {
            engineHeat -= coolingVal;
        }
    }

    // Can also be used to defuel.
    public void refuel(float fuelIncrease) {
        if (fuel < fuelCapacity) {
            fuel += fuelIncrease;
        }
        fuel = (fuel > fuelCapacity) ? fuelCapacity : fuel;
    }

    public PhysicsSpace getPhysicsSpace() {
        return space;
    }

    //TODO Update these
    @Override
    public void write(CWExporter ex) throws IOException {
        super.write(ex);
        OutputCapsule oc = ex.getCapsule(this);
        oc.write(enabled, "enabled", true);
        oc.write(spatial, "spatial", null);
    }

    //TODO Update these
    @Override
    public void read(CWImporter im) throws IOException {
        super.read(im);
        InputCapsule ic = im.getCapsule(this);
        enabled = ic.readBoolean("enabled", true);
        spatial = (Spatial) ic.readSavable("spatial", null);
    }
}
/*
            No, not that kind of ship.

                   ,:',:`,:'
                __||_||_||_||__
           ____["""""""""""""""]____
           \ " '''''''''''''''''''' |
    ~~^~~~^~^~^^~^~^~^~^~^~^~^~~^~^~^^~~^~^
 */

 /*

    === Key Bindings ===
W / S - Pitch up / down
A / D - Roll left / right
Q / E - Yaw left / right
R / F - Thrust vertically up / down
Space - Thrust forward
Shift - Thruster Boost
C - repair
K - Fire Missile



    === How to Fly ===
In most scenarios, simply use upward vertical thrusters in conjunction with the main thrusters. This works best on flatter surfaces, or elevated platforms.

When attempting a faster take-off, use vertical thrusters to build enough height to safely begin forward thrusting.
Given that lift is generated by horizontal movement, enough speed will allow you to fly straight forward without losing height.
However this takes a few seconds to build, so use a combination of vertical thrusters to build height followed by forward thrusting at about
30-45 degrees to the horizontal to counteract gravity.
The vertical component of the main thruster combined with the vertical component of the lift (We're pointing ~30 degrees upward, and lift acts 
at the normal of the ship, so it's not all vertical lift) gives enough thrust to counteract gravity after the first couple of seconds, giving a small
drop in height over that time which is why we climb with vertical thrusters initially; it's to buy us time when we build up velocity with the main engines.

Conversely, if you're attempting to get airborne but wish to travel only a short distance, use vertical thrusters to gain height, then rotate to a near-upward
pointing orientation and thrust with main engines only. This may prove more useful when in a valley of terrain, surrounded by hill-faces.

|    LIFT         SHIP 
| \               _ 
|   \          _/_/
|     \      /    /    / ^^
<------    /  _ /    /   ||
         /  /_/    /     ||  THRUST 
       /   _/    /       ||
     / / / /   /         ||
    /_|-/_/   <---------
    ##
   ##
  # 
    === Ship Glossary ===

Heat :      Builds up when repairing and thrusting. Lowers when not thrusting, and rapidly lowers in water. 
            Affects fuel consumption, and shuts down ship if too high.

Armour :    Health pool. Damaged by missiles, weapons, and on impact a surface too hard. 
            Scraping does no damage. Ship is disabled at 10 armour, and explodes when hit AFTER being at 0 armour.

Repairing : Drains fuel, builds heat, and fixes damaged armour.



    === Flight Mechanics ===
Lift is generated normal to the ships facing direction (Lift is "up" from the ship) and increases with speed.
The ship partially glides; it retains some velocity based on the severity of the change in direction, and redirects some of that in the new direction.
Sudden changes in direction result in less velocity being put into the new direction, and a slower movement in the previous direction.
E.G. :  Transitioning from nose-down to flat immediately means a sudden drop in speed and a large loss of the downward velocity.
        Transitioning from nose-down to flat graudally means a lesser loss of speed, and a partial conversion of some of the downward 
        velocity into horizontal velocity.

Water will actively impair all velocities on the ship, and thusly worsen the effects of any forces (gravity, thrust).
This makes the free-fall through water much slower than air, but it also makes it harder to leave the water.

Thrust is distributed between angular, main, and vertical thrusters based on a fixed priority system.
Main:Vertical:Angular = 6:3:1
The engine's maximum output is shared in such a ratio.
When only is running, it will get full power. Angular thrusters have adjusted outputs to make up for their low ratio.

The vertical and main thrusters have a "warm-up" time to them. When using either, they both contribute to engine "momentum".
Initially, both will be weak, but using either will warm the engine and bring the thrust force of both up to the normal level.
The warm-up (0 to 1.5) is shared by both thrusters. Main thrusters alone increase it to maximum in 1.5 seconds, and the vertical thrusters
increase it in 3 seconds. Combined, it's done in 1 second. Main thrusters add 1 unit out of the 1.5 maximum every second, whereas vertical thrusters only
add 0.5 units every second.

This makes some maneouvres harder, and adds a slight variance and delay to taking off with vertical thrusters. It lessens the effectivness of
the 180-boost maneouvre (as seen in The Expanse), wherein a ship flips its direction (e.g. North to South direction) and thrusts.
With this warm-up, you cannot easily turn the ship with full power to angular thrusters, then full power to main. You will instead turn
just as quick, but lose height (if horizontal), and take longer to being moving in the other direction at the previous speed.

Vertical thrusters do not output any real thrust when the ship's upward speed is greater than 2 units. This is to allow them to be strong enough to perform
take-off/landing easily, but not so strong that they can be used to build huge vertical speed and cheat the gliding system. Obviously they can't be used
to fly horizontally as well, as the drag and lift systems do not benefit perpendicular flight.


    === Internal Mechanics ===
Armour takes damage from missiles and collisions. If you hear a "clunk", you've taken damage. If you hear scraping, you're likely not at risk.
When below 10, the ship is disabled. Repair to fix this. At 0, the ship is irreperable and will detonate in 10~ seconds if not hit by something else.

Fuel drains slowly, but this rate increases linearly as heat does. Currently, landing in water will allow you to refuel gradually.
Without fuel, the ship will no longer be able to thrust or maneouvre. Watch your fuel guage.

Heat builds as thrusters (excluding the angular maneouvring thrusters) are active. It decreases over time, and rapidly cools when in water.
If the heat builds to beyond 110 celsius, the ship will lose control. Any active non-angular thrusters will still remain active, but when deactivated will 
not be controllable again until the ship cools to <110 celsius.



    === Enemies ===
Missiles are force-powered, thusly they will have a hard time curving to hit you if they near-miss you, 
but will build velocity in a straight line faster than you can thrust - it's smarter to out-maneouvre than out-run.
 */
