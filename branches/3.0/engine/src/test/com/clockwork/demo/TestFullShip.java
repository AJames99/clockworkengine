package com.clockwork.demo;

import clockworktest.physics.ExplosiveControl;
import com.clockwork.app.*;
import com.clockwork.app.state.*;
import com.clockwork.audio.*;
import com.clockwork.audio.AudioSource.Status;
import com.clockwork.bounding.*;
import com.clockwork.bullet.BulletAppState;
import com.clockwork.bullet.PhysicsSpace;
import com.clockwork.bullet.collision.*;
import com.clockwork.bullet.collision.shapes.*;
import com.clockwork.bullet.control.*;
import com.clockwork.bullet.util.CollisionShapeFactory;
import com.clockwork.font.*;
import com.clockwork.demo.Missile.TeamTag;
import com.clockwork.input.*;
import com.clockwork.input.controls.ActionListener;
import com.clockwork.input.controls.KeyTrigger;
import com.clockwork.light.*;
import com.clockwork.material.*;
import com.clockwork.material.RenderState.BlendMode;
import com.clockwork.math.*;
import com.clockwork.post.*;
import com.clockwork.post.filters.*;
import com.clockwork.renderer.queue.RenderOrder.ShadowMode;
import com.clockwork.scene.*;
import com.clockwork.scene.Spatial.CullHint;
import com.clockwork.scene.shape.*;
import com.clockwork.shadow.*;
import com.clockwork.terrain.geomipmap.*;
import com.clockwork.terrain.geomipmap.lodcalc.*;
import com.clockwork.terrain.heightmap.*;
import com.clockwork.texture.*;
import com.clockwork.texture.Texture.WrapMode;
import com.clockwork.util.*;
import com.clockwork.water.*;
import java.time.*;
import java.time.format.*;
import java.util.*;

// SHORT-TERM TODO
// Add cannon fire sfx
// Add volume controls by editing listener.setVolume()
// Add sound-barrier break SFX to boost
// Add wing contrail vfx
// Add ship explosion and game-over at 0 hp

// LONG-TERM TODO
// Add temperature-influenced warping and smoke vfx
// Have direction be influenced by velocity ?
// Articulatable wings and thrusters based on speed, turning
// Add lens flare, dirt on lens
// Switch terrain to ran-gen terrain?
public class TestFullShip extends BasicApplication implements ActionListener, PhysicsCollisionListener {

    private final Vector3f defaultPos = new Vector3f(-2517f, 206f, -2081f);
    private final Quaternion defaultRot = new Quaternion(-0.018f, 0.398f, 0.0805f, 0.9139f);
    
    private boolean isShaking = false;
    private boolean isAngularShaking = false;
    private float linearIntensity = 0.1f;
    private float angularIntensity = FastMath.PI / 6;
    private CameraNode camNode;
    private float camOffset;
    private final float camRayLength = 19f;
    
    private SceneNode cameraParentNode;
    private float shipSpeed = 0f;
    
    private final float angularEngineThrust = 18f;
    private final float mainEngineThrust = 50f;
    private final float verticalEngineThrust = 60f;

    BitmapText fuelText;
    BitmapText heatText;
    BitmapText armourText;
    BitmapText speedText;

    TerrainQuad terrain;
    SceneNode terrainPhysicsNode;
    Material matRock;
    Material matTerrain;
    Material matWire;
    private final float grassScale = 256;
    private final float dirtScale = 64;
    private final float rockScale = 128;
    private final float pebblesScale = 128;

    private TranslucentBucketFilter tbf;
    boolean wireframe = false;
    protected BitmapText hintText;
    PointLight pl;
    GeometrySpatial lightMdl;
    GeometrySpatial collisionMarker;
    private final Environment underwaterEnv = Environment.Cave;
    private final Environment normalEnv = new Environment();
    private AudioNode cannonSFX;
    private AudioNode musicSFX;
    private AudioNode windSFX;
    private AudioNode ambienceSFX;

    private AudioNode[] shipCollisionSFXList;
    private AudioNode shipCollisionSFX = new AudioNode();
    private AudioNode shipScrapeSFX = new AudioNode();

    private final float defaultVolume = 0.35f;
    private final float defaultScrapeVolume = 0.05f;
    private boolean underwater = false;
    private SceneNode particleNode;
    ///////


    //private PhysicsHoverControl shipControl;
    private PhysicsShipControl shipControl;
    private Spatial shipSpat;
    private CollisionShape colShape;
    ///////

    private BulletAppState bulletAppState;
    private BetterChaseCamera chaseCam;
    private FilterPostProcessor fpp;
    private WaterFilter water;
    private final float steering = 0.25f;
    public static final float WATER_HEIGHT = -20f;
    private DirectionalLight dl;

    int bricksPerLayer = 8;
    int brickLayers = 30;

    //static float brickWidth = .75f, brickHeight = .25f, brickDepth = .25f;
    static float brickWidth = 0.75f, brickHeight = 0.25f, brickDepth = 0.25f;
    float radius = 3f;
    float angle = 0;

    private boolean timeSlow = false;

    public static void main(String[] args) {
        TestFullShip app = new TestFullShip();
        app.start();
    }

    private void setupKeys() {
        inputManager.addMapping("Lefts", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Rights", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("YawLeft", new KeyTrigger(KeyInput.KEY_Q));
        inputManager.addMapping("YawRight", new KeyTrigger(KeyInput.KEY_E));
        inputManager.addMapping("Ups", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Downs", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("VerticalThrustUp", new KeyTrigger(KeyInput.KEY_R));
        inputManager.addMapping("VerticalThrustDown", new KeyTrigger(KeyInput.KEY_F));
        inputManager.addMapping("Space", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("Boost", new KeyTrigger(KeyInput.KEY_LSHIFT));
        inputManager.addMapping("Reset", new KeyTrigger(KeyInput.KEY_RETURN));
        inputManager.addMapping("Repair", new KeyTrigger(KeyInput.KEY_C));
        inputManager.addMapping("Missile", new KeyTrigger(KeyInput.KEY_K));
        //inputManager.addMapping("TimeSlow", new KeyTrigger(KeyInput.KEY_E));
        inputManager.addListener(this, "Lefts");
        inputManager.addListener(this, "Rights");
        inputManager.addListener(this, "Ups");
        inputManager.addListener(this, "Downs");
        inputManager.addListener(this, "Space");
        inputManager.addListener(this, "Boost");
        inputManager.addListener(this, "Reset");
        inputManager.addListener(this, "Repair");
        //inputManager.addListener(this, "TimeSlow");
        inputManager.addListener(this, "YawLeft");
        inputManager.addListener(this, "YawRight");
        inputManager.addListener(this, "VerticalThrustUp");
        inputManager.addListener(this, "VerticalThrustDown");
        inputManager.addListener(this, "Missile");
    }

    private void setupShadows() {
        final DirectionalLightShadowRenderer pssmRenderer = new DirectionalLightShadowRenderer(assetManager, 1024, 4);
        viewPort.addProcessor(pssmRenderer);
        pssmRenderer.setLight(dl);
        pssmRenderer.setLambda(0.55f);
        pssmRenderer.setShadowIntensity(0.55f);
        pssmRenderer.setShadowCompareMode(com.clockwork.shadow.CompareMode.Software);
        pssmRenderer.setEdgeFilteringMode(EdgeFilteringMode.PCF4);

        //FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        //LightScatteringFilter filter = new LightScatteringFilter(dl.getDirection().multLocal(-3000));
        //filter.setLightDensity(0.2f);
        //fpp.addFilter(filter);
    }

    private void setupLighting() {
        dl = new DirectionalLight();
        dl.setDirection(new Vector3f(-0.5f, -1f, -0.3f).normalizeLocal());
        rootNode.addLight(dl);

        AmbientLight al = new AmbientLight();
        al.setColor(ColorRGBA.White.mult(0.5f));
        rootNode.addLight(al);

        BloomFilter bloom = new BloomFilter();
        bloom.setDownSamplingFactor(2);
        bloom.setBlurScale(1.37f);
        bloom.setExposurePower(3.30f);
        bloom.setExposureCutOff(0.2f);
        bloom.setBloomIntensity(1.45f);
        fpp.addFilter(bloom);
        //BloomUI ui=new BloomUI(inputManager, bloom);
    }

    private void setupFog() {
        FogFilter fog = new FogFilter();
        fog.setFogColor(new ColorRGBA(0.9f, 0.9f, 0.9f, 1.0f));
        fog.setFogDistance(2550f); //155
        fog.setFogDensity(2f); //1
        fpp.addFilter(fog);
    }
    
    private void setupCargo(){
        Spatial cargo_spat = assetManager.loadModel("Blender/cargo.blend");
        cargo_spat.setShadowMode(ShadowMode.CastAndReceive);
        cargo_spat.setLocalScale(0.6f);
        cargo_spat.setName("CARGO");
        cargo_spat.centre();
        
        TangentBiNormalGenerator.generate(cargo_spat);
        
        
        Spatial cargo_hull = assetManager.loadModel("Blender/cargo_hull.blend");
        cargo_hull.centre();
        cargo_hull.setLocalScale(cargo_spat.getLocalScale());
        CollisionShape cargo_collision = CollisionShapeFactory.createDynamicMeshShape(cargo_hull);
        
        CargoControl cargo_control = new CargoControl(cargo_spat, cargo_collision, /*200*/100);
        cargo_control.setFriction(0.8f);
        //cargo_spat.addControl(cargo_control);
        
        rootNode.attachChild(cargo_spat);
        getPhysicsSpace().addAll(cargo_spat);
        
        cargo_control.setPhysicsLocation(defaultPos.add(new Vector3f(0, 10, 0)));
                
        //shipControl.attachCargo(cargo_spat);        
        //shipControl.setupChain(rootNode);
    }

    private void setupTerrain() {
        // TERRAIN TEXTURE material
        matTerrain = new Material(assetManager, "Common/MatDefs/Terrain/TerrainLighting.mdef");
        matTerrain.setBoolean("useTriPlanarMapping", true);
        matTerrain.setFloat("Shininess", 0.0f);

        // ALPHA map (for splat textures)
        matTerrain.setTexture("AlphaMap", assetManager.loadTexture("Textures/Terrain/splat/alpha1.png"));
        matTerrain.setTexture("AlphaMap_1", assetManager.loadTexture("Textures/Terrain/splat/alpha2.png"));
        // this material also supports 'AlphaMap_2', so you can get up to 12 diffuse textures

        // HEIGHTMAP image (for the terrain heightmap)
        Texture heightMapImage = assetManager.loadTexture("Textures/Terrain/splat/mountains512.png");

        // DIRT texture, Diffuse textures 0 to 3 use the first AlphaMap
        Texture dirt = assetManager.loadTexture("Textures/Terrain/splat/stone.jpg");
        dirt.setWrap(WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap", dirt);
        matTerrain.setFloat("DiffuseMap_0_scale", dirtScale);

        // GRASS texture
        Texture grass = assetManager.loadTexture("Textures/Terrain/splat/grass.jpg");
        grass.setWrap(WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap_1", grass);
        matTerrain.setFloat("DiffuseMap_1_scale", grassScale);

        // ROCK texture
        Texture rock = assetManager.loadTexture("Textures/Terrain/splat/road.jpg");
        rock.setWrap(WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap_2", rock);
        matTerrain.setFloat("DiffuseMap_2_scale", rockScale);

        // BRICK texture
        Texture brick = assetManager.loadTexture("Textures/Terrain/splat/dirt.jpg");
        brick.setWrap(WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap_3", brick);
        matTerrain.setFloat("DiffuseMap_3_scale", rockScale);

        // RIVER ROCK texture, this texture will use AlphaMap_1
        Texture riverRock = assetManager.loadTexture("Textures/Terrain/Pond/Pond.jpg");
        riverRock.setWrap(WrapMode.Repeat);
        matTerrain.setTexture("DiffuseMap_4", riverRock);
        matTerrain.setFloat("DiffuseMap_4_scale", pebblesScale);

        // diffuse textures 4 to 7 use AlphaMap_1
        // diffuse textures 8 to 11 use AlphaMap_2
        Texture normalMap0 = assetManager.loadTexture("Textures/Terrain/splat/grass_normal.jpg");
        normalMap0.setWrap(WrapMode.Repeat);
        Texture normalMap1 = assetManager.loadTexture("Textures/Terrain/splat/stone_normal.jpg");
        normalMap1.setWrap(WrapMode.Repeat);
        Texture normalMap2 = assetManager.loadTexture("Textures/Terrain/splat/road_normal.jpg");
        normalMap2.setWrap(WrapMode.Repeat);
        
        matTerrain.setTexture("NormalMap", normalMap0);
        matTerrain.setTexture("NormalMap_1", normalMap2);
        matTerrain.setTexture("NormalMap_2", normalMap2);
        matTerrain.setTexture("NormalMap_4", normalMap2);

        // WIREFRAME material (used to debug the terrain, only useful for this test case)
        matWire = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        matWire.getAdditionalRenderState().setWireframe(true);
        matWire.setColor("Color", ColorRGBA.Green);

        // CREATE HEIGHTMAP
        AbstractHeightMap heightmap = null;
        try {
            heightmap = new ImageBasedHeightMap(heightMapImage.getImage(), 0.5f);
            heightmap.load();
            heightmap.smooth(0.9f, 1);

        } catch (Exception e) {
            e.printStackTrace();
        }

        
        // For use with tri-planar textures ONLY.
        matTerrain.setFloat("DiffuseMap_0_scale", 1f / (float) (1024f / dirtScale));
        matTerrain.setFloat("DiffuseMap_1_scale", 1f / (float) (1024f / grassScale));
        matTerrain.setFloat("DiffuseMap_2_scale", 1f / (float) (1024f / rockScale));
        matTerrain.setFloat("DiffuseMap_3_scale", 1f / (float) (1024f / rockScale));
        matTerrain.setFloat("DiffuseMap_4_scale", 1f / (float) (1024f / pebblesScale));
        
        
        terrain = new TerrainQuad("terrain", 65, 1025, heightmap.getHeightMap());//, new LodPerspectiveCalculatorFactory(getCamera(), 4)); // add this in to see it use entropy for LOD calculations
        TerrainLodControl control = new TerrainLodControl(terrain, getCamera());
        control.setLodCalculator(new DistanceLodCalculator(65, 5.7f)); // patch size, and a multiplier
        terrain.addControl(control);
        terrain.setMaterial(matTerrain);
        terrain.setModelBound(new BoundingBox());
        terrain.updateModelBound();
        terrain.setLocalScale(5f);
        terrain.setShadowMode(ShadowMode.CastAndReceive);
        rootNode.attachChild(terrain);

        terrain.setLocalTranslation(0, -50, 0);
        terrain.updateGeometricState();
        
        /**
         * Create PhysicsRigidBodyControl for collision
         */
        terrain.addControl(new RigidBodyControl(0));
        bulletAppState.getPhysicsSpace().addAll(terrain);
    }

    @Override
    public void basicInitialiseApp() {
        setDisplayStatView(false);
        bulletAppState = new BulletAppState();
        //Experimental!
        //bulletAppState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);
        stateManager.attach(bulletAppState);
        
        ScreenshotAppState screenShotState = new ScreenshotAppState();
        //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        //screenShotState.setFilePath("C:/Users/Alex SSD/Pictures/CW Screens/" + DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.now()) + "/");

        screenShotState.setFilePath("E:/CW Screens/" + DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.now()) + "/");
        stateManager.attach(screenShotState);
//        bulletAppState.getPhysicsSpace().enableDebug(assetManager);
        /*if (settings.getRenderer().startsWith("LWJGL")) {
            BasicShadowRenderer bsr = new BasicShadowRenderer(assetManager, 512);
            bsr.setDirection(new Vector3f(-0.5f, -0.3f, -0.3f).normalizeLocal());
            viewPort.addProcessor(bsr);
        }*/
        //cam.setFrustumFar(150f);
        //flyCam.setMoveSpeed(10);

        setupKeys();
        setupSkyBox();

        // Adding this game as a collision listener
        getPhysicsSpace().addCollisionListener(this);

        particleNode = new SceneNode("particleNode");
        rootNode.attachChild(particleNode);
        fpp = new FilterPostProcessor(assetManager);
        setupLighting();
        setupTerrain();
        buildShip();
        // Ordering of processors matters.
        setupShadows();
        //setupWater();
        setupLakeWater();
        setupFog();
        tbf = new TranslucentBucketFilter(false); //bool = soft particles
        fpp.addFilter(tbf);
        viewPort.addProcessor(fpp);
        
        cannonSFX = new AudioNode(assetManager, "Sound/Effects/Bang.wav", false);
        cannonSFX.setLooping(false);
        musicSFX = new AudioNode(assetManager, "Sound/Music.wav", false);
        musicSFX.setLooping(true);
        musicSFX.setVolume(defaultVolume);
        musicSFX.setPositional(false);
        windSFX = new AudioNode(assetManager, "Sound/wind_loop_2.wav", false);
        windSFX.setLooping(true);
        windSFX.setPositional(false);
        windSFX.setVolume(0.0001f);
        windSFX.play();
        ambienceSFX = new AudioNode(assetManager, "Sound/wind_loop_1.wav", false);
        ambienceSFX.setPositional(false);
        ambienceSFX.setLooping(true);
        ambienceSFX.setVolume(0.1f);
        ambienceSFX.play();
        setupTurrets();
        setupText();
        setupCargo();
    }

    private PhysicsSpace getPhysicsSpace() {
        return bulletAppState.getPhysicsSpace();
    }

    private GeometrySpatial findGeom(Spatial spatial, String name) {
        if (spatial instanceof SceneNode) {
            SceneNode node = (SceneNode) spatial;
            System.out.println(node.getName());
            for (int i = 0; i < node.getQuantity(); i++) {
                Spatial child = node.getChild(i);
                System.out.println(child.getName());
                GeometrySpatial result = findGeom(child, name);
                if (result != null) {
                    return result;
                }
            }
        } else if (spatial instanceof GeometrySpatial) {
            if (spatial.getName().startsWith(name)) {
                return (GeometrySpatial) spatial;
            }
        }
        return null;
    }

    private void setupWater() {
        water = new WaterFilter(rootNode, dl.getDirection());
        //water.setCenter(new Vector3f(230f, 0f, 710f));
        //water.setRadius(16);//260
        water.setWaveScale(0.003f);//0.003
        water.setMaxAmplitude(0.5f);//2
        water.setFoamExistence(new Vector3f(1f, 4, 0.5f));
        water.setFoamTexture((Texture2) assetManager.loadTexture("Common/MatDefs/Water/Textures/foam2.jpg"));
        water.setRefractionStrength(0.2f);
        water.setWaterHeight(WATER_HEIGHT);
        water.setSpeed(0.05f);
        fpp.addFilter(water);
    }

    private void setupLakeWater() {
        water = new WaterFilter(rootNode, dl.getDirection());
        water.setWaterHeight(WATER_HEIGHT);

        //water.setUseFoam(false);
        water.setFoamExistence(new Vector3f(1f, 4, 0.5f));
        water.setFoamTexture((Texture2) assetManager.loadTexture("Common/MatDefs/Water/Textures/foam2.jpg"));

        water.setUseRipples(false);
        water.setDeepWaterColor(ColorRGBA.Brown);
        water.setWaterColor(ColorRGBA.Brown.mult(2.0f));
        water.setWaterTransparency(0.2f);
        water.setMaxAmplitude(0.3f);
        water.setWaveScale(0.008f);
        water.setSpeed(0.7f);
        water.setShoreHardness(1.0f);
        water.setRefractionConstant(0.2f);
        water.setShininess(0.3f);
        water.setSunScale(1.0f);
        water.setColorExtinction(new Vector3f(10.0f, 20.0f, 30.0f));
        fpp.addFilter(water);
    }

    public void setupSkyBox() {
        //rootNode.attachChild(SkyCreator.createSky(assetManager, "Scenes/Beach/FullskiesSunset0068.dds", false));
        rootNode.attachChild(SkyCreator.createSky(assetManager, "Textures/Sky/Bright/BrightSky.dds", false));
    }

    private void buildShip() {
        Material shipMat = assetManager.loadMaterial("Blender/T7/T7.mat");
        //Material shipMat = assetManager.loadMaterial("Blender/Foliage/foliage/bark_brown.mat");
        Texture shipTex = assetManager.loadTexture("Blender/T7/ComboBake.png");
        shipSpat = assetManager.loadModel("Blender/T7/T7-repaired.blend");

        // Needed for collision listener
        shipSpat.setName("ship");
        Spatial shipHullShape = assetManager.loadModel("Blender/T7/T7Hull.blend");

        shipMat.getTextureParam("DiffuseMap").setTextureValue(shipTex);

        shipSpat.setMaterial(shipMat);
        shipSpat.centre();
        //HullCollisionShape colShape = CollisionShapeFactory.createDynamicMeshShape(shipSpat);
        //CollisionShape colShape = CollisionShapeFactory.createDynamicMeshShape(shipSpat.scale(0.5f));
        shipSpat.setShadowMode(ShadowMode.CastAndReceive);
        //shipSpat.setLocalTranslation(new Vector3f(95, 5, -128));
        //shipSpat.setLocalRotation(new Quaternion(new float[]{0, 0.01f, 0}));
        shipSpat.setLocalScale(0.5f);
        shipHullShape.setLocalScale(0.5f);
        colShape = CollisionShapeFactory.createDynamicMeshShape(shipHullShape);
        shipControl = new PhysicsShipControl(colShape, 500);
        shipControl.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_01);
        shipControl.setAngularSleepingThreshold(0.1f);
        shipControl.setFriction(0.4f);
        
        

        shipSpat.addControl(shipControl);
        rootNode.attachChild(shipSpat);
        getPhysicsSpace().add(shipControl);
        shipControl.setPhysicsLocation(defaultPos);
        shipControl.setPhysicsRotation(defaultRot);
        setupCollisionAudio();
        
        setupChaseCamera();
       
        shipControl.setupEmitters(assetManager);
        shipControl.setupAudio(assetManager);
        shipControl.setAssetHandler(assetManager);
        
        
        /*Box cargo_attachment = new Box(5, 1, 9);
        GeometrySpatial geom = new GeometrySpatial("Cargo Holder", cargo_attachment);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        geom.setMaterial(mat);
        //geom.setCullHint(CullHint.Always);
        geom.setLocalTranslation(0, -5, 4);
        //CollisionShape pickup_collision = CollisionShapeFactory.createDynamicMeshShape(geom);
        //RigidBodyControl pickup_control = new RigidBodyControl(pickup_collision, 0);
        //pickup_control.setKinematic(false);
        //pickup_control.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_01);
        //pickup_control.setApplyPhysicsLocal(true);
        //geom.addControl(pickup_control);
        ((SceneNode) shipSpat).attachChild(geom);
        //getPhysicsSpace().addAll(geom);*/
    }

    public void setupTurrets() {
        // Using linear interpolation to find the terrain height.

        /*(if (intOnX > intOnZ) {
            return (1 - intOnX)topLeft + (intOnX - intOnZ)topRight + (intOnZ) * bottomRight;
        } else {
            return (1 - intOnZ)topLeft + (intOnZ - intOnX)bottomLeft + (intOnX) * bottomRight;
        }*/

        
        // .x = X pos, .y = Z pos, in scenespace
        Vector2f turret_pos_xz = new Vector2f(-1250, -1177);
        
        Turret turret = new Turret(assetManager);
        //turret.getMasterNode().setLocalTranslation(new Vector3f(turret_pos_xz.x, terrain.getHeight(turret_pos_xz), turret_pos_xz.y));
        turret.setPhysicsLocation(new Vector3f(turret_pos_xz.x, terrain.getHeight(turret_pos_xz) + terrain.getLocalTranslation().y, turret_pos_xz.y));
        turret.setTarget(shipControl);
        rootNode.attachChild(turret.getMasterNode());
        getPhysicsSpace().add(turret);
        
        
        turret_pos_xz = new Vector2f(defaultPos.x - 20, defaultPos.z - 40);
        Turret friendlyTurret = new Turret(assetManager);
        //turret.getMasterNode().setLocalTranslation(new Vector3f(turret_pos_xz.x, terrain.getHeight(turret_pos_xz), turret_pos_xz.y));
        friendlyTurret.setTag(TeamTag.ALLIED);
        friendlyTurret.setPhysicsLocation(new Vector3f(turret_pos_xz.x, terrain.getHeight(turret_pos_xz) + terrain.getLocalTranslation().y + 3, turret_pos_xz.y));
        rootNode.attachChild(friendlyTurret.getMasterNode());
        getPhysicsSpace().add(friendlyTurret);        
    }

    public void shipMissile() {
        Vector3f pos = shipSpat.getWorldTranslation().clone();
        Quaternion rot = shipSpat.getWorldRotation();
        Vector3f dir = rot.getRotationColumn(2);

        Spatial missile = assetManager.loadModel("Models/SpaceCraft/Rocket.mesh.xml");
        missile.scale(0.5f);
        missile.rotate(0, FastMath.PI, 0);
        missile.updateGeometricState();

        BoundingBox box = (BoundingBox) missile.getWorldBound();
        final Vector3f extent = box.getExtent(null);

        BoxCollisionShape boxShape = new BoxCollisionShape(extent);

        missile.setName("Missile");
        missile.rotate(rot);
        missile.setLocalTranslation(pos.addLocal(0, extent.y * (4.5f * shipSpat.getLocalScale().length()), 0));
        missile.setLocalRotation(shipControl.getPhysicsRotation());
        missile.setShadowMode(ShadowMode.Cast);
        RigidBodyControl control = new ExplosiveControl(assetManager, boxShape, 20);
        control.setLinearVelocity(dir.mult(100));
        control.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_03);
        missile.addControl(control);

        cannonSFX.playInstance();

        rootNode.attachChild(missile);
        getPhysicsSpace().add(missile);
    }

    /*public void turretMissile() {
        Vector3f pos = turret.getLocalTranslation().clone();
        Quaternion rot = turret.getLocalRotation().clone();
        Vector3f dir = rot.getRotationColumn(2);

        Spatial missile = assetManager.loadModel("Models/SpaceCraft/Rocket.mesh.xml");
        missile.scale(5f);
        missile.rotate(0, FastMath.PI, 0);
        missile.updateGeometricState();

        BoundingBox box = (BoundingBox) missile.getWorldBound();
        final Vector3f extent = box.getExtent(null);

        BoxCollisionShape boxShape = new BoxCollisionShape(extent);

        missile.setName("Missile");
        missile.rotate(rot);
        missile.setLocalTranslation(pos);
        missile.setShadowMode(ShadowMode.Cast);
        RigidBodyControl control = new ExplosiveControl(assetManager, boxShape, 20);
        control.setGravity(Vector3f.ZERO);
        control.setLinearVelocity(dir.mult(100));
        control.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_03);
        missile.addControl(control);

//        cannonSFX.playInstance();

        rootNode.attachChild(missile);
        getPhysicsSpace().add(missile);
    }*/
    public void addBrick(Vector3f ori) {
        /*    GeometrySpatial reBoxg = new GeometrySpatial("brick", brick);
        reBoxg.setMaterial(mat);
        reBoxg.setLocalTranslation(ori);
        reBoxg.rotate(0f, (float) Math.toRadians(angle), 0f);
        reBoxg.addControl(new RigidBodyControl(1.5f));
        reBoxg.setShadowMode(ShadowMode.CastAndReceive);
        reBoxg.getControl(RigidBodyControl.class).setFriction(1.6f);
        this.rootNode.attachChild(reBoxg);
        this.getPhysicsSpace().add(reBoxg); */
    }

    private void setupChaseCamera() {
        // cam is the Camera
        // camNode is the space node for the cam, used for camera shakes and other temporary transformations
        // cameraParentNode is the parent for the camNode, used for target inheritance
        flyCam.setEnabled(false);
        camNode = new CameraNode("Camera Base Node", cam);
        cameraParentNode = new SceneNode("Camera Parent Node");
        
        // Now a child of the world instead of the ship, to allow for better camera easing
        //((SceneNode) target).attachChild(cameraParentNode);
        cameraParentNode.attachChild(camNode);

        //camNode.setControlDir(ControlDirection.SpatialToCamera);
        inputManager.setCursorVisible(false);
        stateManager.detach(stateManager.getState(FreeCamAppState.class));
        rootNode.attachChild(cameraParentNode);
        cam.setFrustumFar(1600);
        cameraParentNode.setLocalTranslation(defaultPos);
        cam.setLocation(defaultPos);
    }

    private Vector3f getCameraForward(){
        return new Vector3f(cameraParentNode.getLocalRotation().mult(Vector3f.UNIT_Z));
    }
    
    float cameraLerpFactor = 0.1f * 144f;
    Quaternion camTempR = new Quaternion();
    Vector3f camTempP = new Vector3f();
    
    private void updateChaseCamera(float tpf) {
        //camNode.setLocalTranslation(camNode.getLocalTranslation().add(getCameraForward().mult(0.05f)));

        camTempR.set(cameraParentNode.getLocalRotation());
        camTempP.set(cameraParentNode.getLocalTranslation());

        camTempP = (shipControl.getPhysicsLocation().add(shipControl.getForwardVector().mult(-20)).add(shipControl.getUpVector().mult(6)));
        camTempR.lookAt(
                shipControl.getPhysicsLocation().add(shipControl.getUpVector()).subtract(camTempP), shipControl.getUpVector()
        );

        cameraParentNode.setLocalTranslation(cameraParentNode.getLocalTranslation().interpolate(camTempP, cameraLerpFactor * tpf));
        //cameraParentNode.setLocalRotation(cameraParentNode.);
        cameraParentNode.setLocalRotation(new Quaternion().slerp(cameraParentNode.getLocalRotation(), camTempR, cameraLerpFactor * tpf));
        
        // Check for ray intersections 5 units in the camera's look direction
        // The operation assumes getDirection is normalised, which it will be.
        //List<PhysicsRayTestResult> rayTest = bulletAppState.getPhysicsSpace().rayTest(cam.getLocation(), cam.getLocation().add(cam.getDirection().mult(camRayLength)));
        if (bulletAppState.isinitialised()) {
            List<PhysicsRayTestResult> rayTest = bulletAppState.getPhysicsSpace().rayTest(cameraParentNode.getLocalTranslation(), cameraParentNode.getLocalTranslation().add(getCameraForward().mult(camRayLength)));
            if (rayTest.size() > 0) {
                PhysicsRayTestResult get = rayTest.get(0);
                PhysicsCollisionObject collisionObject = get.getCollisionObject();
                if ("terrain".equals(((SceneNode) collisionObject.getUserObject()).getName())) {
                    camOffset = rayTest.get(0).getHitFraction();
                    camNode.setLocalTranslation((Vector3f.UNIT_Z.mult(camOffset * camRayLength)).add(Vector3f.UNIT_Z.mult(2)));
                }
            } else {
                camNode.setLocalTranslation(Vector3f.ZERO);
            }
        }
    }

    public void onAction(String binding, boolean value, float tpf) {

        if (binding.equals("Lefts")) {
            shipControl.roll(value ? -angularEngineThrust : angularEngineThrust);
        } else if (binding.equals("Rights")) {
            shipControl.roll(value ? angularEngineThrust : -angularEngineThrust);
        } else if (binding.equals("Ups")) {
            shipControl.pitch(value ? angularEngineThrust : -angularEngineThrust);
        } else if (binding.equals("Downs")) {
            shipControl.pitch(value ? -angularEngineThrust : angularEngineThrust);
        } else if (binding.equals("YawRight")) {
            shipControl.yaw(value ? -angularEngineThrust : angularEngineThrust);
        } else if (binding.equals("YawLeft")) {
            shipControl.yaw(value ? angularEngineThrust : -angularEngineThrust);
        } else if (binding.equals("Reset")) {
            if (value) {
                System.out.println("Reset");
                System.out.println("POS: " + shipControl.getPhysicsLocation() + ", ROT: " + shipControl.getPhysicsRotation());
                shipControl.setPhysicsLocation(defaultPos);
                shipControl.setPhysicsRotation(defaultRot);
                //shipControl.setPhysicsRotation(new Quaternion(0, 1, 0, -FastMath.PI));
                shipControl.clearForces();
                shipControl.setLinearVelocity(Vector3f.ZERO);
                shipControl.setAngularVelocity(Vector3f.ZERO);
                if (musicSFX.getStatus() == Status.Playing) {
                    musicSFX.stop();
                    musicSFX.play();
                }
            } else {
            }
        } else if (binding.equals("Space")) {
            if (shipControl.canFly()) {
                shipControl.setThrust(value ? mainEngineThrust : 0f);
            } else {
                shipControl.setThrust(0f);
            }
        } else if (binding.equals("Boost")) {
            if (shipControl.canFly() && value) {
                shipControl.setBoost(true);
            }
        } else if (binding.equals("Repair")) {
            // Repairs and heats up ship
            // Maybe have heat damage ship when >50, and have repair consume fuel instead?
            shipControl.setRepairStatus(value);
            if (value) {
                shipControl.setThrust(0f);
                shipControl.roll(0);
                shipControl.yaw(0);
                shipControl.pitch(0);

            }
        } else if (binding.equals("VerticalThrustUp")) {
            if (shipControl.canFly()) {
                shipControl.setVerticalThrust(value ? verticalEngineThrust : 0f);
            } else {
                shipControl.setVerticalThrust(0f);
            }
        } else if (binding.equals("VerticalThrustDown")) {
            if (shipControl.canFly()) {
                shipControl.setVerticalThrust(value ? -verticalEngineThrust : 0f);
            } else {
                shipControl.setVerticalThrust(0f);
            }
        } else if (binding.equals("Missile")) {
            if(value){
                shipControl.fireMissile(rootNode); //Target is allowed to be null
            }
        }
    }

    private void underwaterAudio(boolean submerged) {
        audioRenderer.setEnvironment(submerged ? underwaterEnv : normalEnv);
        musicSFX.setDryFilter(submerged ? new LowPassFilter(0.15f * defaultVolume, 0.05f * defaultVolume) : null);
    }

    private void AngularShake() {
        // Random between -angularIntensity and angularIntensity
        float shake = (((float) Math.random() * 2) - 1) * angularIntensity;
        camNode.setLocalRotation(new Quaternion().fromAngles(0f, 0f, shake));
    }

    private void setAngularShaking(boolean state) {
        isAngularShaking = state;
        if (!isAngularShaking) {
            camNode.setLocalRotation(Quaternion.IDENTITY);
        }
    }

    private void enableShaking() {
        isShaking = true;
    }

    private void enableAngularShaking() {
        isAngularShaking = true;
    }

    private void disableShaking() {
        isShaking = false;
        camNode.setLocalTranslation(Vector3f.ZERO);
        camNode.setLocalRotation(Quaternion.IDENTITY);
    }

    private void LinearShake() {
        Vector2f shake = new Vector2f((float) Math.random() * linearIntensity, (float) Math.random() * linearIntensity);
        //Vector3f newPosition = camNode.getLocalTranslation();
        Vector3f newPosition = camNode.getLocalTranslation();
        newPosition.x = shake.x;
        newPosition.y = shake.y;
        camNode.setLocalTranslation(newPosition);
    }

    private void setupText() {
        Material darkmat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        darkmat.setColor("Color", new ColorRGBA(0, 0, 0, 0.5f));
        darkmat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);

        GeometrySpatial darkenStats = new GeometrySpatial("StatsDarken", new Quad(200, 40));
        darkenStats.setMaterial(darkmat);
        darkenStats.setLocalTranslation(0, 20, -1);
        darkenStats.setCullHint(CullHint.Never);

        fuelText = new BitmapText(guiFont);
        fuelText.setColor(ColorRGBA.Orange);
        fuelText.setLocalTranslation(0, fuelText.getLineHeight() + 20, 0);
       // fuelText.setLocalScale(4);
        
        armourText = new BitmapText(guiFont);
        armourText.setColor(ColorRGBA.Gray);
        armourText.setLocalTranslation(0, armourText.getLineHeight() + 30, 0);
        //armourText.setLocalScale(4);
        
        heatText = new BitmapText(guiFont);
        heatText.setColor(ColorRGBA.Red);
        heatText.setLocalTranslation(0, heatText.getLineHeight() + 40, 0);
        //heatText.setLocalScale(4);

        speedText = new BitmapText(guiFont);
        speedText.setColor(ColorRGBA.Cyan);
        speedText.setLocalTranslation(0, speedText.getLineHeight() + 50, 0);

        guiNode.attachChild(fuelText);
        guiNode.attachChild(heatText);
        guiNode.attachChild(armourText);
        guiNode.attachChild(speedText);
        guiNode.attachChild(darkenStats);
    }

    float[] shipData;
    float consistency;

    @Override
    public void basicUpdate(float tpf) {
        if (shipControl != null) {
            shipControl.updateShip(tpf);
            shipData = shipControl.getShipData();
            // Converting decimals to ints to remove the excess decimal points
            // Beware off out-of-range conversions from double to int
            // TODO: Move (int) casting to the getData();
            fuelText.setText("FUEL: " + (int) shipData[0]);
            heatText.setText("HEAT: " + (int) shipData[1]);
            armourText.setText("ARMOUR: " + (int) shipData[2]);
            speedText.setText("SPEED: " + (int) shipData[3]);

            if (shipControl.getPhysicsLocation().y < WATER_HEIGHT && !underwater) {
                underwater = true;
                underwaterAudio(underwater);
            } else if (shipControl.getPhysicsLocation().y > WATER_HEIGHT && underwater) {
                underwater = false;
                underwaterAudio(underwater);
            }

            if (underwater) {
                // Add water resistance
                shipControl.setLinearVelocity(shipControl.getLinearVelocity().divide(1.01f));

                // Attempt to cool and refuel the ship in the giant ocean of petrol.
                // If the heat is too high, the fuel-use rate will be so high the refuel won't counteract it.
                shipControl.waterCool(2 / 60f);
                shipControl.refuel(9 / 60f);
            }
            
            // Maximum wind volume of 1, otherwise dependent on speed.
            // Plot in a tool like Desmos to see the curve for volume.
            if(shipControl.getSpeed() > 20f){
                windSFX.setVolume(Math.min(1, (float) Math.pow(shipControl.getSpeed() / 200, 2.5)) / 2);
            }
            else if (shipControl.getAngularVelocity().length() > 0.01f){
                windSFX.setVolume(Math.min(1f, (float) Math.pow(shipControl.getAngularVelocity().length() / 10, 2.5)) / 2);
            }
            else{
                windSFX.setVolume(0);
            }
            listener.setRotation(cam.getRotation());
            listener.setLocation(cam.getLocation());
            //hoverSFX.updateGeometricState();
            //jetSFX.updateGeometricState();
        }

        
        shipSpeed = shipControl.getLinearVelocity().length();
        if (shipSpeed >= 30f) {
            linearIntensity = 0.0005f * shipSpeed;
            angularIntensity = 0.00005f * shipSpeed;
            enableShaking();
            enableAngularShaking();
        } else {
            disableShaking();
        }

        if (isShaking) {
            LinearShake();
            if (isAngularShaking) {
                AngularShake();
            }
        }
        
        //if()
            updateChaseCamera(tpf);
        //}

        if (timeSinceLastCrash < 2f) {
            timeSinceLastCrash += tpf;
        }
        if (timeSinceLastLightCrash < 2f) {
            timeSinceLastLightCrash += tpf;
        }
        if (timeSinceLastLightCrash > 0.3f) {
            shipScrapeSFX.pause();
        } else {
            scrapeVolume = Math.min(defaultScrapeVolume * shipControl.getLinearVelocity().length() / 10f, 1f);
        }
    }

    // Rudimentary setup of audio nodes into an array
    private void setupCollisionAudio() {
        shipCollisionSFXList = new AudioNode[11];
        // Crashes
        shipCollisionSFXList[0] = new AudioNode(assetManager, "Sound/Collisions/metal_sheet_impact_hard2.wav", false);
        shipCollisionSFXList[1] = new AudioNode(assetManager, "Sound/Collisions/metal_sheet_impact_hard6.wav", false);
        shipCollisionSFXList[2] = new AudioNode(assetManager, "Sound/Collisions/metal_sheet_impact_hard7.wav", false);
        shipCollisionSFXList[3] = new AudioNode(assetManager, "Sound/Collisions/metal_sheet_impact_hard8.wav", false);
        shipCollisionSFXList[4] = new AudioNode(assetManager, "Sound/Collisions/vehicle_impact_medium1.wav", false);
        shipCollisionSFXList[5] = new AudioNode(assetManager, "Sound/Collisions/vehicle_impact_medium2.wav", false);
        shipCollisionSFXList[6] = new AudioNode(assetManager, "Sound/Collisions/vehicle_impact_medium3.wav", false);
        shipCollisionSFXList[7] = new AudioNode(assetManager, "Sound/Collisions/vehicle_impact_medium4.wav", false);
        // Scrapes
        shipCollisionSFXList[8] = new AudioNode(assetManager, "Sound/Collisions/Scrape/canister_scrape_smooth_loop1.wav", false);
        shipCollisionSFXList[9] = new AudioNode(assetManager, "Sound/Collisions/Scrape/metal_box_scrape_rough_loop1.wav", false);
        shipCollisionSFXList[10] = new AudioNode(assetManager, "Sound/Collisions/Scrape/metal_box_scrape_smooth_loop1.wav", false);
        for (AudioNode node : shipCollisionSFXList) {
            node.setReverbEnabled(false);
            node.setRefDistance(10f);
            node.setMaxDistance(25f);
            node.setPositional(true);
        }

        for (int i = 8; i < 10; i++) {
            shipCollisionSFXList[i].setLooping(true);
            shipCollisionSFXList[i].setPositional(false);
        }

        // Given that the scrape is always going to be on the ship, we don't translate it in world space like the other collision noises
        // Instead, we can just make the audionode a child of the ship, thusly inheriting the transforms of the ship.
        //((SceneNode) shipSpat).attachChild(shipScrapeSFX); // EDIT: No longer positional, trying for nonpositional instead
    }

    float crashVolume = 0.5f;
    float timeSinceLastCrash = 0f;
    // Used for differentiating between crashes and scraping
    float timeSinceLastLightCrash = 0f;
    float scrapeVolume = 0f;
    Spatial cargo;
    
    @Override
    public void collision(PhysicsCollisionEvent event) {
        try {
            if ("ship".equals(event.getNodeA().getName()) || "ship".equals(event.getNodeB().getName())) {
                //////////if ("terrain".equals(event.getNodeA().getName()) || "terrain".equals(event.getNodeB().getName())) {

                    if (timeSinceLastCrash > 0.35f) {
                        if (shipControl.getLinearVelocity().length() > 5f && event.getAppliedImpulse() > 500f) {
                            shipCollisionSFX = shipCollisionSFXList[FastMath.nextRandomInt(0, 7)];
                            crashVolume = Math.min(event.getAppliedImpulse() / 8000, defaultVolume);
                            shipCollisionSFX.setVolume(crashVolume);
                            shipCollisionSFX.setLocalTranslation("ship".equals(event.getNodeA().getName())
                                    ? event.getNodeA().getLocalTranslation()
                                    : event.getNodeB().getLocalTranslation());
                            shipCollisionSFX.setDryFilter(underwater ? new LowPassFilter(0.45f * crashVolume, 0.05f * crashVolume) : null);
                            shipCollisionSFX.play();
                            timeSinceLastCrash = 0;

                            shipControl.modifyShipArmour(-event.getAppliedImpulse() / 1000);
                        }
                    }

                    // Check every 0.25 seconds for a minor impact, and if we detect one we keep playing the scrape sound.
                    if (event.getAppliedImpulse() < 50f) {
                        // If not currently playing scrape SFX, make a new one
                        if (shipScrapeSFX.getStatus() != Status.Playing) {
                            // If it's only a brief gap between scrapes, keep the same file.
                            if (timeSinceLastLightCrash >= 0.5f) {
                                //shipScrapeSFX = shipCollisionSFXList[FastMath.nextRandomInt(8, 10)];
                                shipScrapeSFX = shipCollisionSFXList[10]; //Debug
                                shipScrapeSFX.setVolume(scrapeVolume);
                                shipScrapeSFX.setDryFilter(underwater ? new LowPassFilter(0.45f * scrapeVolume, 0.05f * scrapeVolume) : null);
                                ((SceneNode) shipSpat).attachChild(shipScrapeSFX);
                            }
                            shipScrapeSFX.play();
                            timeSinceLastLightCrash = 0f;
                        }
                        // If the time since the last scrape collision is less than 0.25f, keep playing and reset.
                        if (timeSinceLastLightCrash < 0.2f) {
                            timeSinceLastLightCrash = 0f;
                            // Update the volume to reflect our speed of scraping
                            shipScrapeSFX.setVolume(scrapeVolume);
                        } // Else, we're not scraping anymore so stop the audio.
                        // This is also checked in the update loop.
                        /*else {
                        shipScrapeSFX.stop();
                    }*/
                    }
                //////////}
                /*if("explosion ghost".equals(event.getNodeA().getName()) || "explosion ghost".equals(event.getNodeB().getName())){
                float force = 400f;//event.getNodeA().getLocalTranslation().distance(event.getNodeB().getLocalTranslation());
                //Vector3f rel_pos = new Vector3f();
                //event.getNodeA().getLocalTranslation().subtract(event.getNodeB().getLocalTranslation(), rel_pos);
                shipControl.applyCentralForce(Vector3f.UNIT_Y.mult(force));
            }*/
            }
            else if ("Cargo Holder".equals(event.getNodeA().getName()) || "Cargo Holder".equals(event.getNodeB().getName())) {
                if ("CARGO".equals(event.getNodeA().getName())){
                    if(!event.getNodeA().getControl(CargoControl.class).isConnected()){
                        cargo = event.getNodeA();
                    }
                }
                else if("CARGO".equals(event.getNodeB().getName())) {
                    if (!event.getNodeB().getControl(CargoControl.class).isConnected()) {
                        cargo = event.getNodeB();
                    }
                }
                else{
                    cargo = null;
                }
                
                if(cargo != null){
                    //cargo.getControl(CargoControl.class).setConnected(true);
                    //shipControl.attachCargo(cargo);
                }
            }
        } catch (NullPointerException exception) {
            System.out.println("Something collided with null-node object - likely a physics ghost object.");
        }
        //fpsText.setText(event.getNodeA() + ", " + event.getNodeB());
    }
}
