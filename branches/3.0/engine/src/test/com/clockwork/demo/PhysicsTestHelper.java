package com.clockwork.demo;

import com.clockwork.app.Application;
import com.clockwork.assets.TextureKey;
import com.clockwork.bullet.PhysicsSpace;
import com.clockwork.bullet.collision.shapes.CollisionShape;
import com.clockwork.bullet.collision.shapes.MeshCollisionShape;
import com.clockwork.bullet.control.RigidBodyControl;
import com.clockwork.input.MouseInput;
import com.clockwork.input.controls.ActionListener;
import com.clockwork.input.controls.MouseButtonTrigger;
import com.clockwork.light.AmbientLight;
import com.clockwork.material.Material;
import com.clockwork.math.ColorRGBA;
import com.clockwork.renderer.queue.RenderOrder.ShadowMode;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.SceneNode;
import com.clockwork.scene.shape.Box;
import com.clockwork.scene.shape.Sphere;
import com.clockwork.scene.shape.Sphere.TextureMode;
import com.clockwork.texture.Texture;
import com.clockwork.assets.AssetHandler;

public class PhysicsTestHelper {

    /**
     * creates a simple physics test world with a floor, an obstacle and some test boxes
     * @param rootNode
     * @param assetHandler
     * @param space
     */
    public static void createPhysicsTestWorld(SceneNode rootNode, AssetHandler assetHandler, PhysicsSpace space) {
        AmbientLight light = new AmbientLight();
        light.setColor(ColorRGBA.LightGray);
        rootNode.addLight(light);

        Material material = new Material(assetHandler, "Common/MatDefs/Misc/Unshaded.mdef");
        material.setTexture("ColorMap", assetHandler.loadTexture("Textures/Terrain/splat/road.jpg"));

        Box floorBox = new Box(140, 0.25f, 140);
        GeometrySpatial floorGeometry = new GeometrySpatial("Floor", floorBox);
        floorGeometry.setMaterial(material);
        floorGeometry.setLocalTranslation(0, -5, 0);
//        Plane plane = new Plane();
//        plane.setOriginNormal(new Vector3f(0, 0.25f, 0), Vector3f.UNIT_Y);
//        floorGeometry.addControl(new RigidBodyControl(new PlaneCollisionShape(plane), 0));
        floorGeometry.addControl(new RigidBodyControl(0));
        rootNode.attachChild(floorGeometry);
        space.add(floorGeometry);

        //movable boxes
        for (int i = 0; i < 12; i++) {
            Box box = new Box(0.25f, 0.25f, 0.25f);
            GeometrySpatial boxGeometry = new GeometrySpatial("Box", box);
            boxGeometry.setMaterial(material);
            boxGeometry.setLocalTranslation(i, 5, -3);
            //RigidBodyControl automatically uses box collision shapes when attached to single geometry with box mesh
            boxGeometry.addControl(new RigidBodyControl(2));
            rootNode.attachChild(boxGeometry);
            space.add(boxGeometry);
        }

        //immovable sphere with mesh collision shape
        Sphere sphere = new Sphere(8, 8, 1);
        GeometrySpatial sphereGeometry = new GeometrySpatial("Sphere", sphere);
        sphereGeometry.setMaterial(material);
        sphereGeometry.setLocalTranslation(4, -4, 2);
        sphereGeometry.addControl(new RigidBodyControl(new MeshCollisionShape(sphere), 0));
        rootNode.attachChild(sphereGeometry);
        space.add(sphereGeometry);

    }
    
    public static void createPhysicsTestWorldSoccer(SceneNode rootNode, AssetHandler assetHandler, PhysicsSpace space) {
        AmbientLight light = new AmbientLight();
        light.setColor(ColorRGBA.LightGray);
        rootNode.addLight(light);

        Material material = new Material(assetHandler, "Common/MatDefs/Misc/Unshaded.mdef");
        material.setTexture("ColorMap", assetHandler.loadTexture("Textures/Terrain/splat/road.jpg"));

        Box floorBox = new Box(20, 0.25f, 20);
        GeometrySpatial floorGeometry = new GeometrySpatial("Floor", floorBox);
        floorGeometry.setMaterial(material);
        floorGeometry.setLocalTranslation(0, -0.25f, 0);
        floorGeometry.addControl(new RigidBodyControl(0));
        rootNode.attachChild(floorGeometry);
        space.add(floorGeometry);

        //movable spheres
        for (int i = 0; i < 5; i++) {
            Sphere sphere = new Sphere(16, 16, .5f);
            GeometrySpatial ballGeometry = new GeometrySpatial("Soccer ball", sphere);
            ballGeometry.setMaterial(material);
            ballGeometry.setLocalTranslation(i, 2, -3);
            //RigidBodyControl automatically uses Sphere collision shapes when attached to single geometry with sphere mesh
            ballGeometry.addControl(new RigidBodyControl(.001f));
            ballGeometry.getControl(RigidBodyControl.class).setRestitution(1);
            rootNode.attachChild(ballGeometry);
            space.add(ballGeometry);
        }
        {
        //immovable Box with mesh collision shape
        Box box = new Box(1, 1, 1);
        GeometrySpatial boxGeometry = new GeometrySpatial("Box", box);
        boxGeometry.setMaterial(material);
        boxGeometry.setLocalTranslation(4, 1, 2);
        boxGeometry.addControl(new RigidBodyControl(new MeshCollisionShape(box), 0));
        rootNode.attachChild(boxGeometry);
        space.add(boxGeometry);
        }
        {
        //immovable Box with mesh collision shape
        Box box = new Box(1, 1, 1);
        GeometrySpatial boxGeometry = new GeometrySpatial("Box", box);
        boxGeometry.setMaterial(material);
        boxGeometry.setLocalTranslation(4, 3, 4);
        boxGeometry.addControl(new RigidBodyControl(new MeshCollisionShape(box), 0));
        rootNode.attachChild(boxGeometry);
        space.add(boxGeometry);
        }
    }

    /**
     * creates a box geometry with a RigidBodyControl
     * @param assetHandler
     * @return
     */
    public static GeometrySpatial createPhysicsTestBox(AssetHandler assetHandler) {
        Material material = new Material(assetHandler, "Common/MatDefs/Misc/Unshaded.mdef");
        material.setTexture("ColorMap", assetHandler.loadTexture("Textures/Terrain/splat/road.jpg"));
        Box box = new Box(0.25f, 0.25f, 0.25f);
        GeometrySpatial boxGeometry = new GeometrySpatial("Box", box);
        boxGeometry.setMaterial(material);
        //RigidBodyControl automatically uses box collision shapes when attached to single geometry with box mesh
        boxGeometry.addControl(new RigidBodyControl(2));
        return boxGeometry;
    }

    /**
     * creates a sphere geometry with a RigidBodyControl
     * @param assetHandler
     * @return
     */
    public static GeometrySpatial createPhysicsTestSphere(AssetHandler assetHandler) {
        Material material = new Material(assetHandler, "Common/MatDefs/Misc/Unshaded.mdef");
        material.setTexture("ColorMap", assetHandler.loadTexture("Textures/Terrain/splat/road.jpg"));
        Sphere sphere = new Sphere(8, 8, 0.25f);
        GeometrySpatial boxGeometry = new GeometrySpatial("Sphere", sphere);
        boxGeometry.setMaterial(material);
        //RigidBodyControl automatically uses sphere collision shapes when attached to single geometry with sphere mesh
        boxGeometry.addControl(new RigidBodyControl(2));
        return boxGeometry;
    }

    /**
     * creates an empty node with a RigidBodyControl
     * @param manager
     * @param shape
     * @param mass
     * @return
     */
    public static SceneNode createPhysicsTestNode(AssetHandler manager, CollisionShape shape, float mass) {
        SceneNode node = new SceneNode("PhysicsNode");
        RigidBodyControl control = new RigidBodyControl(shape, mass);
        node.addControl(control);
        return node;
    }

    /**
     * creates the necessary inputlistener and action to shoot balls from teh camera
     * @param app
     * @param rootNode
     * @param space
     */
    public static void createBallShooter(final Application app, final SceneNode rootNode, final PhysicsSpace space) {
        ActionListener actionListener = new ActionListener() {

            public void onAction(String name, boolean keyPressed, float tpf) {
                Sphere bullet = new Sphere(32, 32, 0.4f, true, false);
                bullet.setTextureMode(TextureMode.Projected);
                Material mat2 = new Material(app.getAssetHandler(), "Common/MatDefs/Misc/Unshaded.mdef");
                TextureKey key2 = new TextureKey("Textures/Terrain/Rock/Rock.PNG");
                key2.setGenerateMips(true);
                Texture tex2 = app.getAssetHandler().loadTexture(key2);
                mat2.setTexture("ColorMap", tex2);
                if (name.equals("shoot") && !keyPressed) {
                    GeometrySpatial bulletg = new GeometrySpatial("bullet", bullet);
                    bulletg.setMaterial(mat2);
                    bulletg.setShadowMode(ShadowMode.CastAndReceive);
                    bulletg.setLocalTranslation(app.getCamera().getLocation());
                    RigidBodyControl bulletControl = new RigidBodyControl(10);
                    bulletg.addControl(bulletControl);
                    bulletControl.setLinearVelocity(app.getCamera().getDirection().mult(25));
                    bulletg.addControl(bulletControl);
                    rootNode.attachChild(bulletg);
                    space.add(bulletControl);
                }
            }
        };
        app.getInputManager().addMapping("shoot", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        app.getInputManager().addListener(actionListener, "shoot");
    }
}
