package com.clockwork.demo;

import clockworktest.physics.ExplosiveControl;
import com.clockwork.assets.AssetHandler;
import com.clockwork.audio.*;
import com.clockwork.bullet.*;
import com.clockwork.bullet.control.*;
import com.clockwork.bullet.objects.*;
import com.clockwork.bullet.util.*;
import com.clockwork.effect.*;
import com.clockwork.effect.ParticleMesh.Type;
import com.clockwork.material.*;
import com.clockwork.math.*;
import com.clockwork.renderer.queue.RenderOrder.Bucket;
import com.clockwork.renderer.queue.RenderOrder.ShadowMode;
import com.clockwork.scene.*;
import java.util.*;

// Add impulse missiles, damage missiles, and drive-shutdown missiles
// If LOS is broken for 3 seconds, fly straight up.
public class Missile extends ExplosiveControl {

    SceneNode missile;
    static Spatial missile_geometry_original;
    Spatial missile_geometry;
    SceneNode missileMaster;
    PhysicsRigidBody target;
    private Quaternion objectiveRotation;
    private Quaternion missileRotation;
    private Vector3f forwardVec = new Vector3f();
    private AudioNode rocketSFX;
    private AudioNode trackingSFX;
    private Vector3f antigravity;
    private static Material smoke_mat;
    private float MISSILE_FORCE = 800f; //400
    private final float MAXIMUM_SPEED = 40;
    private boolean beingTracked = false;
    
    public boolean exists = true;
    
    //private Control targetType;
    private boolean hasTarget = false;
            
    public enum TeamTag{
        ALLIED, ENEMY, NEUTRAL
    }
    public TeamTag tag = TeamTag.NEUTRAL;

    private static AudioNode[] rocketSFXList;

    /*
                     ___
        ______     / o__>-<
   ___/  ____ \__/  /
 <_____/     \____/
    WATCH OUT! It's a Boa Constructor
    */
    
    public Missile(Spatial missileModel, AssetHandler assetHandler, PhysicsRigidBody missileTarget, SceneNode root, GeometrySpatial collision, PhysicsSpace parentSpace) {
        super(assetHandler, CollisionShapeFactory.createDynamicMeshShape(collision), 3f);

        target = missileTarget;
        
        /*if(target != null){
            //copying the target? to get class.
            targetType = target;
        }*/
        
        hasTarget = (target == null) ? false : true;
        
        this.setMass(10f);
        if (missile_geometry_original == null) {
            missile_geometry_original = assetHandler.loadModel("Models/SpaceCraft/Rocket.mesh.xml");
            missile_geometry_original.setName("Missile");
            missile_geometry_original.scale(1f);
            missile_geometry_original.rotate(0, FastMath.PI, 0);
            missile_geometry_original.centre();
            missile_geometry_original.updateGeometricState();
        }
        missile_geometry = missile_geometry_original.clone();
        
        missile = new SceneNode("Missile Node");
        missile.attachChild(missile_geometry);
        missileMaster = new SceneNode("Missile Master Node");

        if(rocketSFX == null){
            setupAudio(assetHandler);
        }

        this.setForceFactor(4000);

        //BoundingBox box = (BoundingBox) missile.getWorldBound();
        //final Vector3f extent = box.getExtent(null);
        //Vector3f pos = parent.getChild(0).getLocalTranslation().clone();
        // Quaternion rot = parent.getChild(0).getWorldRotation();
        // Vector3f dir = rot.getRotationColumn(2);

        missile.setName("Missile");
        // missile.rotate(rot);
        //missile.setLocalTranslation();
        missile.setShadowMode(ShadowMode.Cast);

        //BoxCollisionShape boxShape = new BoxCollisionShape(extent);
        //this.setLinearVelocity(dir.mult(10));
        //this.setLinearVelocity(new Vector3f(40, 0, 0));

        //this.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_03);
        
        //this.setAngularSleepingThreshold(0);
        //this.setPhysicsLocation(pos.addLocal(0, 50, 0));
        missileMaster.addControl(this);

        missileMaster.attachChild(missile);
        root.attachChild(missileMaster);
        parentSpace.addAll(missileMaster);

        antigravity = new Vector3f(0, 0f, 0);
        getPhysicsSpace().getGravity(antigravity);
        antigravity = antigravity.mult(-getMass());
        
        if (smoke_mat == null) {
            //Setting up the engine's emitters
            smoke_mat = new Material(assetHandler, "Common/MatDefs/Misc/Particle.mdef"); //Particle.mdef
            //smoke_mat.getAdditionalRenderState().setColorWrite(true);
            smoke_mat.getAdditionalRenderState().setDepthTest(true);
            smoke_mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
            smoke_mat.setTexture("Texture", assetHandler.loadTexture("Effects/Smoke/Smoke_faint.png"));
        }
        //smoke_mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
        //smoke_mat.setTexture("Texture", assetHandler.loadTexture("Effects/Smoke/Smoke_faint.png"));
        setupParticles();
    }
    
    private void setupParticles(){
        
        ParticleEmitter emit_smoke = new ParticleEmitter("Emitter", Type.Triangle, 300);
        emit_smoke.setGravity(0, 0, 0);
        emit_smoke.getParticleInfluencer().setVelocityVariation(0.01f);
        emit_smoke.setStartSize(0.6f);
        emit_smoke.setEndSize(0.1f);
        
        emit_smoke.setStartColor(ColorRGBA.DarkGray);
        emit_smoke.setEndColor(ColorRGBA.Gray);
        emit_smoke.setLowLife(0.5f);
        emit_smoke.setHighLife(0.52f);
        //emit_smoke.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 0f, 15f));
        emit_smoke.setImagesX(15);
        emit_smoke.setMaterial(smoke_mat);
        emit_smoke.setParticlesPerSec(100);
        
        emit_smoke.setQueueBucket(Bucket.Translucent);
        missileMaster.attachChild(emit_smoke);
        emit_smoke.setEmitting(false);
        emit_smoke.setEmitting(true);
    }

    private void setupAudio(AssetHandler assetHandler) {
        rocketSFXList = new AudioNode[10];

        if (rocketSFXList[0] == null) { //Cheap test to see if we've called setupAudio before
            rocketSFXList[0] = new AudioNode(assetHandler, "Sound/Missiles/rocket_jet.wav", false);
            rocketSFXList[1] = new AudioNode(assetHandler, "Sound/Missiles/explosion_hit_1.wav", false);
            rocketSFXList[2] = new AudioNode(assetHandler, "Sound/Missiles/explosion_hit_2.wav", false);
            rocketSFXList[3] = new AudioNode(assetHandler, "Sound/Missiles/explosion_hit_3.wav", false);
            rocketSFXList[4] = new AudioNode(assetHandler, "Sound/Missiles/explosion_midair_1.wav", false);
            rocketSFXList[5] = new AudioNode(assetHandler, "Sound/Missiles/explosion_midair_2.wav", false);
            rocketSFXList[6] = new AudioNode(assetHandler, "Sound/Missiles/explosion_midair_3.wav", false);
            rocketSFXList[7] = new AudioNode(assetHandler, "Sound/Missiles/explosion_water_1.wav", false);
            rocketSFXList[8] = new AudioNode(assetHandler, "Sound/Missiles/explosion_water_2.wav", false);
            rocketSFXList[9] = new AudioNode(assetHandler, "Sound/Missiles/tracking_active_loop.wav", false);

            for (AudioNode node : rocketSFXList) {
                node.setReverbEnabled(false);
                node.setRefDistance(350f);
                node.setMaxDistance(50000f);
                node.setPositional(true);
                node.setVolume(1f);
            }
        }
        
        rocketSFX = rocketSFXList[0];
        rocketSFX.setRefDistance(100f);
        rocketSFX.setMaxDistance(900f);
        rocketSFX.setVolume(0.05f);
        rocketSFX.setLooping(true);
        missileMaster.attachChild(rocketSFX);
        rocketSFX.play();
        
        trackingSFX = rocketSFXList[9];
        trackingSFX.setRefDistance(50f);
        trackingSFX.setMaxDistance(900f);
        trackingSFX.setVolume(0.05f);
        trackingSFX.setLooping(true);
        missileMaster.attachChild(trackingSFX);
        trackingSFX.play();
    }

    public boolean isBeingTracked(){
        return beingTracked;
    }
    
    public void setBeingTracked(boolean beingTracked){
        this.beingTracked = beingTracked;
    }
    
    public Vector3f getForwardVector() {
        return this.getPhysicsRotation().mult(new Vector3f(0, 0, 1));
    }

    public Vector3f getForwardVector(Vector3f input) {
        input.set(getForwardVector());
        return input;
    }
    
    float randomFactor = 5f;
    public Vector3f randomiseTargeting(){
        if(target != null){
            return new Vector3f((float) Math.random(), (float) Math.random(), (float) Math.random()).mult(randomFactor / 4);
        }
        else{
            return new Vector3f((float) Math.random(), (float) Math.random(), (float) Math.random()).mult(randomFactor);
        }
    }

    int loops = 0;
    float ran_time = 0;
    Vector3f randomVec = new Vector3f();

    @Override
    public void update(float tpf) {

        // Decay velocity - aids steering.
        //setLinearVelocity(getLinearVelocity().mult(0.98f * (144*tpf)));
        if(getLinearVelocity().length() > MAXIMUM_SPEED){
            setLinearVelocity(getLinearVelocity().normalize().mult(MAXIMUM_SPEED));
        }
        getForwardVector(forwardVec);

        super.update(tpf);
        if (target instanceof Missile ? ((Missile) target).exists : true) {
            if (target != null) {
                objectiveRotation = this.getPhysicsRotation();
                missileRotation = this.getPhysicsRotation();
                //targetRotation = new Quaternion();
                //targetRotation.lookAt(this.getPhysicsLocation(), target.getLocalTranslation());
                //missileRotation.slerp(targetRotation, tpf);
                //this.setPhysicsRotation(missileRotation);        

                // Every 2 seconds, randomise the targeting slightly to add variation
                if (ran_time > 2) {
                    randomVec = randomiseTargeting();
                    ran_time = 0;
                } else {
                    ran_time += tpf;
                }

                objectiveRotation.lookAt(target.getPhysicsLocation().subtract(getPhysicsLocation()).add(randomVec), Vector3f.UNIT_Y);

                //missileRotation.lookAt(((PhysicsShipControl)target.getControl(0)).getPhysicsLocation(), Vector3f.UNIT_Y);
                missileRotation.slerp(objectiveRotation, tpf * 13);

                this.setPhysicsRotation(missileRotation);
                missileMaster.setLocalRotation(missileRotation);
                getForwardVector(forwardVec);
            }
        } else if (hasTarget) {
            Vector3f position = getPhysicsLocation();
            TeamTag missileTag = tag;

            System.out.println("FINDING NEW TARGET");


            class Traverser implements SceneGraphTraverser {

                float minDist = 1000;
                RigidBodyControl newtarget = null;

                @Override
                public void visit(Spatial spatial) {
                    Missile missile_control = spatial.getControl(Missile.class);
                    if (missile_control != null) {
                        if (missile_control.getPhysicsLocation().distance(position) < minDist && !missile_control.isBeingTracked() && missile_control.exists && missile_control.tag != missileTag) {
                            minDist = missile_control.getPhysicsLocation().distance(position);
                            missile_control.setBeingTracked(true);
                            newtarget = missile_control;
                        }
                    }
                }

                public RigidBodyControl getTarget() {
                    if(newtarget != null){System.out.println("FOUND NEW TARGET");}
                    return newtarget;
                }
            }
            Traverser traverser = new Traverser();
            missileMaster.getParent().breadthFirstTraversal(traverser);
            target = traverser.getTarget();
        }
        this.applyCentralForce(forwardVec.mult(MISSILE_FORCE)); //600
        this.applyCentralForce(antigravity);
        //spatial.updateGeometricState();
    }

    public void blowup(boolean collision) {
        if(target != null){
            if(target instanceof Missile){
                ((Missile) target).setBeingTracked(false);
            }
        }
        
        List<Spatial> childrenList = new ArrayList<Spatial>(missileMaster.getChildren());
        for (Spatial spatial : childrenList) {
            spatial.removeFromParent();
        }

        rocketSFX.stop();
        trackingSFX.stop();
        // Use for the TestFullShip example only
        if (this.getPhysicsLocation().y < TestFullShip.WATER_HEIGHT) {
            rocketSFX = rocketSFXList[FastMath.nextRandomInt(7, 8)].clone(); //Underwater blast
        } else if (collision) {
            rocketSFX = rocketSFXList[FastMath.nextRandomInt(1, 3)].clone(); //Collision blast
        } else {
            rocketSFX = rocketSFXList[FastMath.nextRandomInt(4, 6)].clone(); //Midair blast
        }
        rocketSFX.play();
        spatial.removeFromParent();
        missileMaster.removeFromParent();
        missile_geometry.removeFromParent();
        exists = false;
        // Already done in ExplosiveControl
        this.destroy();
    }
    
    public void setMissileForce(float force){
        MISSILE_FORCE = force;
    }
}
