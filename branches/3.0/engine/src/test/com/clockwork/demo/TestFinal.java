package com.clockwork.demo;

import com.clockwork.app.BasicApplication;
import com.clockwork.bullet.*;
import com.clockwork.bullet.collision.shapes.*;
import com.clockwork.bullet.control.*;
import com.clockwork.bullet.util.*;
import com.clockwork.effect.*;
import com.clockwork.effect.shapes.*;
import com.clockwork.font.*;
import com.clockwork.light.*;
import com.clockwork.material.*;
import com.clockwork.math.*;
import com.clockwork.post.*;
import com.clockwork.post.filters.*;
import com.clockwork.renderer.queue.*;
import com.clockwork.renderer.queue.RenderOrder.ShadowMode;
import com.clockwork.scene.*;
import com.clockwork.scene.control.CameraControl.ControlDirection;
import com.clockwork.scene.shape.*;
import com.clockwork.shadow.*;
import com.clockwork.texture.*;
import com.clockwork.texture.Texture.WrapMode;
import com.clockwork.util.SkyCreator;
import com.clockwork.util.TangentBiNormalGenerator;
import com.clockwork.water.*;
import java.util.*;

public class TestFinal extends BasicApplication {
    
    private BulletAppState bulletAppState;
    private CameraNode camNode;
    boolean rotate = false;
    public float defaultTPF = 0.0f;
    public static float PIXELSMOVED_TO_RADIANSROTATED = 0.01f;
    private boolean timeSlowed;
    private HashSet<String> pressedMappings = new HashSet<String>();
    private BitmapText fireballText, 
            zawarudoText,
            shurikenText,
            jabText,
            hookText,
            heavyhitText,
            punchText;
    
    private BitmapText fighterStateText;
    private FighterNode playerNode;
    private StandNode standNode;

    //private Vector3f lightDir = new Vector3f(-1, -1, .5f).normalizeLocal();
    private Vector3f lightDir = new Vector3f(-1f, -0.2f, 1f).normalizeLocal();
    private static float WATER_HEIGHT = 9;
    private WaterFilter water;
    private SceneNode particleNode;
    private FilterPostProcessor fpp;
    private TranslucentBucketFilter tbf;
    //private DepthOfFieldFilter dofFilter;
    private FogFilter fog;
    public static void main(String[] args) {
        TestFinal app = new TestFinal();
        app.start();
    }

    public void setupSkyBox() {
        rootNode.attachChild(SkyCreator.createSky(assetManager, "Scenes/Beach/FullskiesSunset0068.dds", false));
        //rootNode.attachChild(SkyCreator.createSky(assetManager, "Textures/Sky/Bright/BrightSky.dds", false));
    }
    
    private void setupChaseCamera(SceneNode target) {
        flyCam.setEnabled(false);
        camNode = new CameraNode("CamNode", cam);
        camNode.setControlDir(ControlDirection.SpatialToCamera);
        camNode.setLocalTranslation(new Vector3f(-5, 2, 8));
        camNode.lookAt(new Vector3f(target.getLocalTranslation()).add(0, 1, 0), Vector3f.UNIT_Y);
        target.attachChild(camNode);
    }
    
    DirectionalLight dl;
    public void setupLighting() {

        dl = new DirectionalLight();
        dl.setDirection(lightDir);
        dl.setColor(new ColorRGBA(.9f, .9f, .9f, 1));
        //dl.setColor(ColorRGBA.White.clone().multLocal(2));
        rootNode.addLight(dl);
        
        AmbientLight amb = new AmbientLight();
        amb.setColor(new ColorRGBA(0.5f, 0.5f, 0.5f, 1));
        rootNode.addLight(amb);
    }
    
    private void createParticles() {
        
        Material material = new Material(assetManager, "Common/MatDefs/Misc/Particle.mdef");
        material.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/flame.png"));
        material.setFloat("Softness", 3f);       
        
        ParticleEmitter smoke = new ParticleEmitter("Smoke", ParticleMesh.Type.Triangle, 30);
        smoke.setMaterial(material);
        smoke.setShape(new EmitterSphereShape(Vector3f.ZERO, 5));
        smoke.setImagesX(1);
        smoke.setImagesY(1); // 2x2 texture animation
        smoke.setStartColor(new ColorRGBA(0.1f, 0.1f, 0.1f,1f)); // dark gray
        smoke.setEndColor(new ColorRGBA(0.5f, 0.5f, 0.5f, 0.3f)); // gray      
        smoke.setStartSize(8f);
        smoke.setEndSize(9f);
        smoke.setGravity(0, -0.001f, 0);
        smoke.setLowLife(100f);
        smoke.setHighLife(100f);
        smoke.setLocalTranslation(16f, 18f, 16f);        
        smoke.emitAllParticles();
        
        particleNode.attachChild(smoke);
        
        //fpp = new FilterPostProcessor(assetManager);        
        tbf = new TranslucentBucketFilter(true);
        fpp.addFilter(tbf);
        viewPort.addProcessor(fpp);
    }
    
    public void setupWater() {
        //FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        water = new WaterFilter(rootNode, lightDir);
        //water.setCenter(new Vector3f(16f, 3f, 16f));
        //water.setRadius(16);//260
        water.setWaveScale(0.003f);//0.003
        water.setMaxAmplitude(0.5f);//2
        water.setFoamExistence(new Vector3f(1f, 4, 0.5f));
        water.setFoamTexture((Texture2) assetManager.loadTexture("Common/MatDefs/Water/Textures/foam2.jpg"));
        water.setRefractionStrength(0.2f);
        water.setWaterHeight(WATER_HEIGHT * 2);
        water.setSpeed(0.05f);
        
        fpp.addFilter(water);
        
        viewPort.addProcessor(fpp);
    }
    
    Material mat;

    public void setupFloor() {
        mat = assetManager.loadMaterial("Textures/Terrain/BrickWall/BrickWall2.mat");
        mat.getTextureParam("DiffuseMap").getTextureValue().setWrap(WrapMode.Repeat);
        mat.getTextureParam("NormalMap").getTextureValue().setWrap(WrapMode.Repeat);

       // SceneNode floorGeom = (SceneNode) assetManager.loadAsset("Models/WaterTest/WaterTest.mesh.xml");
        //Geometry g = ((Geometry) floorGeom.getChild(0));
        //g.getMesh().scaleTextureCoordinates(new Vector2f(10, 10));
        Spatial floorMesh = assetManager.loadModel("Blender/Pool/Pool.mesh.xml");
        SceneNode floorGeom = new SceneNode("floorGeom");
        /*
        Quad q = new Quad(100, 100);
        q.scaleTextureCoordinates(new Vector2f(10, 10));
        Geometry g = new Geometry("geom", q);
        g.setLocalRotation(new Quaternion().fromAngleAxis(-FastMath.HALF_PI, Vector3f.UNIT_X));
        */
        floorGeom.attachChild(floorMesh);
        
        
        TangentBiNormalGenerator.generate(floorGeom);
        //floorGeom.setLocalTranslation(-50, 22, 60);
        floorGeom.setLocalTranslation(16f, 3f, 16f);
        floorGeom.rotate(0, FastMath.PI / 4, 0);
        floorGeom.setLocalScale(20);
        floorGeom.setMaterial(mat);        
        floorGeom.setShadowMode(ShadowMode.Receive);
        rootNode.attachChild(floorGeom);
        
        CollisionShape floorShape = CollisionShapeFactory.createMeshShape(floorGeom);
        floorMesh.addControl(new RigidBodyControl(floorShape,0));
        getPhysicsSpace().add(floorMesh);
    }
      
    public void setupPlants(){
        Spatial trunkModel = assetManager.loadModel("Blender/Foliage/foliage/tree_oak_joined.mesh.xml");
        Spatial leafModel = assetManager.loadModel("Blender/Foliage/foliage/tree_oak_joined_leaves.mesh.xml");
        TangentBiNormalGenerator.generate(leafModel);        
        TangentBiNormalGenerator.generate(trunkModel);        

        Material barkMaterial = assetManager.loadMaterial("Blender/Foliage/foliage/bark_brown.mat");
        Material leafMaterial = assetManager.loadMaterial("Blender/Foliage/foliage/tree_leaves.mat");
        
        leafMaterial.getTextureParam("DiffuseMap").getTextureValue().setWrap(WrapMode.Repeat);
        leafMaterial.getTextureParam("NormalMap").getTextureValue().setWrap(WrapMode.Repeat);
        leafMaterial.getTextureParam("AlphaMap").getTextureValue().setWrap(WrapMode.Repeat);
        barkMaterial.getTextureParam("DiffuseMap").getTextureValue().setWrap(WrapMode.Repeat);
        barkMaterial.getTextureParam("NormalMap").getTextureValue().setWrap(WrapMode.Repeat);
        leafMaterial.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
        leafMaterial.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Off);
        leafMaterial.getAdditionalRenderState().setAlphaTest(true);
        
        trunkModel.setMaterial(barkMaterial);
        leafModel.setMaterial(leafMaterial);
        
        leafModel.setQueueBucket(RenderOrder.Bucket.Transparent);

        SceneNode tree_full = new SceneNode("Complete Tree");
        
        tree_full.rotate(0, FastMath.HALF_PI, FastMath.HALF_PI);
        tree_full.setLocalTranslation(18f, 22f, 0f);
        tree_full.setLocalScale(4);
        trunkModel.setShadowMode(ShadowMode.CastAndReceive);
        leafModel.setShadowMode(ShadowMode.Cast);
        
        tree_full.attachChild(leafModel);
        tree_full.attachChild(trunkModel);
        rootNode.attachChild(tree_full);
    }

    public void setupSignpost() {
        Spatial signpost = assetManager.loadModel("Models/Sign Post/Sign Post.mesh.xml");
        Material mat = assetManager.loadMaterial("Models/Sign Post/Sign Post.mat");
        TangentBiNormalGenerator.generate(signpost);
        signpost.setMaterial(mat);
        signpost.rotate(0, FastMath.HALF_PI, 0);
        signpost.setLocalTranslation(18, 25.7f, 31);
        signpost.setLocalScale(4);
        signpost.setShadowMode(ShadowMode.CastAndReceive);
        rootNode.attachChild(signpost);
    }
    
    private void setupShadows(DirectionalLight dl){
        
        final DirectionalLightShadowRenderer pssmRenderer = new DirectionalLightShadowRenderer(assetManager, 1024, 4);
        viewPort.addProcessor(pssmRenderer);
        pssmRenderer.setLight(dl);
        pssmRenderer.setLambda(0.55f);
        pssmRenderer.setShadowIntensity(0.55f);
        pssmRenderer.setShadowCompareMode(com.clockwork.shadow.CompareMode.Software);
        pssmRenderer.setEdgeFilteringMode(EdgeFilteringMode.PCF4);
        
        //FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        LightScatteringFilter filter = new LightScatteringFilter(lightDir.multLocal(-3000));
        filter.setLightDensity(0.2f);
        fpp.addFilter(filter);
        viewPort.addProcessor(fpp);
    }
    
    private void setupFog(){
        fog=new FogFilter();
        fog.setFogColor(new ColorRGBA(0.9f, 0.9f, 0.9f, 1.0f));
        fog.setFogDistance(155);
        fog.setFogDensity(2.0f);
        fpp.addFilter(fog);
    }
   
    private void setupFightingGame() {
        setDisplayFps(false);
        setDisplayStatView(false);
        
        // Create debug text
        BitmapText helpText = new BitmapText(guiFont);
        helpText.setLocalTranslation(0, settings.getHeight(), 0);
        helpText.setText("Moves:\n" +
                "Fireball: Down, Down+Right, Right\n"+
                "Shuriken: Left, Down, Attack1(Z)\n"+
                "Jab: Attack1(Z)\n"+
                "Punch: Attack1(Z), Attack1(Z)\n"+
                "The World: Right, Heavy, Jab, Right, Special\n");
        guiNode.attachChild(helpText);
    }
    
    private void setupCharacter(){
        Spatial fighterModel = assetManager.loadModel("/Blender/2.5x/export/BaseMesh_01.mesh.xml");
        Material fighterMaterial = new Material(getAssetHandler(), "Common/MatDefs/Light/Lighting.mdef");
        fighterMaterial.setColor("Diffuse", ColorRGBA.Blue);
        fighterModel.setMaterial(fighterMaterial);
        fighterModel.setShadowMode(RenderOrder.ShadowMode.CastAndReceive);
        
        playerNode = new FighterNode("Stand User", true, fighterModel, inputManager);
        //playerNode.setJumpForces(0.001f, 0.001f);
        playerNode.setMovementSpeeds(5f, 5f);
        playerNode.setLocalScale(5f);
        getPhysicsSpace().add(playerNode.getPhysicsControl());
        rootNode.attachChild(playerNode);
        
        playerNode.getPhysicsControl().setViewDirection(new Vector3f(0,FastMath.PI / 4,0));
        playerNode.getPhysicsControl().warp(new Vector3f(0, 35, 15));
        
        setupChaseCamera(playerNode);
        
        // Allows debugging of the player character's current state.
        fighterStateText = new BitmapText(guiFont);
        fighterStateText.setColor(ColorRGBA.Orange);
        fighterStateText.setLocalTranslation(0, fighterStateText.getLineHeight(), 0);
        guiNode.attachChild(fighterStateText);
        
        // STAND
        Spatial standModel = assetManager.loadModel("/Blender/2.5x/export/BaseMesh_01.mesh.xml");
        Material standMaterial = new Material(getAssetHandler(), "Common/MatDefs/Misc/Unshaded.mdef");
        standMaterial.setColor("Color", new ColorRGBA((float) 132/255,(float)  77/255, (float) 158/255, 1));
        //standMaterial.setColor("Color", new ColorRGBA(0.231f, 0.074f, 0.342f, 1));
        standModel.setMaterial(standMaterial);
        standNode = new StandNode("Stand", standModel, playerNode);
       
        playerNode.attachChild(standNode);
        playerNode.setStand(standNode);
        standNode.setLocalTranslation(new Vector3f(0.55f,0.2f,-0.2f));
    }
    
    @Override
    public void basicInitialiseApp() {
        //cam.setLocation(new Vector3f(-15.445636f, 30.162927f, 60.252777f));
       // cam.setRotation(new Quaternion(0.05173137f, 0.92363626f, -0.13454558f, 0.35513034f));
        //flyCam.setMoveSpeed(30);
        
        // Activate Physics
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        
        particleNode = new SceneNode("particleNode");
        rootNode.attachChild(particleNode);
        fpp = new FilterPostProcessor(assetManager);

        setupLighting();
        setupSkyBox();
        setupFloor();
        setupSignpost();
        setupPlants();
        setupShadows(dl);
        setupWater();
        setupCharacter();
        createParticles();     
        //setupFog();

        setupFightingGame();

        // Disable the default FPS fly cam (Don't forget this!)
        flyCam.setEnabled(false);
    }

    @Override
    public void basicUpdate(float tpf) {
        playerNode.simpleUpdate(tpf);
        fighterStateText.setText(playerNode.fighterState.toString());
    }
    
    private PhysicsSpace getPhysicsSpace() {
        return bulletAppState.getPhysicsSpace();
    }
}
