package com.clockwork.demo;

import com.clockwork.bullet.collision.shapes.*;
import com.clockwork.bullet.control.*;
import com.clockwork.bullet.joints.*;
import com.clockwork.bullet.util.*;
import com.clockwork.math.*;
import com.clockwork.scene.*;
import com.clockwork.scene.shape.*;

public class CargoControl extends RigidBodyControl{
    
    private boolean connected = false;
    private RigidBodyControl cargoHook;
    private Point2PointJoint hookJoint;
    private SceneNode cargoHookNode;
    
    public CargoControl(Spatial spat, CollisionShape col, float mass){
        super(col, mass);
        /*GeometrySpatial geom = new GeometrySpatial("box2", new Box(1,1,1));
        cargoHookNode = new SceneNode("Hook");
        
        cargoHook = new RigidBodyControl(CollisionShapeFactory.createBoxShape(geom));
        
        cargoHook.setApplyPhysicsLocal(true);
        cargoHook.setCollisionGroup(RigidBodyControl.COLLISION_GROUP_NONE);
        cargoHook.setPhysicsLocation(this.getPhysicsLocation());
        cargoHookNode.addControl(cargoHook);
        hookJoint = new Point2PointJoint(
                    this,
                    cargoHook,
                    Vector3f.ZERO,
                    Vector3f.ZERO//cargo.getControl(CargoControl.class).getPhysicsLocation().subtract(getPhysicsLocation()) // this -> cargo vector
            );
        
        hookJoint.setCollisionBetweenLinkedBodys(false);*/
        
        //((SceneNode) spat).attachChild(cargoHookNode); // Not needed: cargoHook is already connected by hookJoint
        spat.addControl(this);
        spat.centre();
        //getPhysicsSpace().add(cargoHook);
        //getPhysicsSpace().add(hookJoint);
    }
    
    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
    
    public RigidBodyControl getHook(){
        return cargoHook;
    }
    
    public Spatial getHookSpat(){
        return cargoHookNode;
    }
    
    @Override
    public void update(float tpf){
        super.update(tpf);
    }
}
