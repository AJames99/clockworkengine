package com.clockwork.demo;

import com.clockwork.assets.AssetHandler;
import com.clockwork.bullet.collision.*;
import com.clockwork.bullet.collision.shapes.*;
import com.clockwork.bullet.control.*;
import com.clockwork.bullet.objects.*;
import com.clockwork.bullet.util.*;
import com.clockwork.demo.Missile.TeamTag;
import com.clockwork.material.*;
import com.clockwork.math.*;
import com.clockwork.renderer.queue.RenderOrder.ShadowMode;
import com.clockwork.scene.*;
import com.clockwork.scene.shape.*;
import com.clockwork.texture.*;

// Turret spatial child
// Missiles should be children of this
// this should be immovable

public class Turret extends RigidBodyControl implements PhysicsControl{

    private Spatial missile;
    //turretSpat parent of frame, frame parent of body.
    private GeometrySpatial body_geom;
    private GeometrySpatial frame_geom;
    private SceneNode body_node;
    private SceneNode frame_node;
    
    // Loaded only when not yet already loaded, should be used by all turrets.
    public static SceneNode turret_geom = null;
    public static Texture body_tex;
    public static Texture frame_tex;
    public static Material body_mat;
    public static Material frame_mat;
    
    private SceneNode turretSpace;
    private PhysicsRigidBody target;
    private AssetHandler assetHandler;
    public TeamTag tag;

    public Turret(AssetHandler assetHandler) {
        
        super(new MeshCollisionShape(new Box(5f, 5f, 5f)), 0);
        this.assetHandler = assetHandler;
        
        tag = TeamTag.ENEMY;
        
        turretSpace = new SceneNode("Master");
        
        // Load the static assets
        //body_mat = (body_mat == null) ? assetHandler.loadMaterial("Blender/T7/T7.mat") : body_mat;
        //body_tex = (body_tex == null) ? assetHandler.loadTexture("Blender/T7/ComboBake.png") : body_tex;        
        //turret_geom = (turret_geom == null) ? (SceneNode)assetHandler.loadModel("Blender/turret.blend") : turret_geom;
        //if(turret_geom == null){
            turret_geom = (SceneNode) assetHandler.loadModel("Blender/turret.blend");
        //}
        
        
        // Shouldn't have to split the materials or textures, should be handled in Blender asset handler
        body_geom = findGeom(turret_geom, "BodyGeometry1");
        frame_geom = findGeom(turret_geom, "FrameGeometry1");
        
        //Hierarchy
        frame_node = new SceneNode("Turret Frame");
        body_node = new SceneNode("Turret Body");
        frame_node.attachChild(frame_geom);
        body_node.attachChild(body_geom);
        
        ((SceneNode) turretSpace).attachChild(frame_node);
        frame_node.attachChild(body_node);
        
        
        turretSpace.setName("turret");
        Spatial turretHullShape = assetHandler.loadModel("Blender/turret.blend");
        
        //body_mat.getTextureParam("DiffuseMap").setTextureValue(body_tex);

        turretSpace.setShadowMode(ShadowMode.CastAndReceive);
        //CollisionShape colShape = CollisionShapeFactory.createDynamicMeshShape(turretHullShape);
        turretSpace.addControl(this);
        
        this.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_03);
        
        // Do in main loop
        //getPhysicsSpace().add(turretSpat);
    }
    
    private GeometrySpatial findGeom(Spatial spatial, String name) {
        if (spatial instanceof SceneNode) {
            SceneNode node = (SceneNode) spatial;
            for (int i = 0; i < node.getQuantity(); i++) {
                Spatial child = node.getChild(i);
                GeometrySpatial result = findGeom(child, name);
                if (result != null) {
                    return result;
                }
            }
        } else if (spatial instanceof GeometrySpatial) {
            if (spatial.getName().contains(name)) {
                return (GeometrySpatial) spatial;
            }
            else {
                return null;
            }
        }
        return null;
    }
    
    public SceneNode getMasterNode(){
        return turretSpace;
    }
    
    float attackCooldown = 30f; // 2 per 30 seconds
    float antiAirCooldown = 5f; // 2 per 35 seconds
    float cooldown = 0;
    float shotsFired = 0;
    
    
    @Override
    public void update(float tpf) {
        super.update(tpf);

        // Check NPC logic
        // Rotate
        // Missile 'recharge' occurs all the time, but the turret only fires missiles
        // when the target is within 1000 units.
        if (cooldown < ((tag == TeamTag.ENEMY) ? attackCooldown : antiAirCooldown)) {
            cooldown += tpf;
        }
        switch (tag) {
            case ALLIED:
                if (shotsFired < 2 && cooldown >= antiAirCooldown) {
                    fireAntiMissile(getMasterNode().getParent());
                    cooldown = antiAirCooldown - 0.8f;
                    shotsFired++;
                } else if (cooldown >= antiAirCooldown) {
                    cooldown = 0f;
                    shotsFired = 0f;
                }
                break;
            case ENEMY:
                if (target != null && target.getPhysicsLocation().distance(this.getPhysicsLocation()) < 10000f) { //1000
                    if (shotsFired < 2 && cooldown >= attackCooldown) {
                        fireMissile(target);
                        cooldown = attackCooldown - 1f;
                        shotsFired++;
                    } else if (cooldown >= attackCooldown) {
                        cooldown = 0f;
                        shotsFired = 0f;
                    }
                }
                break;
        }
        
        if(target != null){
            Vector3f direction = new Vector3f(target.getPhysicsLocation().subtract(this.getPhysicsLocation()));
            
            Quaternion rot_quat = new Quaternion();
            rot_quat.lookAt(direction, Vector3f.UNIT_Y);
            
            float[] ANGLES = new float[3];
            rot_quat.toAngles(ANGLES);
            // 0 pitch, 1 yaw, 2 roll - Allows elimination of roll, pitch, and yaw selectively
            frame_node.setLocalRotation(new Quaternion().fromAngles(0, ANGLES[1], 0));
            body_node.setLocalRotation(new Quaternion().fromAngles(ANGLES[0], 0, 0));
        }
    }
    
    public void setTarget(PhysicsRigidBody target){
        this.target = target;
    }
    
    public void setTag(TeamTag tag){
        this.tag = tag;
    }
    public TeamTag getTag(){
        return tag;
    }

    // Missiles are parented to this Turret object, not turretSpat
    public void fireMissile(PhysicsRigidBody target) {
        Missile missileInstance = new Missile(missile, assetHandler, target, getMasterNode(), new GeometrySpatial("box", new Box(Vector3f.ZERO, 2,2,2)), getPhysicsSpace());        
        missileInstance.setPhysicsLocation(this.getPhysicsLocation().add(0, 10, 0));
        missileInstance.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_03);
        missileInstance.tag = tag;
        missileInstance.setMissileForce(700f);
        missileInstance.setMaxTime(60f);
        
    }
    
    public void fireAntiMissile(SceneNode root) {
        Vector3f position = getPhysicsLocation();
        TeamTag turretTag = tag;

        class Traverser implements SceneGraphTraverser {
            float minDist = 10000; //1000
            RigidBodyControl newtarget;
            @Override
            public void visit(Spatial spatial) {
                Missile missile_control = spatial.getControl(Missile.class);
                if (missile_control != null) {
                    if (missile_control.getPhysicsLocation().distance(position) < minDist && !missile_control.isBeingTracked() && missile_control.exists && missile_control.tag != turretTag) {
                        minDist = missile_control.getPhysicsLocation().distance(position);
                        missile_control.setBeingTracked(true);
                        newtarget = missile_control;
                    }
                }
            }

            public RigidBodyControl getTarget() {
                //System.out.println(newtarget.getPhysicsLocation());
                return newtarget;
            }
        }
        Traverser traverser = new Traverser();

        root.breadthFirstTraversal(traverser);
        //TODO don't set cooldowns to 0 or add to shots fired if we don't actually fire.
        if (traverser.getTarget() != null) {
            target = traverser.getTarget();
            Missile missileInstance = new Missile(missile, assetHandler, target, root, new GeometrySpatial("box", new Box(Vector3f.ZERO, 2, 2, 2)), getPhysicsSpace());
            missileInstance.setPhysicsLocation(this.getPhysicsLocation().add(0, 10, 0));
            //missileInstance.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_02);
            missileInstance.tag = tag;
            missileInstance.setMissileForce(1000f);
        }
    }
}
