

package clockworktest.water;

import com.clockwork.app.BasicApplication;
import com.clockwork.input.KeyInput;
import com.clockwork.input.controls.ActionListener;
import com.clockwork.input.controls.KeyTrigger;
import com.clockwork.material.Material;
import com.clockwork.math.Vector3f;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.SceneNode;
import com.clockwork.scene.Spatial;
import com.clockwork.scene.shape.Box;
import com.clockwork.scene.shape.Sphere;
import com.clockwork.util.SkyCreator;
import com.clockwork.water.BasicWaterProcessor;

/**
 * Demonstrates the water processor, with simple lighting and reflections.
 */
public class TestSimpleWater extends BasicApplication implements ActionListener {

    Material mat;
    Spatial waterPlane;
    GeometrySpatial lightSphere;
    BasicWaterProcessor waterProcessor;
    SceneNode sceneNode;
    boolean useWater = true;
    private Vector3f lightPos =  new Vector3f(33,12,-29);


    public static void main(String[] args) {
        TestSimpleWater app = new TestSimpleWater();
        app.start();
    }

    @Override
    public void basicInitialiseApp() {
        initInput();
        initScene();

        waterProcessor = new BasicWaterProcessor(assetManager);
        waterProcessor.setReflectionScene(sceneNode);
        waterProcessor.setDebug(true);
        viewPort.addProcessor(waterProcessor);

        waterProcessor.setLightPosition(lightPos);

        //waterPlane = waterProcessor.createWaterGeometry(100, 100);
        waterPlane=(Spatial)  assetManager.loadModel("Models/WaterTest/WaterTest.mesh.xml");
        waterPlane.setMaterial(waterProcessor.getMaterial());
        waterPlane.setLocalScale(40);
        waterPlane.setLocalTranslation(-5, 0, 5);

        rootNode.attachChild(waterPlane);
    }

    private void initScene() {
        //init cam location
        cam.setLocation(new Vector3f(0, 10, 10));
        cam.lookAt(Vector3f.ZERO, Vector3f.UNIT_Y);
        //init scene
        sceneNode = new SceneNode("Scene");
        mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        mat.setTexture("ColorMap", assetManager.loadTexture("Interface/Logo/Gear.png"));
        Box b = new Box(1, 1, 1);
        GeometrySpatial geom = new GeometrySpatial("Box", b);
        geom.setMaterial(mat);
        sceneNode.attachChild(geom);

        // load sky
        sceneNode.attachChild(SkyCreator.createSky(assetManager, "Textures/Sky/Bright/BrightSky.dds", false));
        rootNode.attachChild(sceneNode);

        //add lightPos GeometrySpatial
        Sphere lite=new Sphere(8, 8, 3.0f);
        lightSphere=new GeometrySpatial("lightsphere", lite);
        lightSphere.setMaterial(mat);
        lightSphere.setLocalTranslation(lightPos);
        rootNode.attachChild(lightSphere);
    }

    protected void initInput() {
        flyCam.setMoveSpeed(3);
        //init input
        inputManager.addMapping("toggle_water", new KeyTrigger(KeyInput.KEY_O));
        inputManager.addListener(this, "toggle_water");
        inputManager.addMapping("light_up", new KeyTrigger(KeyInput.KEY_T));
        inputManager.addListener(this, "light_up");
        inputManager.addMapping("light_down", new KeyTrigger(KeyInput.KEY_G));
        inputManager.addListener(this, "light_down");
        inputManager.addMapping("light_left", new KeyTrigger(KeyInput.KEY_H));
        inputManager.addListener(this, "light_left");
        inputManager.addMapping("light_right", new KeyTrigger(KeyInput.KEY_K));
        inputManager.addListener(this, "light_right");
        inputManager.addMapping("light_forward", new KeyTrigger(KeyInput.KEY_U));
        inputManager.addListener(this, "light_forward");
        inputManager.addMapping("light_back", new KeyTrigger(KeyInput.KEY_J));
        inputManager.addListener(this, "light_back");
    }

    @Override
    public void basicUpdate(float tpf) {
        fpsText.setText("Light Position: "+lightPos.toString()+" Change Light location with U, H, J, K and T, G. Toggle water with O");
        lightSphere.setLocalTranslation(lightPos);
        waterProcessor.setLightPosition(lightPos);
    }

    public void onAction(String name, boolean value, float tpf) {
        if (name.equals("toggle_water") && value) {
            if (!useWater) {
                useWater = true;
                waterPlane.setMaterial(waterProcessor.getMaterial());
            } else {
                useWater = false;
                waterPlane.setMaterial(mat);
            }
        } else if (name.equals("light_up") && value) {
            lightPos.y++;
        } else if (name.equals("light_down") && value) {
            lightPos.y--;
        } else if (name.equals("light_left") && value) {
            lightPos.x--;
        } else if (name.equals("light_right") && value) {
            lightPos.x++;
        } else if (name.equals("light_forward") && value) {
            lightPos.z--;
        } else if (name.equals("light_back") && value) {
            lightPos.z++;
        }
    }
}
