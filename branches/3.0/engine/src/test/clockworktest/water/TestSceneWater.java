package clockworktest.water;

import com.clockwork.app.BasicApplication;
import com.clockwork.assets.plugins.ZipLocator;
import com.clockwork.light.DirectionalLight;
import com.clockwork.material.Material;
import com.clockwork.math.*;
import com.clockwork.renderer.queue.RenderOrder.ShadowMode;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.SceneNode;
import com.clockwork.scene.Spatial;
import com.clockwork.scene.shape.Quad;
import com.clockwork.scene.shape.Sphere;
import com.clockwork.util.SkyCreator;
import com.clockwork.water.BasicWaterProcessor;
import java.io.File;

public class TestSceneWater extends BasicApplication {

    public static void main(String[] args) {

        TestSceneWater app = new TestSceneWater();
        app.start();
    }

    public void basicInitialiseApp() {
        this.flyCam.setMoveSpeed(10);
        SceneNode mainScene = new SceneNode();
        cam.setLocation(new Vector3f(-27.0f, 1.0f, 75.0f));
        cam.setRotation(new Quaternion(0.03f, 0.9f, 0f, 0.4f));

        mainScene.attachChild(SkyCreator.createSky(assetManager, "Textures/Sky/Bright/BrightSky.dds", false));

        assetManager.registerLocator("wildhouse.zip", ZipLocator.class);

        Spatial scene = assetManager.loadModel("main.scene");

        DirectionalLight sun = new DirectionalLight();
        Vector3f lightDir = new Vector3f(-0.37352666f, -0.50444174f, -0.7784704f);
        sun.setDirection(lightDir);
        sun.setColor(ColorRGBA.White.clone().multLocal(2));
        scene.addLight(sun);

        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        mat.setTexture("ColorMap", assetManager.loadTexture("Interface/Logo/Gear.png"));
        Sphere lite = new Sphere(8, 8, 3.0f);
        GeometrySpatial lightSphere = new GeometrySpatial("lightsphere", lite);
        lightSphere.setMaterial(mat);
        Vector3f lightPos = lightDir.multLocal(-400);
        lightSphere.setLocalTranslation(lightPos);
        rootNode.attachChild(lightSphere);

        BasicWaterProcessor waterProcessor = new BasicWaterProcessor(assetManager);
        waterProcessor.setReflectionScene(mainScene);
        waterProcessor.setDebug(false);
        waterProcessor.setLightPosition(lightPos);
        waterProcessor.setRefractionClippingOffset(1.0f);

        Vector3f waterLocation = new Vector3f(0, -20, 0);
        waterProcessor.setPlane(new Plane(Vector3f.UNIT_Y, waterLocation.dot(Vector3f.UNIT_Y)));
        WaterUI waterUi = new WaterUI(inputManager, waterProcessor);
        waterProcessor.setWaterColor(ColorRGBA.Brown);
        waterProcessor.setDebug(true);
        // Lower render sizes give higher performance
//        waterProcessor.setRenderSize(128,128);
//        waterProcessor.setWaterDepth(20);
//        waterProcessor.setDistortionScale(0.1f);
//        waterProcessor.setWaveSpeed(0.01f);

        Quad quad = new Quad(400, 400);

        quad.scaleTextureCoordinates(new Vector2f(6f, 6f));

        GeometrySpatial water = new GeometrySpatial("water", quad);
        water.setShadowMode(ShadowMode.Receive);
        water.setLocalRotation(new Quaternion().fromAngleAxis(-FastMath.HALF_PI, Vector3f.UNIT_X));
        water.setMaterial(waterProcessor.getMaterial());
        water.setLocalTranslation(-200, -20, 250);

        rootNode.attachChild(water);

        viewPort.addProcessor(waterProcessor);

        mainScene.attachChild(scene);
        rootNode.attachChild(mainScene);
    }
}
