
package clockworktest.physics;

import com.clockwork.demo.PhysicsShipControl;
import com.clockwork.demo.Missile;
import com.clockwork.bullet.PhysicsSpace;
import com.clockwork.bullet.PhysicsTickListener;
import com.clockwork.bullet.collision.PhysicsCollisionEvent;
import com.clockwork.bullet.collision.PhysicsCollisionListener;
import com.clockwork.bullet.collision.PhysicsCollisionObject;
import com.clockwork.bullet.collision.shapes.CollisionShape;
import com.clockwork.bullet.collision.shapes.SphereCollisionShape;
import com.clockwork.bullet.control.RigidBodyControl;
import com.clockwork.bullet.objects.PhysicsGhostObject;
import com.clockwork.bullet.objects.PhysicsRigidBody;
import com.clockwork.effect.ParticleEmitter;
import com.clockwork.effect.ParticleMesh.Type;
import com.clockwork.effect.shapes.EmitterSphereShape;
import com.clockwork.export.CWExporter;
import com.clockwork.export.CWImporter;
import com.clockwork.material.Material;
import com.clockwork.math.ColorRGBA;
import com.clockwork.math.Vector3f;
import com.clockwork.renderer.queue.RenderOrder.Bucket;
import com.clockwork.scene.*;
import java.io.IOException;
import java.util.Iterator;
import com.clockwork.assets.AssetHandler;

public class ExplosiveControl extends RigidBodyControl implements PhysicsCollisionListener, PhysicsTickListener {

    private float explosionRadius = 10;
    private PhysicsGhostObject ghostObject;
    private Vector3f vector = new Vector3f();
    private Vector3f vector2 = new Vector3f();
    private float forceFactor = 1;
    private ParticleEmitter effect;
    private float fxTime = 0.5f;
    private float maxTime = 30f;//8f; // 30 Seconds
    private float curTime = -1.0f;
    private float timer;

    public ExplosiveControl(CollisionShape shape, float mass) {
        super(shape, mass);
        createGhostObject();
    }

    public ExplosiveControl(AssetHandler manager, CollisionShape shape, float mass) {
        super(shape, mass);
        createGhostObject();
        prepareEffect(manager);
    }

    public void setPhysicsSpace(PhysicsSpace space) {
        super.setPhysicsSpace(space);
        if (space != null) {
            space.addCollisionListener(this);
        }
    }

    private void prepareEffect(AssetHandler assetHandler) {
        int COUNT_FACTOR = 1;
        float COUNT_FACTOR_F = 1f;
        effect = new ParticleEmitter("Flame", Type.Triangle, 32 * COUNT_FACTOR);
        effect.setSelectRandomImage(true);
        effect.setStartColor(new ColorRGBA(1f, 0.4f, 0.05f, (float) (1f / COUNT_FACTOR_F)));
        effect.setEndColor(new ColorRGBA(.4f, .22f, .12f, 0f));
        effect.setStartSize(1.3f);
        effect.setEndSize(2f);
        effect.setShape(new EmitterSphereShape(Vector3f.ZERO, 1f));
        effect.setParticlesPerSec(0);
        effect.setGravity(0, -5f, 0);
        effect.setLowLife(.4f);
        effect.setHighLife(.5f);
        effect.setInitialVelocity(new Vector3f(0, 7, 0));
        effect.setVelocityVariation(1f);
        effect.setImagesX(2);
        effect.setImagesY(2);
        Material mat = new Material(assetHandler, "Common/MatDefs/Misc/Particle.mdef");
        mat.setTexture("Texture", assetHandler.loadTexture("Effects/Explosion/flame.png"));
        effect.setMaterial(mat);
        effect.setQueueBucket(Bucket.Translucent);
    }

    protected void createGhostObject() {
        ghostObject = new PhysicsGhostObject(new SphereCollisionShape(explosionRadius));
        // Necessary if we want to detect if the collision is with the ghostObject
        // as physics objects are unnamed
        Spatial namedObject = new SceneNode("explosion ghost");
    }
    
    public void setMaxTime(float maxTime){
        this.maxTime = maxTime;
    }
    
    public float getMaxTime(){
        return maxTime;
    }

    public void collision(PhysicsCollisionEvent event) {
        if (space == null) {
            return;
        }
        if (event.getObjectA() == this || event.getObjectB() == this) {
            space.add(ghostObject);
            ghostObject.setPhysicsLocation(getPhysicsLocation(vector));
            space.addTickListener(this);
            if (effect != null && spatial.getParent() != null) {
                curTime = 0;
                effect.setLocalTranslation(spatial.getLocalTranslation());
                spatial.getParent().attachChild(effect);
                effect.emitAllParticles();
            }
            if(this instanceof Missile){
                ((Missile) this).blowup(true);
            }
            space.remove(this);
            spatial.removeFromParent();
            this.destroy();
        }
    }
    
    public void prePhysicsTick(PhysicsSpace space, float f) {
        space.removeCollisionListener(this);
    }

    public void physicsTick(PhysicsSpace space, float f) {
        //get all overlapping objects and apply impulse to them
        for (Iterator<PhysicsCollisionObject> it = ghostObject.getOverlappingObjects().iterator(); it.hasNext();) {            
            PhysicsCollisionObject physicsCollisionObject = it.next();
            if (physicsCollisionObject instanceof PhysicsRigidBody && !(physicsCollisionObject instanceof Missile)) {
                PhysicsRigidBody rBody = (PhysicsRigidBody) physicsCollisionObject;
                rBody.getPhysicsLocation(vector2);
                vector2.subtractLocal(vector);
                float force = explosionRadius - vector2.length();
                force *= forceFactor;
                force = force > 0 ? force : 0;
                vector2.normalizeLocal();
                vector2.multLocal(force);
                ((PhysicsRigidBody) physicsCollisionObject).applyImpulse(vector2, Vector3f.ZERO);
                if(physicsCollisionObject instanceof PhysicsShipControl){
                    ((PhysicsShipControl) physicsCollisionObject).applyTorqueImpulse(vector2.mult(0.2f));
                    ((PhysicsShipControl) physicsCollisionObject).modifyShipArmour(-vector2.length() / 600);
                }
            }
        }
        space.removeTickListener(this);
        space.remove(ghostObject);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        if(enabled){
            timer+=tpf;
            if(timer>maxTime){
                if(spatial.getParent()!=null){
                    space.removeCollisionListener(this);
                    if(this instanceof Missile){
                        ((Missile) this).blowup(false);
                    }
                    space.remove(this);
                    spatial.removeFromParent();
                }
            }
        }
        if (enabled && curTime >= 0) {
            curTime += tpf;
            if (curTime > fxTime) {
                curTime = -1;
                effect.removeFromParent();
            }
        }
    }

    /**
     * @return the explosionRadius
     */
    public float getExplosionRadius() {
        return explosionRadius;
    }

    /**
     * @param explosionRadius the explosionRadius to set
     */
    public void setExplosionRadius(float explosionRadius) {
        this.explosionRadius = explosionRadius;
        createGhostObject();
    }

    public float getForceFactor() {
        return forceFactor;
    }

    public void setForceFactor(float forceFactor) {
        this.forceFactor = forceFactor;
    }
    
    
    @Override
    public void read(CWImporter im) throws IOException {
        throw new UnsupportedOperationException("Reading not supported.");
    }

    @Override
    public void write(CWExporter ex) throws IOException {
        throw new UnsupportedOperationException("Saving not supported.");
    }
}
