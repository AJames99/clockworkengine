

package clockworktest.collision;

import com.clockwork.app.BasicApplication;
import com.clockwork.collision.CollisionResult;
import com.clockwork.collision.CollisionResults;
import com.clockwork.light.DirectionalLight;
import com.clockwork.material.Material;
import com.clockwork.math.ColorRGBA;
import com.clockwork.math.Quaternion;
import com.clockwork.math.Ray;
import com.clockwork.math.Vector3f;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.SceneNode;
import com.clockwork.scene.Spatial;
import com.clockwork.scene.debug.Arrow;
import com.clockwork.scene.shape.Box;

public class TestMousePick extends BasicApplication {

    public static void main(String[] args) {
        TestMousePick app = new TestMousePick();
        app.start();
    }
    
    SceneNode scene_objects;
    GeometrySpatial arrow;

    @Override
    public void basicInitialiseApp() {
        flyCam.setEnabled(false);
        initMark();

        scene_objects = new SceneNode("Shootables");
        rootNode.attachChild(scene_objects);
        scene_objects.attachChild(makeCube("Object A", -2f, 0f, 1f));
        scene_objects.attachChild(makeCube("Object B", 1f, -2f, 0f));
        scene_objects.attachChild(makeCube("Object C", 0f, 1f, -2f));
        scene_objects.attachChild(makeCube("Object D", 1f, 0f, -4f));
        scene_objects.attachChild(makeFloor());
        scene_objects.attachChild(makeComplexMesh());
    }

    @Override
    public void basicUpdate(float tpf){
        Vector3f origin = cam.getWorldCoordinates(inputManager.getCursorPosition(), 0.0f);
        Vector3f direction = cam.getWorldCoordinates(inputManager.getCursorPosition(), 0.3f);
        direction.subtractLocal(origin).normalizeLocal();

        Ray ray = new Ray(origin, direction);
        CollisionResults results = new CollisionResults();
        scene_objects.collideWith(ray, results);
        if (results.size() > 0) {
            CollisionResult closest = results.getClosestCollision();
            arrow.setLocalTranslation(closest.getContactPoint());

            Quaternion q = new Quaternion();
            q.lookAt(closest.getContactNormal(), Vector3f.UNIT_Y);
            arrow.setLocalRotation(q);

            rootNode.attachChild(arrow);
        } else {
            rootNode.detachChild(arrow);
        }
    }
 
    protected GeometrySpatial makeCube(String name, float x, float y, float z) {
        Box box = new Box(new Vector3f(x, y, z), 1, 1, 1);
        GeometrySpatial cube = new GeometrySpatial(name, box);
        Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        mat1.setColor("Color", ColorRGBA.randomColor());
        cube.setMaterial(mat1);
        return cube;
    }

    /** A floor to show that the "shot" can go through several objects. */
    protected GeometrySpatial makeFloor() {
        Box box = new Box(new Vector3f(0, -4, -5), 15, .2f, 15);
        GeometrySpatial floor = new GeometrySpatial("the Floor", box);
        Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        mat1.setColor("Color", ColorRGBA.Gray);
        floor.setMaterial(mat1);
        return floor;
    }

    protected void initMark() {
        Arrow arrow = new Arrow(Vector3f.UNIT_Z.mult(2f));
        arrow.setLineWidth(3);

        //Sphere sphere = new Sphere(30, 30, 0.2f);
        this.arrow = new GeometrySpatial("Shot", arrow);
        //mark = new GeometrySpatial("BOOM!", sphere);
        Material mark_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        mark_mat.setColor("Color", ColorRGBA.Red);
        this.arrow.setMaterial(mark_mat);
    }

    protected Spatial makeComplexMesh() {
        Spatial mesh = assetManager.loadModel("Models/Ferrari/Car.mesh.xml");
        mesh.scale(0.5f);
        mesh.setLocalTranslation(-1.0f, -1.5f, -0.6f);

        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.1f, -0.7f, -1.0f).normalizeLocal());
        mesh.addLight(sun);
        return mesh;
    }
}
