package clockworktest.batching;

import com.clockwork.app.BasicApplication;
import com.clockwork.bounding.BoundingBox;
import com.clockwork.light.DirectionalLight;
import com.clockwork.material.Material;
import com.clockwork.math.ColorRGBA;
import com.clockwork.math.FastMath;
import com.clockwork.math.Quaternion;
import com.clockwork.math.Vector3f;
import com.clockwork.scene.BatchNode;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.SceneNode;
import com.clockwork.scene.Spatial;
import com.clockwork.scene.debug.WireFrustum;
import com.clockwork.scene.shape.Box;
import com.clockwork.system.NanosecondTimer;
import com.clockwork.util.TangentBiNormalGenerator;

public class TestBatchNode extends BasicApplication {

    public static void main(String[] args) {

        TestBatchNode app = new TestBatchNode();
        app.start();
    }
    BatchNode batch;
    WireFrustum frustum;
    GeometrySpatial frustumMdl;
    private Vector3f[] points;

    {
        points = new Vector3f[8];
        for (int i = 0; i < points.length; i++) {
            points[i] = new Vector3f();
        }
    }

    @Override
    public void basicInitialiseApp() {
        batch = new BatchNode("Batch node");

        /**
         * A simple cube with a colour bleeding through a transparent texture.
         */
        Box boxshape4 = new Box(Vector3f.ZERO, 1f, 1f, 1f);
        cube = new GeometrySpatial("cube1", boxshape4);
        Material mat = assetManager.loadMaterial("Textures/Terrain/Pond/Pond.mat");
        cube.setMaterial(mat);

        Box box = new Box(Vector3f.ZERO, 1f, 1f, 1f);
        cube2 = new GeometrySpatial("cube2", box);
        cube2.setMaterial(mat);

        TangentBiNormalGenerator.generate(cube);
        TangentBiNormalGenerator.generate(cube2);


        n = new SceneNode("aNode");
        batch.attachChild(cube);
        batch.batch();
        rootNode.attachChild(batch);
        cube.setLocalTranslation(3, 0, 0);
        cube2.setLocalTranslation(0, 20, 0);


        updateBoundPoints(points);
        frustum = new WireFrustum(points);
        frustumMdl = new GeometrySpatial("f", frustum);
        frustumMdl.setCullHint(Spatial.CullHint.Never);
        frustumMdl.setMaterial(new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef"));
        frustumMdl.getMaterial().getAdditionalRenderState().setWireframe(true);
        frustumMdl.getMaterial().setColor("Color", ColorRGBA.Green);
        rootNode.attachChild(frustumMdl);
        dl = new DirectionalLight();
        dl.setColor(ColorRGBA.White.mult(2));
        dl.setDirection(new Vector3f(1, -1, -1));
        rootNode.addLight(dl);
        flyCam.setMoveSpeed(10);
    }
    SceneNode n;
    GeometrySpatial cube;
    GeometrySpatial cube2;
    float time = 0;
    DirectionalLight dl;
    boolean done = false;

    @Override
    public void basicUpdate(float tpf) {
        // First update, attach the second cube and batch
        if (!done) {
            done = true;
            batch.attachChild(cube2);
            batch.batch();
        }
        updateBoundPoints(points);
        frustum.update(points);
        time += tpf;
        dl.setDirection(cam.getDirection());
        cube2.setLocalTranslation(FastMath.sin(-time) * 3, FastMath.cos(time) * 3, 0);
        cube2.setLocalRotation(new Quaternion().fromAngleAxis(time, Vector3f.UNIT_Z));
        cube2.setLocalScale(Math.max(FastMath.sin(time), 0.5f));
    }

    public void updateBoundPoints(Vector3f[] points) {
        BoundingBox bb = (BoundingBox) batch.getWorldBound();
        float xe = bb.getXExtent();
        float ye = bb.getYExtent();
        float ze = bb.getZExtent();
        float x = bb.getCenter().x;
        float y = bb.getCenter().y;
        float z = bb.getCenter().z;

        points[0].set(new Vector3f(x - xe, y - ye, z - ze));
        points[1].set(new Vector3f(x - xe, y + ye, z - ze));
        points[2].set(new Vector3f(x + xe, y + ye, z - ze));
        points[3].set(new Vector3f(x + xe, y - ye, z - ze));

        points[4].set(new Vector3f(x + xe, y - ye, z + ze));
        points[5].set(new Vector3f(x - xe, y - ye, z + ze));
        points[6].set(new Vector3f(x - xe, y + ye, z + ze));
        points[7].set(new Vector3f(x + xe, y + ye, z + ze));
    }
}
