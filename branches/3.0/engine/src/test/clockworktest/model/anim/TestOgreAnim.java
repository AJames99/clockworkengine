

package clockworktest.model.anim;

import com.clockwork.animation.*;
import com.clockwork.app.BasicApplication;
import com.clockwork.input.KeyInput;
import com.clockwork.input.controls.ActionListener;
import com.clockwork.input.controls.KeyTrigger;
import com.clockwork.light.DirectionalLight;
import com.clockwork.math.ColorRGBA;
import com.clockwork.math.Quaternion;
import com.clockwork.math.Vector3f;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.SceneNode;
import com.clockwork.scene.Spatial;
import com.clockwork.scene.shape.Box;

public class TestOgreAnim extends BasicApplication 
        implements AnimationEventListener, ActionListener {

    private AnimationChannel channel;
    private AnimationControl control;
    private GeometrySpatial geom;

    public static void main(String[] args) {
        TestOgreAnim app = new TestOgreAnim();
        app.start();
    }

    @Override
    public void basicInitialiseApp() {
        flyCam.setMoveSpeed(10f);
        cam.setLocation(new Vector3f(6.4013605f, 7.488437f, 12.843031f));
        cam.setRotation(new Quaternion(-0.060740203f, 0.93925786f, -0.2398315f, -0.2378785f));

        DirectionalLight dl = new DirectionalLight();
        dl.setDirection(new Vector3f(-0.1f, -0.7f, -1).normalizeLocal());
        dl.setColor(new ColorRGBA(1f, 1f, 1f, 1.0f));
        rootNode.addLight(dl);

        Spatial model = (Spatial) assetManager.loadModel("Models/Oto/Oto.mesh.xml");
        model.centre();

        control = model.getControl(AnimationControl.class);
        control.addListener(this);
        channel = control.createChannel();

        control.getAnimationNames().forEach((anim) -> {
            System.out.println(anim);
        });
        
        
        
        channel.setAnim("stand");
        geom = (GeometrySpatial)((SceneNode)model).getChild(0);
        ArmatureControl skeletonControl = model.getControl(ArmatureControl.class);
        skeletonControl.setHardwareSkinningPreferred(true);

        Box b = new Box(.25f,3f,.25f);
        GeometrySpatial item = new GeometrySpatial("Item", b);
        item.move(0, 1.5f, 0);
        item.setMaterial(assetManager.loadMaterial("Common/Materials/RedColor.mat"));
        SceneNode node = skeletonControl.getAttachmentsNode("hand.right");
        node.attachChild(item);

        rootNode.attachChild(model);

        inputManager.addListener(this, "Attack");
        inputManager.addMapping("Attack", new KeyTrigger(KeyInput.KEY_SPACE));
    }

    @Override
    public void basicUpdate(float tpf) {
        super.basicUpdate(tpf);
    }
    

    public void onAnimCycleDone(AnimationControl control, AnimationChannel channel, String animName) {
        if (animName.equals("Dodge")){
            channel.setAnim("stand", 0.50f);
            channel.setLoopMode(LoopMode.DontLoop);
            channel.setSpeed(1f);
        }
    }

    public void onAnimChange(AnimationControl control, AnimationChannel channel, String animName) {
    }

    public void onAction(String binding, boolean value, float tpf) {
        if (binding.equals("Attack") && value){
            if (!channel.getAnimationName().equals("Dodge")){
                channel.setAnim("Dodge", 0.50f);
                channel.setLoopMode(LoopMode.Cycle);
                channel.setSpeed(0.10f);
            }
        }
    }

}
