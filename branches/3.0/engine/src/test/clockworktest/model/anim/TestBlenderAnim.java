package clockworktest.model.anim;

import com.clockwork.animation.*;
import com.clockwork.app.BasicApplication;
import com.clockwork.assets.BlenderKey;
import com.clockwork.light.DirectionalLight;
import com.clockwork.math.ColorRGBA;
import com.clockwork.math.Quaternion;
import com.clockwork.math.Vector3f;
import com.clockwork.scene.SceneNode;
import com.clockwork.scene.Spatial;

public class TestBlenderAnim extends BasicApplication {

    private AnimationChannel channel;
    private AnimationControl control;

    public static void main(String[] args) {
    	TestBlenderAnim app = new TestBlenderAnim();
        app.start();
    }

    @Override
    public void basicInitialiseApp() {
        flyCam.setMoveSpeed(10f);
        cam.setLocation(new Vector3f(6.4013605f, 7.488437f, 12.843031f));
        cam.setRotation(new Quaternion(-0.060740203f, 0.93925786f, -0.2398315f, -0.2378785f));

        DirectionalLight dl = new DirectionalLight();
        dl.setDirection(new Vector3f(-0.1f, -0.7f, -1).normalizeLocal());
        dl.setColor(new ColorRGBA(1f, 1f, 1f, 1.0f));
        rootNode.addLight(dl);

        //BlenderKey blenderKey = new BlenderKey("Blender/2.4x/BaseMesh_249.blend");
        BlenderKey blenderKey = new BlenderKey("Blender/2.5x/BaseMesh_256.blend");
        
        Spatial scene = (Spatial) assetManager.loadModel(blenderKey);
        rootNode.attachChild(scene);
        
        Spatial model = this.findNode(rootNode, "BaseMesh_01");
        model.centre();
        
        control = model.getControl(AnimationControl.class);
        //ArmatureControl skeletonControl = model.getControl(ArmatureControl.class);
        //skeletonControl.setHardwareSkinningPreferred(true);
        channel = control.createChannel();

        channel.setAnim("base_MPunch");
    }
    
    private Spatial findNode(SceneNode rootNode, String name) {
        if (name.equals(rootNode.getName())) {
            return rootNode;
        }
        return rootNode.getChild(name);
    }
}
