
package clockworktest.model.anim;

import com.clockwork.animation.*;
import com.clockwork.app.BasicApplication;
import com.clockwork.font.BitmapText;
import com.clockwork.light.DirectionalLight;
import com.clockwork.math.*;
import com.clockwork.scene.Spatial;
import java.util.ArrayList;
import java.util.List;

// Hardware skinning test

public class TestHardwareSkinning extends BasicApplication{

    private AnimationChannel channel;
    private AnimationControl control;
    //private String[] names = {"Dodge", "Walk", "pull", "push"};
    private final static int SIZE = 10;
    private boolean hardwareSkinningEnabled = true;
    private List<ArmatureControl> skeletalControls = new ArrayList<ArmatureControl>();
    private BitmapText hardwareSkinningText;

    public static void main(String[] args) {
        TestHardwareSkinning app = new TestHardwareSkinning();
        app.start();
    }

    @Override
    public void basicInitialiseApp() {
        flyCam.setMoveSpeed(10f);
        cam.setLocation(new Vector3f(3.8664846f, 6.2704787f, 9.664585f));
        cam.setRotation(new Quaternion(-0.054774776f, 0.94064945f, -0.27974048f, -0.18418397f));
        makeHudText();
 
        DirectionalLight dl = new DirectionalLight();
        dl.setDirection(new Vector3f(-0.1f, -0.7f, -1).normalizeLocal());
        dl.setColor(new ColorRGBA(1f, 1f, 1f, 1.0f));
        rootNode.addLight(dl);

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                Spatial model = (Spatial) assetManager.loadModel("Models/Oto/Oto.mesh.xml");
                model.setLocalScale(0.1f);
                model.setLocalTranslation(FastMath.nextRandomFloat()*0.5f + i - SIZE / 2, 0, FastMath.nextRandomFloat()*0.5f +j - SIZE / 2);
                control = model.getControl(AnimationControl.class);

                channel = control.createChannel();
                //channel.setAnim(names[(i + j) % 4]);
                channel.setAnim("Walk");
                channel.setTime(FastMath.nextRandomFloat());
                channel.setSpeed(1 + FastMath.nextRandomFloat()*0.1f);
                ArmatureControl skeletonControl = model.getControl(ArmatureControl.class);
                skeletonControl.setHardwareSkinningPreferred(hardwareSkinningEnabled);
                skeletalControls.add(skeletonControl);
                rootNode.attachChild(model);
            }
        }
    }

    private void makeHudText() {
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        hardwareSkinningText = new BitmapText(guiFont, false);
        hardwareSkinningText.setSize(guiFont.getCharSet().getRenderedSize());
        hardwareSkinningText.setText("HWS : "+ hardwareSkinningEnabled);
        hardwareSkinningText.setLocalTranslation(0, cam.getHeight(), 0);
        guiNode.attachChild(hardwareSkinningText);
    }
}
