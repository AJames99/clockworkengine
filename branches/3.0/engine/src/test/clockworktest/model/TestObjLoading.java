

package clockworktest.model;

import com.clockwork.app.BasicApplication;
import com.clockwork.material.Material;
import com.clockwork.scene.GeometrySpatial;

/**
 * Tests OBJ format loading
 */
public class TestObjLoading extends BasicApplication {

    public static void main(String[] args){
        TestObjLoading app = new TestObjLoading();
        app.start();
    }

    public void basicInitialiseApp() {
        // create the geometry and attach it
        GeometrySpatial teaGeom = (GeometrySpatial) assetManager.loadModel("Models/Teapot/Teapot.obj");
        
        // show normals as material
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/ShowNormals.mdef");
        teaGeom.setMaterial(mat);

        rootNode.attachChild(teaGeom);
    }
}
