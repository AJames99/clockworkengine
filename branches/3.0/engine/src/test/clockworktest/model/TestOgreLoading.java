
package clockworktest.model;

import com.clockwork.animation.*;
import com.clockwork.app.BasicApplication;
import com.clockwork.light.DirectionalLight;
import com.clockwork.light.PointLight;
import com.clockwork.math.ColorRGBA;
import com.clockwork.math.FastMath;
import com.clockwork.math.Vector3f;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.Spatial;
import com.clockwork.scene.shape.Sphere;

public class TestOgreLoading extends BasicApplication {

    float angle1;
    float angle2;
    PointLight pl;
    PointLight p2;
    Spatial lightMdl;
    Spatial lightMd2;

    public static void main(String[] args) {
        TestOgreLoading app = new TestOgreLoading();
        app.start();
    }

    public void basicInitialiseApp() {
        flyCam.setMoveSpeed(10f);

        DirectionalLight dl = new DirectionalLight();
        dl.setDirection(new Vector3f(-0.1f, -0.7f, 1).normalizeLocal());
        dl.setColor(new ColorRGBA(1f, 1f, 1f, 1.0f));
        rootNode.addLight(dl);


        lightMdl = new GeometrySpatial("Light", new Sphere(10, 10, 0.1f));
        lightMdl.setMaterial(assetManager.loadMaterial("Common/Materials/RedColor.mat"));
        rootNode.attachChild(lightMdl);

        lightMd2 = new GeometrySpatial("Light", new Sphere(10, 10, 0.1f));
        lightMd2.setMaterial(assetManager.loadMaterial("Common/Materials/WhiteColor.mat"));
        rootNode.attachChild(lightMd2);


        pl = new PointLight();
        pl.setColor(new ColorRGBA(1, 0.9f, 0.9f, 0));
        pl.setPosition(new Vector3f(0f, 0f, 4f));
        rootNode.addLight(pl);

        p2 = new PointLight();
        p2.setColor(new ColorRGBA(0.9f, 1, 0.9f, 0));
        p2.setPosition(new Vector3f(0f, 0f, 3f));
        rootNode.addLight(p2);


        Spatial elephant = (Spatial) assetManager.loadModel("Models/Elephant/Elephant.mesh.xml");
        float scale = 0.05f;
        elephant.scale(scale, scale, scale);
        ArmatureControl skeletonControl = elephant.getControl(ArmatureControl.class);
        skeletonControl.setHardwareSkinningPreferred(true);
        rootNode.attachChild(elephant);
        
    }

    @Override
    public void basicUpdate(float tpf) {
        angle1 += tpf * .25f;
        angle1 %= FastMath.TWO_PI;

        angle2 += tpf * .50f;
        angle2 %= FastMath.TWO_PI;

        pl.setPosition(new Vector3f(FastMath.cos(angle1) * 4f, .5f, FastMath.sin(angle1) * 4f));
        p2.setPosition(new Vector3f(FastMath.cos(angle2) * 4f, .5f, FastMath.sin(angle2) * 4f));
        lightMdl.setLocalTranslation(pl.getPosition());
        lightMd2.setLocalTranslation(p2.getPosition());
    }
}
