

package clockworktest.conversion;

import com.clockwork.app.BasicApplication;
import com.clockwork.font.BitmapText;
import com.clockwork.material.Material;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.shape.Quad;
import com.clockwork.texture.Image;
import com.clockwork.texture.Texture;
import clockworktools.converters.MipMapGenerator;

public class TestMipMapGen extends BasicApplication {

    public static void main(String[] args){
        TestMipMapGen app = new TestMipMapGen();
        app.start();
    }

    @Override
    public void basicInitialiseApp() {
        BitmapText txt = guiFont.createLabel("Left Plane: HW Mipmap");
        txt.setLocalTranslation(0, settings.getHeight() - txt.getLineHeight() * 4, 0);
        guiNode.attachChild(txt);

        txt = guiFont.createLabel("Right Plane: AWT Mipmap");
        txt.setLocalTranslation(0, settings.getHeight() - txt.getLineHeight() * 3, 0);
        guiNode.attachChild(txt);

        Quad quadMesh = new Quad(1, 1);
        quadMesh.updateGeometry(1, 1, false);
        quadMesh.updateBound();

        GeometrySpatial quad1 = new GeometrySpatial("Textured Quad", quadMesh);
        GeometrySpatial quad2 = new GeometrySpatial("Textured Quad 2", quadMesh);

        // Hardware Mipmap
        Texture tex = assetManager.loadTexture("Interface/Logo/Test.png");
        tex.setMinFilter(Texture.MinFilter.Trilinear);

        // Java AWT Mipmap
        Texture texCustomMip = tex.clone();
        Image imageCustomMip = texCustomMip.getImage().clone();
        MipMapGenerator.generateMipMaps(imageCustomMip);
        texCustomMip.setImage(imageCustomMip);

        Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        mat1.setTexture("ColorMap", tex);

        Material mat2 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        mat2.setTexture("ColorMap", texCustomMip);

        quad1.setMaterial(mat1);

        quad2.setMaterial(mat2);
        quad2.setLocalTranslation(1, 0, 0);

        rootNode.attachChild(quad1);
        rootNode.attachChild(quad2);
    }

}
