package clockworktest.audio;

import com.clockwork.app.BasicApplication;
import com.clockwork.audio.AudioNode;
import com.clockwork.audio.Environment;
import com.clockwork.math.FastMath;
import com.clockwork.math.Vector3f;
import org.lwjgl.openal.AL10;
import org.lwjgl.openal.AL11;

/**
 * Doppler Effect test
 */
public class TestDoppler extends BasicApplication {

    private AudioNode beep;

    private float x = 20, z = 0;
    private float rate = -0.5f;
    private float xDist = 20;
    private float zDist = 5;
    private float angle = FastMath.TWO_PI;

    public static void main(String[] args) {
        TestDoppler test = new TestDoppler();
        test.start();
    }

    @Override
    public void basicInitialiseApp() {
        audioRenderer.setEnvironment(Environment.Catacombs);
        AL10.alDistanceModel(AL11.AL_EXPONENT_DISTANCE);

        beep = new AudioNode(assetManager, "Sound/Effects/Beep.ogg", false);
        beep.setPositional(true);
        beep.setLooping(true);
        beep.setReverbEnabled(true);
        beep.setRefDistance(100000000);
        beep.setMaxDistance(100000000);
        beep.play();
    }

    @Override
    public void basicUpdate(float tpf) {
        float dx = (float) Math.sin(angle) * xDist;

        float dz = (float) (-Math.cos(angle) * zDist);

        x += dx * tpf * 0.05f;
        z += dz * tpf * 0.05f;

        angle += tpf * rate;

        if (angle > FastMath.TWO_PI) {
            angle = FastMath.TWO_PI;
            rate = -rate;
        } else if (angle < -0) {
            angle = -0;
            rate = -rate;
        }

        beep.setVelocity(new Vector3f(dx, 0, dz));
        beep.setLocalTranslation(x, 0, z);
        beep.updateGeometricState();

        System.out.println("LOC: " + (int) x + ", " + (int) z
                + ", VEL: " + (int) dx + ", " + (int) dz);
    }

}
