
package clockworktest.tools;

import com.clockwork.app.BasicApplication;
import com.clockwork.light.AmbientLight;
import com.clockwork.scene.SceneNode;
import com.clockwork.scene.Spatial;
import clockworktools.savegame.SaveGame;

public class TestSaveGame extends BasicApplication {

    public static void main(String[] args) {

        TestSaveGame app = new TestSaveGame();
        app.start();
    }

    @Override
    public void basicUpdate(float tpf) {
    }

    public void basicInitialiseApp() {

        // A Node used to store the player data
        SceneNode myPlayer = new SceneNode();
        myPlayer.setName("PlayerNode");
        myPlayer.setUserData("Name", "ABC");
        myPlayer.setUserData("HP", 100.0f);
        myPlayer.setUserData("Score", 0);

        // The player's model is attached to the player node
        Spatial model = assetManager.loadModel("Blender/cargo.blend");
        myPlayer.attachChild(model);

        // Before saving the game, the player model should be detached from the main node 
        // such that it is not saved along with it.
        // The player model should be loaded along with other assets, and will likely
        // not be unique to the save file.
        myPlayer.detachAllChildren();
        SaveGame.saveGame("SaveFilesInc/testgame", "savedgameslot_001", myPlayer);

        SceneNode player = (SceneNode) SaveGame.loadGame("SaveFilesInc/testgame", "savedgameslot_001");
        player.attachChild(model);
        rootNode.attachChild(player);

        System.out.println("Name: " + player.getUserData("Name"));
        System.out.println("HP: " + player.getUserData("HP"));
        System.out.println("Score: " + player.getUserData("Score"));

        AmbientLight al = new AmbientLight();
        rootNode.addLight(al);        
    }
}
