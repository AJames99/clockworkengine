

package clockworktest.tools;

import com.clockwork.app.BasicApplication;
import com.clockwork.bounding.BoundingBox;
import com.clockwork.light.DirectionalLight;
import com.clockwork.material.MaterialList;
import com.clockwork.math.ColorRGBA;
import com.clockwork.math.Vector3f;
import com.clockwork.post.SceneProcessor;
import com.clockwork.renderer.RenderingManager;
import com.clockwork.renderer.Viewport;
import com.clockwork.renderer.queue.RenderOrder;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.Spatial;
import com.clockwork.scene.plugins.ogre.OgreMeshKey;
import com.clockwork.texture.FrameBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import clockworktools.optimize.FastOctnode;
import clockworktools.optimize.Octree;

/* Octreeing:
 * Check if an element (mesh or poly) fits completely inside of an AABB node
 * If yes, subdivide the node into 8 children (Hence Oct-tree) - this is limited
 * to a certain depth.
 * If not, continue to the next node iteratively until there exist no more nodes left
 * Add the element the the smallest node that can contain it entirely.
 * 
 * When rendering, if we cannot see a node in the camera's frustum we cull it.
 * With octrees, we infer that if we cannot see an octree node, then we cannot 
 * see any of its subdivisions/children, and thus we do not render any of them.
*/

public class TestOctree extends BasicApplication implements SceneProcessor {

    private Octree tree;
    private FastOctnode fastRoot;
    private GeometrySpatial[] globalGeoms;
    private BoundingBox octBox;

    private Set<GeometrySpatial> renderSet = new HashSet<GeometrySpatial>(300);

    public static void main(String[] args){
        TestOctree app = new TestOctree();
        app.start();
    }

    @Override
    public void basicInitialiseApp() {
        flyCam.setMoveSpeed(500);
        
        assetManager.registerLocator("quake3level.zip", com.clockwork.assets.plugins.ZipLocator.class);
        MaterialList matList = (MaterialList) assetManager.loadAsset("Scene.material");
        OgreMeshKey key = new OgreMeshKey("main.meshxml", matList);
        Spatial scene = assetManager.loadModel(key);

        DirectionalLight dl = new DirectionalLight();
        dl.setColor(ColorRGBA.White);
        dl.setDirection(new Vector3f(-1, -1, -1).normalize());
        rootNode.addLight(dl);

        DirectionalLight dl2 = new DirectionalLight();
        dl2.setColor(ColorRGBA.White);
        dl2.setDirection(new Vector3f(1, -1, 1).normalize());
        rootNode.addLight(dl2);

        // Creating a new Octree.
        tree = new Octree(scene);
        // Extreme octree parameters.
        tree.construct(90, 0.001f, 500);
        
        ArrayList<GeometrySpatial> globalGeomList = new ArrayList<GeometrySpatial>();
        tree.createFastOctnodes(globalGeomList);
        tree.generateFastOctnodeLinks();

        globalGeomList.stream().map((geom) -> {
            geom.addLight(dl);
            return geom;
        }).map((geom) -> {
            geom.addLight(dl2);
            return geom;
        }).forEachOrdered((geom) -> {
            geom.updateGeometricState();
        });
        
        globalGeoms = globalGeomList.toArray(new GeometrySpatial[0]);
        fastRoot = tree.getFastRoot();
        octBox = tree.getBound();

        viewPort.addProcessor(this);
    }

    public void initialise(RenderingManager rm, Viewport vp) {
    }

    public void reshape(Viewport vp, int w, int h) {
    }

    public boolean isinitialised() {
        return true;
    }

    public void preFrame(float tpf) {
    }

    public void postQueue(RenderOrder rq) {
        renderSet.clear();
        fastRoot.generateRenderSet(globalGeoms, renderSet, cam, octBox, true);
        renderSet.forEach((geom) -> {
            rq.addToQueue(geom, geom.getQueueBucket());
        }); //tree.renderBounds(rq, transform, box, mat);
        //renderManager.flushQueue(viewPort);
    }

    public void postFrame(FrameBuffer out) {
    }

    public void cleanup() {
    }
}
