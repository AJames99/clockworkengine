

package clockworktest.scene;

import com.clockwork.app.BasicApplication;
import com.clockwork.assets.plugins.HTTPZipLocator;
import com.clockwork.assets.plugins.ZipLocator;
import com.clockwork.light.AmbientLight;
import com.clockwork.light.DirectionalLight;
import com.clockwork.math.ColorRGBA;
import com.clockwork.math.Vector3f;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.Spatial;
import com.clockwork.scene.shape.Sphere;
import com.clockwork.util.SkyCreator;
import java.io.File;

public class TestSceneLoading extends BasicApplication {

    private Sphere sphereMesh = new Sphere(32, 32, 10, false, true);
    private GeometrySpatial sphere = new GeometrySpatial("Sky", sphereMesh);

    public static void main(String[] args) {
     
        TestSceneLoading app = new TestSceneLoading();
        app.start();
    }

    @Override
    public void basicUpdate(float tpf){
        sphere.setLocalTranslation(cam.getLocation());
    }

    public void basicInitialiseApp() {
        this.flyCam.setMoveSpeed(10);

        rootNode.attachChild(SkyCreator.createSky(assetManager, "Textures/Sky/Bright/BrightSky.dds", false));

        assetManager.registerLocator("wildhouse.zip", ZipLocator.class);
        
        Spatial scene = assetManager.loadModel("main.scene");

        AmbientLight al = new AmbientLight();
        scene.addLight(al);

        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(0.69077975f, -0.6277887f, -0.35875428f).normalizeLocal());
        sun.setColor(ColorRGBA.White.clone().multLocal(2));
        scene.addLight(sun);

        rootNode.attachChild(scene);
    }
}
