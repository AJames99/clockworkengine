package clockworktest.app;

import com.clockwork.app.Application;
import com.clockwork.math.Vector3f;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.shape.Box;

/**
 * Test a bare application, without the use of the usual BasicApplication class.
 */
public class TestBasicApp extends Application {

    private GeometrySpatial boxGeom;

    public static void main(String[] args){
        TestBasicApp app = new TestBasicApp();
        app.start();
    }

    @Override
    public void initialise(){
        super.initialise();

        System.out.println("Initialise");

        // create a box
        boxGeom = new GeometrySpatial("Box", new Box(Vector3f.ZERO, 2, 2, 2));

        // load some default material
        boxGeom.setMaterial(assetManager.loadMaterial("Interface/Logo/Logo.mat"));

        // attach box to display in primary viewport
        viewPort.attachScene(boxGeom);
    }

    @Override
    public void update(){
        super.update();

        // do some animation
        float tpf = timer.getTimePerFrame();
        boxGeom.rotate(tpf * 0.5f, tpf * 0.2f, tpf * 0.15f);
        
        // dont forget to update the scenes
        boxGeom.updateLogicalState(tpf);
        boxGeom.updateGeometricState();

        // render the viewports
        renderManager.render(tpf, context.isRenderable());
    }

    @Override
    public void destroy(){
        super.destroy();
        System.out.println("Destroy");
    }
}
