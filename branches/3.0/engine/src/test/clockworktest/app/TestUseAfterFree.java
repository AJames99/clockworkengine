
package clockworktest.app;

import com.clockwork.app.BasicApplication;
import com.clockwork.material.Material;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.shape.Box;
import com.clockwork.texture.Texture;
import com.clockwork.util.BufferUtils;

public class TestUseAfterFree extends BasicApplication {

    private float time = 0;
    private Material mat;
    private Texture deletedTex;
    
    public static void main(String[] args) {
        TestUseAfterFree app = new TestUseAfterFree();
        app.start();
    }

    @Override
    public void basicInitialiseApp() {
        Box box = new Box(1, 1, 1);
        GeometrySpatial geom = new GeometrySpatial("Box", box);
        mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        mat.setTexture("ColorMap", assetManager.loadTexture("Interface/Logo/Gear.png"));
        geom.setMaterial(mat);
        rootNode.attachChild(geom);
    }
    
    @Override
    public void basicUpdate(float tpf) {
        if (time < 0) {
            if (deletedTex != null) {
                deletedTex.getImage().resetObject();
            }
            return;
        }
        
        time += tpf;
        if (time > 5) {
            System.out.println("Assigning deleted texture!");
            
            deletedTex = assetManager.loadTexture("Interface/Logo/Test.png");
            BufferUtils.destroyDirectBuffer(deletedTex.getImage().getData(0));
            mat.setTexture("ColorMap", deletedTex);
            System.out.println("Assigned deleted texture to object.");
            time = -1;
        }
    }
}