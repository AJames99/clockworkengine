

package clockworktest.app;

import com.clockwork.app.BasicApplication;
import com.clockwork.material.Material;
import com.clockwork.math.Vector3f;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.shape.Box;
import com.clockwork.util.BufferUtils;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

public class TestReleaseDirectMemory extends BasicApplication {

    public static void main(String[] args){
        TestReleaseDirectMemory app = new TestReleaseDirectMemory();
        app.start();
    }

    @Override
    public void basicInitialiseApp() {
        Box b = new Box(Vector3f.ZERO, 1, 1, 1);
        GeometrySpatial geom = new GeometrySpatial("Box", b);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        mat.setTexture("ColorMap", assetManager.loadTexture("Interface/Logo/Gear.png"));
        geom.setMaterial(mat);
        rootNode.attachChild(geom);
    }

    @Override
    public void basicUpdate(float tpf) {
        ByteBuffer buf = BufferUtils.createByteBuffer(500000);
        BufferUtils.destroyDirectBuffer(buf);
        
        FloatBuffer buf2 = BufferUtils.createFloatBuffer(500000);
        BufferUtils.destroyDirectBuffer(buf2);
    }
    
}
