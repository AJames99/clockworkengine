

package clockworktest.app.state;

import com.clockwork.app.state.AbstractAppState;
import com.clockwork.scene.SceneNode;

public class RootNodeState extends AbstractAppState {

    private SceneNode rootNode = new SceneNode("Root Node");

    public SceneNode getRootNode(){
        return rootNode;
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        rootNode.updateLogicalState(tpf);
        rootNode.updateGeometricState();
    }

}
