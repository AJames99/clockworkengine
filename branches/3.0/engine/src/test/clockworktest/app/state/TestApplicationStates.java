package clockworktest.app.state;

import com.clockwork.app.Application;
import com.clockwork.niftygui.NiftyCWDisplay;
import com.clockwork.scene.Spatial;
import com.clockwork.system.AppSettings;
import com.clockwork.system.EngineContext;

public class TestApplicationStates extends Application {

    public static void main(String[] args){
        TestApplicationStates app = new TestApplicationStates();
        app.start();
    }

    @Override
    public void start(EngineContext.Type contextType){
        // Create a new configuration for the test app, with defaults
        AppSettings settings = new AppSettings(true);
        // Arbitrary resolution
        settings.setResolution(1024, 768);
        // Apply settings
        setSettings(settings);
        
        super.start(contextType);
    }

    @Override
    public void initialise(){
        super.initialise();

        System.out.println("Initialise");

        RootNodeState state = new RootNodeState();
        viewPort.attachScene(state.getRootNode());
        stateManager.attach(state);

        // Instantiate a simple model, using the asset manager
        Spatial model = assetManager.loadModel("Models/Teapot/Teapot.obj");
        model.scale(3);
        // Assign generic materials
        model.setMaterial(assetManager.loadMaterial("Interface/Logo/Logo.mat"));
        // Include in scene
        state.getRootNode().attachChild(model);

        // Create a GUI test using Nifty and add it as a processor
        NiftyCWDisplay niftyDisplay = new NiftyCWDisplay(assetManager,
                                                           inputManager,
                                                           audioRenderer,
                                                           guiViewPort);
        niftyDisplay.getNifty().fromXml("Interface/Nifty/HelloCW.xml", "start");
        guiViewPort.addProcessor(niftyDisplay);
    }

    @Override
    public void update(){
        super.update();

        // Perform animation
        float tpf = timer.getTimePerFrame();

        stateManager.update(tpf);
        stateManager.render(renderManager);

        // Render the viewports
        renderManager.render(tpf, context.isRenderable());
    }

    @Override
    public void destroy(){
        super.destroy();
        System.out.println("Destroy");
    }
}
