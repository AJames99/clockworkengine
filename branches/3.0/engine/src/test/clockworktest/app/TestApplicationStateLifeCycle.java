

package clockworktest.app;

import com.clockwork.app.Application;
import com.clockwork.app.BasicApplication;
import com.clockwork.app.state.AbstractAppState;
import com.clockwork.app.state.AppStateManager;
import com.clockwork.material.Material;
import com.clockwork.math.Vector3f;
import com.clockwork.renderer.RenderingManager;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.shape.Box;

public class TestApplicationStateLifeCycle extends BasicApplication {

    public static void main(String[] args){
        TestApplicationStateLifeCycle app = new TestApplicationStateLifeCycle();
        app.start();
    }

    @Override
    public void basicInitialiseApp() {
        Box box = new Box(Vector3f.ZERO, 1, 1, 1);
        GeometrySpatial geometry = new GeometrySpatial("Box", box);
        Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.mdef");
        material.setTexture("ColorMap", assetManager.loadTexture("Interface/Logo/Gear.png"));
        geometry.setMaterial(material);
        rootNode.attachChild(geometry);

        System.out.println("Attaching test state.");
        stateManager.attach(new TestState());        
    }

    @Override
    public void basicUpdate(float tpf) {
        if(stateManager.getState(TestState.class) != null) {
            System.out.println("Detaching test state."); 
            stateManager.detach(stateManager.getState(TestState.class));
            System.out.println("Done"); 
        }        
    }
    
    public class TestState extends AbstractAppState {
 
        @Override
        public void initialise(AppStateManager stateManager, Application app) {
            super.initialise(stateManager, app);
            System.out.println("Initialised");
        }
 
        @Override
        public void stateAttached(AppStateManager stateManager) {
            super.stateAttached(stateManager);
            System.out.println("Attached");
        }
 
        @Override
        public void update(float tpf) {
            super.update(tpf);
            System.out.println("Updated");
        }

        @Override
        public void render(RenderingManager rm) {
            super.render(rm);
            System.out.println("Rendered");
        }

        @Override
        public void postRender() {
            super.postRender();
            System.out.println("Post rendered");
        }

        @Override
        public void stateDetached(AppStateManager stateManager) {
            super.stateDetached(stateManager);
            System.out.println("Detached");
        }
 
        @Override
        public void cleanup() {
            super.cleanup();
            System.out.println("Cleanup"); 
        }
    }    
}
