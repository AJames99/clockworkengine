
package clockworktest.app;

import com.clockwork.math.Vector3f;
import com.clockwork.util.TempVariables;

public class TestTempVars {

    private static final int ITERATIONS = 10000000;
    private static final int NANOS_TO_MS = 1000000;
    
    private static final Vector3f sumCompute = new Vector3f();
    
    public static void main(String[] args) {
        long milliseconds, nanos;
        
        for (int i = 0; i < 4; i++){
            System.gc();
        }
               
        sumCompute.set(0, 0, 0);
        nanos = System.nanoTime();
        for (int i = 0; i < ITERATIONS; i++) {
            methodUsingTempVars();
        }
        milliseconds = (System.nanoTime() - nanos) / NANOS_TO_MS;
        System.out.println("100,000,000 TempVars calls: " + milliseconds + " ms");
        System.out.println(sumCompute);

        sumCompute.set(0, 0, 0);
        nanos = System.nanoTime();
        for (int i = 0; i < ITERATIONS; i++) {
            methodUsingAllocation();
        }
        milliseconds = (System.nanoTime() - nanos) / NANOS_TO_MS;
        System.out.println("100,000,000 allocation calls: " + milliseconds + " ms");
        System.out.println(sumCompute);
        
        nanos = System.nanoTime();
        for (int i = 0; i < 10; i++){
            System.gc();
        }
        milliseconds = (System.nanoTime() - nanos) / NANOS_TO_MS;
        System.out.println("Cleanup time after allocation calls: " + milliseconds + " ms");
    }

    public static void methodUsingAllocation(){
        Vector3f vector = new Vector3f();
        vector.set(0.1f, 0.2f, 0.3f);
        sumCompute.addLocal(vector);
    }

    public static void methodUsingTempVars() {
        TempVariables vars = TempVariables.get();
        {
            vars.vect1.set(0.1f, 0.2f, 0.3f);
            sumCompute.addLocal(vars.vect1);
        }
        vars.release();
    }
}
