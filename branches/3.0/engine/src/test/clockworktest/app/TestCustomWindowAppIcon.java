
package clockworktest.app;

import com.clockwork.app.BasicApplication;
import com.clockwork.font.BitmapText;
import com.clockwork.system.AppSettings;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class TestCustomWindowAppIcon extends BasicApplication {

    private static final Logger log=Logger.getLogger(TestCustomWindowAppIcon.class.getName());

    public static void main(String[] args) {
        TestCustomWindowAppIcon app = new TestCustomWindowAppIcon();
        AppSettings settings = new AppSettings(true);

        try {
            Class<TestCustomWindowAppIcon> testClass = TestCustomWindowAppIcon.class;

            settings.setIcons(new BufferedImage[]{
                        ImageIO.read(testClass.getResourceAsStream("/Interface/icons/gearIcon256.png")),
                        ImageIO.read(testClass.getResourceAsStream("/Interface/icons/gearIcon128.png")),
                        ImageIO.read(testClass.getResourceAsStream("/Interface/icons/gearIcon32.png")),
                        ImageIO.read(testClass.getResourceAsStream("/Interface/icons/gearIcon16.png")),
                    });
        } catch (IOException e) {
            log.log(java.util.logging.Level.WARNING, "Unable to successfully load the program's icons", e);
        }
        app.setSettings(settings);
        app.start();
    }

    @Override
    public void basicInitialiseApp() {
        // Write text to the GUI
        setDisplayStatView(false);
        BitmapText helloText = new BitmapText(guiFont);
        helloText.setText("The application's icon should now be the gear symbol");
        helloText.setLocalTranslation(300, helloText.getLineHeight(), 0);
        guiNode.attachChild(helloText);
    }
}
