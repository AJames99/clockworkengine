

package clockworktest.asset;

import com.clockwork.assets.AssetLoader;
import com.clockwork.assets.plugins.ClasspathLocator;
import com.clockwork.system.EngineSystem;
import com.clockwork.assets.AssetHandler;

/**
 * Demonstrates loading a file from a custom AssetLoader}
 */
public class TestCustomLoader {
    public static void main(String[] args){
        AssetHandler assetHandler = EngineSystem.newAssetHandler();
        assetHandler.registerLocator("/", ClasspathLocator.class);
        assetHandler.registerLoader(TextLoader.class, "fnt");
        System.out.println(assetHandler.loadAsset("Interface/Fonts/Console.fnt"));
    }
}
