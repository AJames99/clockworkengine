

package clockworktest.asset;

import com.clockwork.assets.DesktopAssetHandler;
import com.clockwork.assets.plugins.ClasspathLocator;
import com.clockwork.audio.AudioData;
import com.clockwork.audio.plugins.WAVLoader;
import com.clockwork.texture.Texture;
import com.clockwork.texture.plugins.AWTLoader;
import com.clockwork.assets.AssetHandler;

public class TestAbsoluteLocators {
    public static void main(String[] args){
        AssetHandler AH = new DesktopAssetHandler();

        AH.registerLoader(AWTLoader.class, "jpg");
        AH.registerLoader(WAVLoader.class, "wav");

        // register absolute locator
        AH.registerLocator("/",  ClasspathLocator.class);

        // find a sound
        AudioData audio = AH.loadAudio("Sound/Effects/Gun.wav");

        // find a texture
        Texture tex = AH.loadTexture("Textures/Terrain/Pond/Pond.jpg");

        if (audio == null)
            throw new RuntimeException("Cannot find audio!");
        else
            System.out.println("Audio loaded from Sounds/Effects/Gun.wav");

        if (tex == null)
            throw new RuntimeException("Cannot find texture!");
        else
            System.out.println("Texture loaded from Textures/Terrain/Pond/Pond.jpg");

        System.out.println("Success!");
    }
}
