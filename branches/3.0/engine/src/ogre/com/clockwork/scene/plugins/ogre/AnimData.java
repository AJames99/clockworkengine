
package com.clockwork.scene.plugins.ogre;

import com.clockwork.animation.Animation;
import com.clockwork.animation.Armature;
import java.util.ArrayList;

public class AnimData {

    public final Armature skeleton;
    public final ArrayList<Animation> anims;

    public AnimData(Armature skeleton, ArrayList<Animation> anims) {
        this.skeleton = skeleton;
        this.anims = anims;
    }
}
