
package com.clockwork.scene.debug;

import com.clockwork.animation.Armature;
import com.clockwork.renderer.queue.RenderOrder.Bucket;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.SceneNode;

public class SkeletonDebugger extends SceneNode {

    private SkeletonWire wires;
    private SkeletonPoints points;
    private Armature skeleton;

    public SkeletonDebugger(String name, Armature skeleton){
        super(name);

        this.skeleton = skeleton;
        wires = new SkeletonWire(skeleton);
        points = new SkeletonPoints(skeleton);

        attachChild(new GeometrySpatial(name+"_wires", wires));
        attachChild(new GeometrySpatial(name+"_points", points));

        setQueueBucket(Bucket.Transparent);
    }

    public SkeletonDebugger(){
    }

    @Override
    public void updateLogicalState(float tpf){
        super.updateLogicalState(tpf);

//        skeleton.resetAndUpdate();
        wires.updateGeometry();
        points.updateGeometry();
    }

    public SkeletonPoints getPoints() {
        return points;
    }

    public SkeletonWire getWires() {
        return wires;
    }
    
    
}
