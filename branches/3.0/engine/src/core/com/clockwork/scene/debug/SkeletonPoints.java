
package com.clockwork.scene.debug;

import com.clockwork.animation.Joint;
import com.clockwork.animation.Armature;
import com.clockwork.math.Vector3f;
import com.clockwork.scene.MeshGeometry;
import com.clockwork.scene.VertexBuffer;
import com.clockwork.scene.VertexBuffer.Format;
import com.clockwork.scene.VertexBuffer.Type;
import com.clockwork.scene.VertexBuffer.Usage;
import com.clockwork.util.BufferUtils;
import java.nio.FloatBuffer;

public class SkeletonPoints extends MeshGeometry {

    private Armature skeleton;

    public SkeletonPoints(Armature skeleton){
        this.skeleton = skeleton;

        setMode(Mode.Points);

        VertexBuffer pb = new VertexBuffer(Type.Position);
        FloatBuffer fpb = BufferUtils.createFloatBuffer(skeleton.getBoneCount() * 3);
        pb.setupData(Usage.Stream, 3, Format.Float, fpb);
        setBuffer(pb);

        setPointSize(7);

        updateCounts();
    }

    public void updateGeometry(){
        VertexBuffer vb = getBuffer(Type.Position);
        FloatBuffer posBuf = getFloatBuffer(Type.Position);
        posBuf.clear();
        for (int i = 0; i < skeleton.getBoneCount(); i++){
            Joint bone = skeleton.getBone(i);
            Vector3f bonePos = bone.getModelSpacePosition();

            posBuf.put(bonePos.getX()).put(bonePos.getY()).put(bonePos.getZ());
        }
        posBuf.flip();
        vb.updateData(posBuf);

        updateBound();
    }
}
