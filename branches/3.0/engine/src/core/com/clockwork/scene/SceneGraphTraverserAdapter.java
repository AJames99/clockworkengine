
package com.clockwork.scene;

/**
 * SceneGraphTraverserAdapter is used to traverse the scene
 graph tree. The adapter version of the interface simply separates 
 between the GeometrySpatial geometries} and the SceneNode nodes} by
 supplying visit methods that take them.
 Use by calling Spatial#depthFirstTraversal(com.clockwork.scene.SceneGraphTraverser) }
 or Spatial#breadthFirstTraversal(com.clockwork.scene.SceneGraphTraverser)}.
 */
public class SceneGraphTraverserAdapter implements SceneGraphTraverser {
    
    /**
     * Called when a GeometrySpatial} is visited.
     * 
     * @param geom The visited geometry
     */
    public void visit(GeometrySpatial geom) {}
    
    /**
     * Called when a SceneNode} is visited.
     * 
     * @param geom The visited node
     */
    public void visit(SceneNode geom) {}

    @Override
    public final void visit(Spatial spatial) {
        if (spatial instanceof GeometrySpatial) {
            visit((GeometrySpatial)spatial);
        } else {
            visit((SceneNode)spatial);
        }
    }
}
