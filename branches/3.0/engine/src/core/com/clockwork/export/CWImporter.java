
package com.clockwork.export;

import com.clockwork.assets.AssetLoader;
import com.clockwork.assets.AssetHandler;

public interface CWImporter extends AssetLoader {
    public InputCapsule getCapsule(Savable id);
    public AssetHandler getAssetHandler();
    
    /**
     * Returns the version number written in the header of the CWO/XML
     * file.
     * 
     * @return Global version number for the file
     */
    public int getFormatVersion();
}
