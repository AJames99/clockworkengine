
package com.clockwork.animation;

/**
 * AnimationEventListener allows user code to receive various
 events regarding an AnimationControl. For example, when an animation cycle is done.
 * 
 */
public interface AnimationEventListener {

    /**
     * Invoked when an animation "cycle" is done. For non-looping animations,
     * this event is invoked when the animation is finished playing. For
     * looping animations, this even is invoked each time the animation is restarted.
     *
     * @param control The control to which the listener is assigned.
     * @param channel The channel being altered
     * @param animName The new animation that is done.
     */
    public void onAnimCycleDone(AnimationControl control, AnimationChannel channel, String animName);

    /**
     * Invoked when a animation is set to play by the user on the given channel.
     *
     * @param control The control to which the listener is assigned.
     * @param channel The channel being altered
     * @param animName The new animation name set.
     */
    public void onAnimChange(AnimationControl control, AnimationChannel channel, String animName);

}
