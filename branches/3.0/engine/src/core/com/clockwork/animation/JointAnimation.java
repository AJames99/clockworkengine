
package com.clockwork.animation;

/**
 * @deprecated use Animation instead with tracks of selected type (ie. BoneTrack, SpatialTrack, MeshTrack)
 */
@Deprecated
public final class JointAnimation extends Animation {

    @Deprecated
    public JointAnimation(String name, float length) {
        super(name, length);
    }
}
