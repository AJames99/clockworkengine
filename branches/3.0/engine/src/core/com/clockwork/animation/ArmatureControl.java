
package com.clockwork.animation;

import com.clockwork.export.*;
import com.clockwork.material.Material;
import com.clockwork.math.FastMath;
import com.clockwork.math.Matrix4f;
import com.clockwork.renderer.RenderingManager;
import com.clockwork.renderer.RendererException;
import com.clockwork.renderer.Viewport;
import com.clockwork.scene.*;
import com.clockwork.scene.VertexBuffer.Type;
import com.clockwork.scene.control.AbstractControl;
import com.clockwork.scene.control.Control;
import com.clockwork.shader.VariableType;
import com.clockwork.util.SafeArrayList;
import com.clockwork.util.TempVariables;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Armature control deforms a model according to a skeleton, It handles the
 computation of the deformation matrices and performs the transformations on
 the mesh
 *
 */
public class ArmatureControl extends AbstractControl implements Cloneable {

    /**
     * The skeleton of the model.
     */
    private Armature skeleton;
    /**
     * List of targets which this controller effects.
     */
    private SafeArrayList<MeshGeometry> targets = new SafeArrayList<MeshGeometry>(MeshGeometry.class);
    /**
     * Used to track when a mesh was updated. Meshes are only updated if they
     * are visible in at least one camera.
     */
    private boolean wasMeshUpdated = false;
    
    /**
     * User wishes to use hardware skinning if available.
     */
    private transient boolean hwSkinningDesired = false;
    
    /**
     * Hardware skinning is currently being used.
     */
    private transient boolean hwSkinningEnabled = false;
    
    /**
     * Hardware skinning was tested on this GPU, results
     * are stored in #hwSkinningSupported variable.
     */
    private transient boolean hwSkinningTested = false;
    
    /**
     * If hardware skinning was #hwSkinningTested tested, then
     * this variable will be set to true if supported, and false if otherwise.
     */
    private transient boolean hwSkinningSupported = false;
    
    /**
     * Joint offset matrices, recreated each frame
     */
    private transient Matrix4f[] offsetMatrices;
    /**
     * Material references used for hardware skinning
     */
    private Set<Material> materials = new HashSet<Material>();

    /**
     * Serialisation only. Do not use.
     */
    public ArmatureControl() {
    }

    private void switchToHardware() {
        // Next full 10 bones (e.g. 30 on 24 bones)
        int numBones = ((skeleton.getBoneCount() / 10) + 1) * 10;
        for (Material m : materials) {
            m.setInt("NumberOfBones", numBones);
        }
        for (MeshGeometry mesh : targets) {
            if (mesh.isAnimated()) {
                mesh.prepareForAnim(false);
            }
        }
    }

    private void switchToSoftware() {
        for (Material m : materials) {
            if (m.getParam("NumberOfBones") != null) {
                m.clearParam("NumberOfBones");
            }
        }
        for (MeshGeometry mesh : targets) {
            if (mesh.isAnimated()) {
                mesh.prepareForAnim(true);
            }
        }
    }

    private boolean testHardwareSupported(RenderingManager rm) {
        for (Material m : materials) {
            // Some of the animated mesh(es) do not support hardware skinning,
            // so it is not supported by the model.
            if (m.getMaterialDef().getMaterialParam("NumberOfBones") == null) {
                Logger.getLogger(ArmatureControl.class.getName()).log(Level.WARNING, 
                        "Not using hardware skinning for {0}, " + 
                        "because material {1} doesn''t support it.", 
                        new Object[]{spatial, m.getMaterialDef().getName()});
                
                return false;
            }
        }

        switchToHardware();
        
        try {
            rm.preloadScene(spatial);
            return true;
        } catch (RendererException e) {
            Logger.getLogger(ArmatureControl.class.getName()).log(Level.WARNING, "Could not enable HW skinning due to shader compile error:", e);
            return false;
        }
    }

    /**
     * Specifies if hardware skinning is preferred. If it is preferred and
     * supported by GPU, it shall be enabled, if its not preferred, or not
     * supported by GPU, then it shall be disabled.
     * 
     * see #isHardwareSkinningUsed() 
     */
    public void setHardwareSkinningPreferred(boolean preferred) {
        hwSkinningDesired = preferred;
    }
    
    /**
     * @return True if hardware skinning is preferable to software skinning.
     * Set to false by default.
     * 
     * see #setHardwareSkinningPreferred(boolean) 
     */
    public boolean isHardwareSkinningPreferred() {
        return hwSkinningDesired;
    }
    
    /**
     * @return True is hardware skinning is activated and is currently used, false otherwise.
     */
    public boolean isHardwareSkinningUsed() {
        return hwSkinningEnabled;
    }
    
    /**
     * Creates a skeleton control. The list of targets will be acquired
     * automatically when the control is attached to a node.
     *
     * @param skeleton the skeleton
     */
    public ArmatureControl(Armature skeleton) {
        this.skeleton = skeleton;
    }

    /**
     * Creates a skeleton control.
     *
     * @param targets the meshes controlled by the skeleton
     * @param skeleton the skeleton
     */
    @Deprecated
    ArmatureControl(MeshGeometry[] targets, Armature skeleton) {
        this.skeleton = skeleton;
        this.targets = new SafeArrayList<MeshGeometry>(MeshGeometry.class, Arrays.asList(targets));
    }


    private void findTargets(SceneNode node) {
        MeshGeometry sharedMesh = null;        

        for (Spatial child : node.getChildren()) {
            if (child instanceof GeometrySpatial) {
                GeometrySpatial geom = (GeometrySpatial) child;

                // is this geometry using a shared mesh?
                MeshGeometry childSharedMesh = geom.getUserData(UserData.CW_SHAREDMESH);

                if (childSharedMesh != null) {
                    // Don’t bother with non-animated shared meshes
                    if (childSharedMesh.isAnimated()) {
                        // child is using shared mesh,
                        // so animate the shared mesh but ignore child
                        if (sharedMesh == null) {
                            sharedMesh = childSharedMesh;
                        } else if (sharedMesh != childSharedMesh) {
                            throw new IllegalStateException("Two conflicting shared meshes for " + node);
                        }
                        materials.add(geom.getMaterial());
                    }
                } else {
                    MeshGeometry mesh = geom.getMesh();
                    if (mesh.isAnimated()) {
                        targets.add(mesh);
                        materials.add(geom.getMaterial());
                    }
                }
            } else if (child instanceof SceneNode) {
                findTargets((SceneNode) child);
            }
        }

        if (sharedMesh != null) {
            targets.add(sharedMesh);
        }

    }

    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        updateTargetsAndMaterials(spatial);
    }

    private void controlRenderSoftware() {
        resetToBind(); // reset morph meshes to bind pose

        offsetMatrices = skeleton.computeSkinningMatrices();

        for (MeshGeometry mesh : targets) {
            // NOTE: This assumes that code higher up
            // Already ensured those targets are animated
            // otherwise a crash will happen in skin update
            softwareSkinUpdate(mesh, offsetMatrices);
        }     
    }

    private void controlRenderHardware() {
        offsetMatrices = skeleton.computeSkinningMatrices();
        for (Material m : materials) {
            m.setParam("BoneMatrices", VariableType.Matrix4Array, offsetMatrices);
        }
    }

    
    @Override
    protected void controlRender(RenderingManager rm, Viewport vp) {
        if (!wasMeshUpdated) {
            updateTargetsAndMaterials(spatial);
            
            // Prevent illegal cases. These should never happen.
            assert hwSkinningTested || (!hwSkinningTested && !hwSkinningSupported && !hwSkinningEnabled);
            assert !hwSkinningEnabled || (hwSkinningEnabled && hwSkinningTested && hwSkinningSupported);

            if (hwSkinningDesired && !hwSkinningTested) {
                hwSkinningTested = true;
                hwSkinningSupported = testHardwareSupported(rm);

                if (hwSkinningSupported) {
                    hwSkinningEnabled = true;
                    
                    Logger.getLogger(ArmatureControl.class.getName()).log(Level.INFO, "Hardware skinning engaged for " + spatial);
                } else {
                    switchToSoftware();
                }
            } else if (hwSkinningDesired && hwSkinningSupported && !hwSkinningEnabled) {
                switchToHardware();
                hwSkinningEnabled = true;
            } else if (!hwSkinningDesired && hwSkinningEnabled) {
                switchToSoftware();
                hwSkinningEnabled = false;
            }

            if (hwSkinningEnabled) {
                controlRenderHardware();
            } else {
                controlRenderSoftware();
            }

            wasMeshUpdated = true;
        }
    }

    @Override
    protected void controlUpdate(float tpf) {
        wasMeshUpdated = false;
     }

    //only do this for software updates
    void resetToBind() {
        for (MeshGeometry mesh : targets) {
            if (mesh.isAnimated()) {
                Buffer bwBuff = mesh.getBuffer(Type.BoneWeight).getData();
                Buffer biBuff = mesh.getBuffer(Type.BoneIndex).getData();
                if (!biBuff.hasArray() || !bwBuff.hasArray()) {
                    mesh.prepareForAnim(true); // prepare for software animation
                }
                VertexBuffer bindPos = mesh.getBuffer(Type.BindPosePosition);
                VertexBuffer bindNorm = mesh.getBuffer(Type.BindPoseNormal);
                VertexBuffer pos = mesh.getBuffer(Type.Position);
                VertexBuffer norm = mesh.getBuffer(Type.Normal);
                FloatBuffer pb = (FloatBuffer) pos.getData();
                FloatBuffer nb = (FloatBuffer) norm.getData();
                FloatBuffer bpb = (FloatBuffer) bindPos.getData();
                FloatBuffer bnb = (FloatBuffer) bindNorm.getData();
                pb.clear();
                nb.clear();
                bpb.clear();
                bnb.clear();

                //reseting bind tangents if there is a bind tangent buffer
                VertexBuffer bindTangents = mesh.getBuffer(Type.BindPoseTangent);
                if (bindTangents != null) {
                    VertexBuffer tangents = mesh.getBuffer(Type.Tangent);
                    FloatBuffer tb = (FloatBuffer) tangents.getData();
                    FloatBuffer btb = (FloatBuffer) bindTangents.getData();
                    tb.clear();
                    btb.clear();
                    tb.put(btb).clear();
                }


                pb.put(bpb).clear();
                nb.put(bnb).clear();
            }
        }
    }

    public Control cloneForSpatial(Spatial spatial) {
        SceneNode clonedNode = (SceneNode) spatial;
        AnimationControl ctrl = spatial.getControl(AnimationControl.class);
        ArmatureControl clone = new ArmatureControl();

        clone.skeleton = ctrl.getSkeleton();

        clone.setSpatial(clonedNode);

        // Fix attachments for the cloned node
        for (int i = 0; i < clonedNode.getQuantity(); i++) {
            // go through attachment nodes, apply them to correct bone
            Spatial child = clonedNode.getChild(i);
            if (child instanceof SceneNode) {
                SceneNode clonedAttachNode = (SceneNode) child;
                Joint originalBone = (Joint) clonedAttachNode.getUserData("AttachedBone");

                if (originalBone != null) {
                    Joint clonedBone = clone.skeleton.getBone(originalBone.getName());

                    clonedAttachNode.setUserData("AttachedBone", clonedBone);
                    clonedBone.setAttachmentsNode(clonedAttachNode);
                }
            }
        }

        return clone;
    }

    /**
     *
     * @param boneName the name of the bone
     * @return the node attached to this bone
     */
    public SceneNode getAttachmentsNode(String boneName) {
        Joint b = skeleton.getBone(boneName);
        if (b == null) {
            throw new IllegalArgumentException("Given bone name does not exist "
                    + "in the skeleton.");
        }

        SceneNode n = b.getAttachmentsNode();
        SceneNode model = (SceneNode) spatial;
        model.attachChild(n);
        return n;
    }

    /**
     * returns the skeleton of this control
     *
     * @return
     */
    public Armature getSkeleton() {
        return skeleton;
    }

    /**
     * returns a copy of array of the targets meshes of this control
     *
     * @return
     */
    public MeshGeometry[] getTargets() {        
        return targets.toArray(new MeshGeometry[targets.size()]);
    }

    /**
     * Update the mesh according to the given transformation matrices
     *
     * @param mesh then mesh
     * @param offsetMatrices the transformation matrices to apply
     */
    private void softwareSkinUpdate(MeshGeometry mesh, Matrix4f[] offsetMatrices) {

        VertexBuffer tb = mesh.getBuffer(Type.Tangent);
        if (tb == null) {
            //if there are no tangents use the classic skinning
            applySkinning(mesh, offsetMatrices);
        } else {
            //if there are tangents use the skinning with tangents
            applySkinningTangents(mesh, offsetMatrices, tb);
        }


    }

    /**
     * Method to apply skinning transforms to mesh buffers
     *
     * @param mesh
     * @param offsetMatrices the offset matrices to apply
     */
    private void applySkinning(MeshGeometry mesh, Matrix4f[] offsetMatrices) {
        int maxWeightsPerVert = mesh.getMaxNumWeights();
        if (maxWeightsPerVert <= 0) {
            throw new IllegalStateException("Max weights per vert is incorrectly set!");
        }

        int fourMinusMaxWeights = 4 - maxWeightsPerVert;

        // NOTE: This code assumes the vertex buffer is in bind pose
        // resetToBind() has been called this frame
        VertexBuffer vb = mesh.getBuffer(Type.Position);
        FloatBuffer fvb = (FloatBuffer) vb.getData();
        fvb.rewind();

        VertexBuffer nb = mesh.getBuffer(Type.Normal);
        FloatBuffer fnb = (FloatBuffer) nb.getData();
        fnb.rewind();

        // get boneIndexes and weights for mesh
        ByteBuffer ib = (ByteBuffer) mesh.getBuffer(Type.BoneIndex).getData();
        FloatBuffer wb = (FloatBuffer) mesh.getBuffer(Type.BoneWeight).getData();

        ib.rewind();
        wb.rewind();

        float[] weights = wb.array();
        byte[] indices = ib.array();
        int idxWeights = 0;

        TempVariables vars = TempVariables.get();

        float[] posBuf = vars.skinPositions;
        float[] normBuf = vars.skinNormals;

        int iterations = (int) FastMath.ceil(fvb.limit() / ((float) posBuf.length));
        int bufLength = posBuf.length;
        for (int i = iterations - 1; i >= 0; i--) {
            // read next set of positions and normals from native buffer
            bufLength = Math.min(posBuf.length, fvb.remaining());
            fvb.get(posBuf, 0, bufLength);
            fnb.get(normBuf, 0, bufLength);
            int verts = bufLength / 3;
            int idxPositions = 0;

            // iterate vertices and apply skinning transform for each effecting bone
            for (int vert = verts - 1; vert >= 0; vert--) 

            fvb.position(fvb.position() - bufLength);
            fvb.put(posBuf, 0, bufLength);
            fnb.position(fnb.position() - bufLength);
            fnb.put(normBuf, 0, bufLength);
        }

        vars.release();

        vb.updateData(fvb);
        nb.updateData(fnb);

    }

    /**
     * Specific method for skinning with tangents to avoid cluttering the
     * classic skinning calculation with null checks that would slow down the
     * process even if tangents don't have to be computed. Also the iteration
     * has additional indexes since tangent has 4 components instead of 3 for
     * pos and norm
     *
     * @param maxWeightsPerVert maximum number of weights per vertex
     * @param mesh the mesh
     * @param offsetMatrices the offsetMaytrices to apply
     * @param tb the tangent vertexBuffer
     */
    private void applySkinningTangents(MeshGeometry mesh, Matrix4f[] offsetMatrices, VertexBuffer tb) {
        int maxWeightsPerVert = mesh.getMaxNumWeights();

        if (maxWeightsPerVert <= 0) {
            throw new IllegalStateException("Max weights per vert is incorrectly set!");
        }

        int fourMinusMaxWeights = 4 - maxWeightsPerVert;

        // NOTE: This part assumes the vertex buffer is in the bind pose
        // resetToBind() has been called this frame
        VertexBuffer vb = mesh.getBuffer(Type.Position);
        FloatBuffer fvb = (FloatBuffer) vb.getData();
        fvb.rewind();

        VertexBuffer nb = mesh.getBuffer(Type.Normal);

        FloatBuffer fnb = (FloatBuffer) nb.getData();
        fnb.rewind();


        FloatBuffer ftb = (FloatBuffer) tb.getData();
        ftb.rewind();


        // get boneIndexes and weights for mesh
        ByteBuffer ib = (ByteBuffer) mesh.getBuffer(Type.BoneIndex).getData();
        FloatBuffer wb = (FloatBuffer) mesh.getBuffer(Type.BoneWeight).getData();

        ib.rewind();
        wb.rewind();

        float[] weights = wb.array();
        byte[] indices = ib.array();
        int idxWeights = 0;

        TempVariables vars = TempVariables.get();


        float[] posBuf = vars.skinPositions;
        float[] normBuf = vars.skinNormals;
        float[] tanBuf = vars.skinTangents;

        int iterations = (int) FastMath.ceil(fvb.limit() / ((float) posBuf.length));
        int bufLength = 0;
        int tanLength = 0;
        for (int i = iterations - 1; i >= 0; i--) {
            // read next set of positions and normals from native buffer
            bufLength = Math.min(posBuf.length, fvb.remaining());
            tanLength = Math.min(tanBuf.length, ftb.remaining());
            fvb.get(posBuf, 0, bufLength);
            fnb.get(normBuf, 0, bufLength);
            ftb.get(tanBuf, 0, tanLength);
            int verts = bufLength / 3;
            int idxPositions = 0;
            //tangents has their own index because of the 4 components
            int idxTangents = 0;

            // iterate vertices and apply skinning transform for each effecting bone
            for (int vert = verts - 1; vert >= 0; vert--) 

            fvb.position(fvb.position() - bufLength);
            fvb.put(posBuf, 0, bufLength);
            fnb.position(fnb.position() - bufLength);
            fnb.put(normBuf, 0, bufLength);
            ftb.position(ftb.position() - tanLength);
            ftb.put(tanBuf, 0, tanLength);
        }

        vars.release();

        vb.updateData(fvb);
        nb.updateData(fnb);
        tb.updateData(ftb);


    }

    @Override
    public void write(CWExporter ex) throws IOException {
        super.write(ex);
        OutputCapsule oc = ex.getCapsule(this);
        oc.write(skeleton, "skeleton", null);
        //Targets and materials don't need to be saved, they'll be gathered on each frame
    }

    @Override
    public void read(CWImporter im) throws IOException {
        super.read(im);
        InputCapsule in = im.getCapsule(this);
        skeleton = (Armature) in.readSavable("skeleton", null);
    }

    private void updateTargetsAndMaterials(Spatial spatial) {
        targets.clear();
        materials.clear();           
        if (spatial != null && spatial instanceof SceneNode) {
            SceneNode node = (SceneNode) spatial;                        
            findTargets(node);
        }
    }
}
