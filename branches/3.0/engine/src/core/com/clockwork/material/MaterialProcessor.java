
package com.clockwork.material;

import com.clockwork.assets.AssetKey;
import com.clockwork.assets.AssetProcessor;

public class MaterialProcessor implements AssetProcessor {

    public Object postProcess(AssetKey key, Object obj) {
        return null;
    }

    public Object createClone(Object obj) {
        return ((Material) obj).clone();
    }
}
