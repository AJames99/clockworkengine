
package com.clockwork.input;

/**
 * Abstract interface for an input device.
 * 
 * see MouseInput
 * see KeyInput
 * see JoyInput
 */
public interface Input {

    /**
     * Initialises the native side to listen into events from the device.
     */
    public void initialise();

    /**
     * Queries the device for input. All events should be sent to the
     * RawInputListener set with setInputListener.
     *
     * see #setInputListener(com.clockwork.input.RawInputListener)
     */
    public void update();

    /**
     * Ceases listening to events from the device.
     */
    public void destroy();

    /**
     * @return True if the device has been initialised and not destroyed.
     * see #initialise()
     * see #destroy() 
     */
    public boolean isinitialised();

    /**
     * Sets the input listener to receive events from this device. The
     * appropriate events should be dispatched through the callbacks
     * in RawInputListener.
     * @param listener
     */
    public void setInputListener(RawInputListener listener);

    /**
     * @return The current absolute time as nanoseconds. This time is expected
     * to be relative to the time given in InputEvents time property.
     */
    public long getInputTimeNanos();
}
