
package com.clockwork.input.dummy;

import com.clockwork.input.Input;
import com.clockwork.input.RawInputListener;

/**
 * DummyInput as an implementation of Input that raises no
 * input events.
 * 
 */
public class DummyInput implements Input {

    protected boolean inited = false;

    public void initialise() {
        if (inited)
            throw new IllegalStateException("Input already initialised.");

        inited = true;
    }

    public void update() {
        if (!inited)
            throw new IllegalStateException("Input not initialised.");
    }

    public void destroy() {
        if (!inited)
            throw new IllegalStateException("Input not initialised.");

        inited = false;
    }

    public boolean isinitialised() {
        return inited;
    }

    public void setInputListener(RawInputListener listener) {
    }

    public long getInputTimeNanos() {
        return System.currentTimeMillis() * 1000000;
    }

}
