
package com.clockwork.shader;

import com.clockwork.material.Material;
import com.clockwork.math.*;
import com.clockwork.renderer.Camera;
import com.clockwork.renderer.RenderingManager;
import com.clockwork.system.Timer;
import java.util.List;

/**
 * ShaderUniformBindingManager helps RenderingManager} to manage
 ShaderUniformBinding uniform bindings}.
 * 
 * The #updateUniformBindings(java.util.List) } will update
 * a given list of uniforms based on the current state
 * of the manager.
 * 
 * 
 */
public class ShaderUniformBindingManager {

    private Timer timer;
    private float near, far;
    private int viewX, viewY, viewWidth, viewHeight;
    private Vector3f camUp = new Vector3f(),
            camLeft = new Vector3f(),
            camDir = new Vector3f(),
            camLoc = new Vector3f();
    private Matrix4f tempMatrix = new Matrix4f();
    private Matrix4f viewMatrix = new Matrix4f();
    private Matrix4f projMatrix = new Matrix4f();
    private Matrix4f viewProjMatrix = new Matrix4f();
    private Matrix4f worldMatrix = new Matrix4f();
    private Matrix4f worldViewMatrix = new Matrix4f();
    private Matrix4f worldViewProjMatrix = new Matrix4f();
    private Matrix3f normalMatrix = new Matrix3f();
    private Matrix4f worldMatrixInv = new Matrix4f();
    private Matrix3f worldMatrixInvTrsp = new Matrix3f();
    private Matrix4f viewMatrixInv = new Matrix4f();
    private Matrix4f projMatrixInv = new Matrix4f();
    private Matrix4f viewProjMatrixInv = new Matrix4f();
    private Matrix4f worldViewMatrixInv = new Matrix4f();
    private Matrix3f normalMatrixInv = new Matrix3f();
    private Matrix4f worldViewProjMatrixInv = new Matrix4f();
    private Vector4f viewPort = new Vector4f();
    private Vector2f resolution = new Vector2f();
    private Vector2f resolutionInv = new Vector2f();
    private Vector2f nearFar = new Vector2f();

    /**
     * Internal use only.
     * Updates the given list of uniforms with ShaderUniformBinding uniform bindings}
 based on the current world state.
     */
    public void updateUniformBindings(List<ShaderUniform> params) {
        for (int i = 0; i < params.size(); i++) {
            ShaderUniform u = params.get(i);
            switch (u.getBinding()) {
                case WorldMatrix:
                    u.setValue(VariableType.Matrix4, worldMatrix);
                    break;
                case ViewMatrix:
                    u.setValue(VariableType.Matrix4, viewMatrix);
                    break;
                case ProjectionMatrix:
                    u.setValue(VariableType.Matrix4, projMatrix);
                    break;
                case ViewProjectionMatrix:
                    u.setValue(VariableType.Matrix4, viewProjMatrix);
                    break;
                case WorldViewMatrix:
                    worldViewMatrix.set(viewMatrix);
                    worldViewMatrix.multLocal(worldMatrix);
                    u.setValue(VariableType.Matrix4, worldViewMatrix);
                    break;
                case NormalMatrix:
                    tempMatrix.set(viewMatrix);
                    tempMatrix.multLocal(worldMatrix);
                    tempMatrix.toRotationMatrix(normalMatrix);
                    normalMatrix.invertLocal();
                    normalMatrix.transposeLocal();
                    u.setValue(VariableType.Matrix3, normalMatrix);
                    break;
                case WorldViewProjectionMatrix:
                    worldViewProjMatrix.set(viewProjMatrix);
                    worldViewProjMatrix.multLocal(worldMatrix);
                    u.setValue(VariableType.Matrix4, worldViewProjMatrix);
                    break;
                case WorldMatrixInverse:
                    worldMatrixInv.set(worldMatrix);
                    worldMatrixInv.invertLocal();
                    u.setValue(VariableType.Matrix4, worldMatrixInv);
                    break;
                case WorldMatrixInverseTranspose:
                    worldMatrix.toRotationMatrix(worldMatrixInvTrsp);
                    worldMatrixInvTrsp.invertLocal().transposeLocal();
                    u.setValue(VariableType.Matrix3, worldMatrixInvTrsp);
                    break;
                case ViewMatrixInverse:
                    viewMatrixInv.set(viewMatrix);
                    viewMatrixInv.invertLocal();
                    u.setValue(VariableType.Matrix4, viewMatrixInv);
                    break;
                case ProjectionMatrixInverse:
                    projMatrixInv.set(projMatrix);
                    projMatrixInv.invertLocal();
                    u.setValue(VariableType.Matrix4, projMatrixInv);
                    break;
                case ViewProjectionMatrixInverse:
                    viewProjMatrixInv.set(viewProjMatrix);
                    viewProjMatrixInv.invertLocal();
                    u.setValue(VariableType.Matrix4, viewProjMatrixInv);
                    break;
                case WorldViewMatrixInverse:
                    worldViewMatrixInv.set(viewMatrix);
                    worldViewMatrixInv.multLocal(worldMatrix);
                    worldViewMatrixInv.invertLocal();
                    u.setValue(VariableType.Matrix4, worldViewMatrixInv);
                    break;
                case NormalMatrixInverse:
                    tempMatrix.set(viewMatrix);
                    tempMatrix.multLocal(worldMatrix);
                    tempMatrix.toRotationMatrix(normalMatrixInv);
                    normalMatrixInv.invertLocal();
                    normalMatrixInv.transposeLocal();
                    normalMatrixInv.invertLocal();
                    u.setValue(VariableType.Matrix3, normalMatrixInv);
                    break;
                case WorldViewProjectionMatrixInverse:
                    worldViewProjMatrixInv.set(viewProjMatrix);
                    worldViewProjMatrixInv.multLocal(worldMatrix);
                    worldViewProjMatrixInv.invertLocal();
                    u.setValue(VariableType.Matrix4, worldViewProjMatrixInv);
                    break;
                case ViewPort:
                    viewPort.set(viewX, viewY, viewWidth, viewHeight);
                    u.setValue(VariableType.Vector4, viewPort);
                    break;
                case Resolution:
                    resolution.set(viewWidth, viewHeight);
                    u.setValue(VariableType.Vector2, resolution);
                    break;
                case ResolutionInverse:
                    resolutionInv.set(1f / viewWidth, 1f / viewHeight);
                    u.setValue(VariableType.Vector2, resolutionInv);
                    break;
                case Aspect:
                    float aspect = ((float) viewWidth) / viewHeight;
                    u.setValue(VariableType.Float, aspect);
                    break;
                case FrustumNearFar:
                    nearFar.set(near, far);
                    u.setValue(VariableType.Vector2, nearFar);
                    break;
                case CameraPosition:
                    u.setValue(VariableType.Vector3, camLoc);
                    break;
                case CameraDirection:
                    u.setValue(VariableType.Vector3, camDir);
                    break;
                case CameraLeft:
                    u.setValue(VariableType.Vector3, camLeft);
                    break;
                case CameraUp:
                    u.setValue(VariableType.Vector3, camUp);
                    break;
                case Time:
                    u.setValue(VariableType.Float, timer.getTimeInSeconds());
                    break;
                case Tpf:
                    u.setValue(VariableType.Float, timer.getTimePerFrame());
                    break;
                case FrameRate:
                    u.setValue(VariableType.Float, timer.getFrameRate());
                    break;
            }
        }
    }

    /**
     * Internal use only. Sets the world matrix to use for future
 rendering. This has no effect unless objects are rendered manually
 using Material#render(com.clockwork.scene.Geometry, com.clockwork.renderer.RenderingManager) }.
 Using #renderGeometry(com.clockwork.scene.Geometry) } will 
 override this value.
     * 
     * @param mat The world matrix to set
     */
    public void setWorldMatrix(Matrix4f mat) {
        worldMatrix.set(mat);
    }

    /**
     * Set the timer that should be used to query the time based
 ShaderUniformBinding}s for material world parameters.
     * 
     * @param timer The timer to query time world parameters
     */
    public void setTimer(com.clockwork.system.Timer timer) {
        this.timer = timer;
    }

    public void setCamera(Camera cam, Matrix4f viewMatrix, Matrix4f projMatrix, Matrix4f viewProjMatrix) {
        this.viewMatrix.set(viewMatrix);
        this.projMatrix.set(projMatrix);
        this.viewProjMatrix.set(viewProjMatrix);

        camLoc.set(cam.getLocation());
        cam.getLeft(camLeft);
        cam.getUp(camUp);
        cam.getDirection(camDir);

        near = cam.getFrustumNear();
        far = cam.getFrustumFar();
    }

    public void setViewPort(int viewX, int viewY, int viewWidth, int viewHeight) {
        this.viewX = viewX;
        this.viewY = viewY;
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
    }
}
