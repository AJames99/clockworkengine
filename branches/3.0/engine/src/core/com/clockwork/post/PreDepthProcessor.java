
package com.clockwork.post;

import com.clockwork.material.Material;
import com.clockwork.material.RenderState;
import com.clockwork.material.RenderState.FaceCullMode;
import com.clockwork.renderer.RenderingManager;
import com.clockwork.renderer.Viewport;
import com.clockwork.renderer.queue.RenderOrder;
import com.clockwork.texture.FrameBuffer;
import com.clockwork.assets.AssetHandler;

/**
 * Processor that lays depth first, this can improve performance in complex
 * scenes.
 */
public class PreDepthProcessor implements SceneProcessor {

    private RenderingManager rm;
    private Viewport vp;
    private AssetHandler assetHandler;
    private Material preDepth;
    private RenderState forcedRS;

    public PreDepthProcessor(AssetHandler assetHandler){
        this.assetHandler = assetHandler;
        preDepth = new Material(assetHandler, "Common/MatDefs/Shadow/PreShadow.mdef");
        preDepth.getAdditionalRenderState().setPolyOffset(0, 0);
        preDepth.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Back);

        forcedRS = new RenderState();
        forcedRS.setDepthTest(true);
        forcedRS.setDepthWrite(false);
    }

    public void initialise(RenderingManager rm, Viewport vp) {
        this.rm = rm;
        this.vp = vp;
    }

    public void reshape(Viewport vp, int w, int h) {
        this.vp = vp;
    }

    public boolean isinitialised() {
        return vp != null;
    }

    public void preFrame(float tpf) {
    }

    public void postQueue(RenderOrder rq) {
        // lay depth first
        rm.setForcedMaterial(preDepth);
        rq.renderQueue(RenderOrder.Bucket.Opaque, rm, vp.getCamera(), false);
        rm.setForcedMaterial(null);

        rm.setForcedRenderState(forcedRS);
    }

    public void postFrame(FrameBuffer out) {
        rm.setForcedRenderState(null);
    }

    public void cleanup() {
        vp = null;
    }

}
