
package com.clockwork.app.state;

import com.clockwork.app.Application;
import com.clockwork.renderer.RenderingManager;

/**
 * AbstractAppState implements some common methods
 * that make creation of AppStates easier.
 */
public class AbstractAppState implements AppState {

    /**
     * initialised is set to true when the method
     * AbstractAppState initialise(com.clockwork.app.state.AppStateManager, com.clockwork.app.Application)
     * is called. When AbstractAppState#cleanup()is called, initialised
     * is set back to false.
     */
    protected boolean initialised = false;
    private boolean enabled = true;

    public void initialise(AppStateManager stateManager, Application app) {
        initialised = true;
    }

    public boolean isinitialised() {
        return initialised;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
    public boolean isEnabled() {
        return enabled;
    }

    public void stateAttached(AppStateManager stateManager) {
    }

    public void stateDetached(AppStateManager stateManager) {
    }

    public void update(float tpf) {
    }

    public void render(RenderingManager rm) {
    }

    public void postRender(){
    }

    public void cleanup() {
        initialised = false;
    }

}
