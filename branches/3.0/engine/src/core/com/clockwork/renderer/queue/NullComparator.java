
package com.clockwork.renderer.queue;

import com.clockwork.renderer.Camera;
import com.clockwork.scene.GeometrySpatial;

/**
 * NullComparator does not sort geometries. They will be in
 * arbitrary order.
 * 
 */
public class NullComparator implements GeometryComparator {
    public int compare(GeometrySpatial o1, GeometrySpatial o2) {
        return 0;
    }

    public void setCamera(Camera cam) {
    }
}
