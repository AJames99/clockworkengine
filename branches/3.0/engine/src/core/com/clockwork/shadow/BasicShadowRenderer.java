
package com.clockwork.shadow;

import com.clockwork.material.Material;
import com.clockwork.math.Vector3f;
import com.clockwork.post.SceneProcessor;
import com.clockwork.renderer.Camera;
import com.clockwork.renderer.RenderingManager;
import com.clockwork.renderer.Renderer;
import com.clockwork.renderer.Viewport;
import com.clockwork.renderer.queue.GeometryList;
import com.clockwork.renderer.queue.RenderOrder;
import com.clockwork.renderer.queue.RenderOrder.ShadowMode;
import com.clockwork.texture.FrameBuffer;
import com.clockwork.texture.Image.Format;
import com.clockwork.texture.Texture2;
import com.clockwork.ui.Picture;
import com.clockwork.assets.AssetHandler;

/**
 * BasicShadowRenderer uses standard shadow mapping with one map
 * it's useful to render shadows in a small scene, but edges might look a bit jagged.
 * 
 * 
 * @deprecated use DirectionalLightShadowRenderer} with one split.
 */
@Deprecated
public class BasicShadowRenderer implements SceneProcessor {

    private RenderingManager renderManager;
    private Viewport viewPort;
    private FrameBuffer shadowFB;
    private Texture2 shadowMap;
    private Camera shadowCam;
    private Material preshadowMat;
    private Material postshadowMat;
    private Picture dispPic = new Picture("Picture");
    private boolean noOccluders = false;
    private Vector3f[] points = new Vector3f[8];
    private Vector3f direction = new Vector3f();
    protected Texture2 dummyTex;
    private float shadowMapSize;

    /**
     * Creates a BasicShadowRenderer
     * @param manager the asset manager
     * @param size the size of the shadow map (the map is square)
     */
    public BasicShadowRenderer(AssetHandler manager, int size) {
        shadowFB = new FrameBuffer(size, size, 1);
        shadowMap = new Texture2(size, size, Format.Depth);
        shadowFB.setDepthTexture(shadowMap);
        shadowCam = new Camera(size, size);
        
         //DO NOT COMMENT THIS (it prevent the OSX incomplete read buffer crash)
        dummyTex = new Texture2(size, size, Format.RGBA8);        
        shadowFB.setColorTexture(dummyTex);
        shadowMapSize = (float)size;
        preshadowMat = new Material(manager, "Common/MatDefs/Shadow/PreShadow.mdef");
        postshadowMat = new Material(manager, "Common/MatDefs/Shadow/BasicPostShadow.mdef");
        postshadowMat.setTexture("ShadowMap", shadowMap);

        dispPic.setTexture(manager, shadowMap, false);

        for (int i = 0; i < points.length; i++) {
            points[i] = new Vector3f();
        }
    }

    public void initialise(RenderingManager rm, Viewport vp) {
        renderManager = rm;
        viewPort = vp;

        reshape(vp, vp.getCamera().getWidth(), vp.getCamera().getHeight());
    }

    public boolean isinitialised() {
        return viewPort != null;
    }

    /**
     * returns the light direction used for this processor
     * @return 
     */
    public Vector3f getDirection() {
        return direction;
    }

    /**
     * sets the light direction to use to computs shadows
     * @param direction 
     */
    public void setDirection(Vector3f direction) {
        this.direction.set(direction).normalizeLocal();
    }

    /**
     * debug only
     * @return 
     */
    public Vector3f[] getPoints() {
        return points;
    }

    /**
     * debug only
     * returns the shadow camera 
     * @return 
     */
    public Camera getShadowCamera() {
        return shadowCam;
    }

    public void postQueue(RenderOrder rq) {
        GeometryList occluders = rq.getShadowQueueContent(ShadowMode.Cast);
        if (occluders.size() == 0) {
            noOccluders = true;
            return;
        } else {
            noOccluders = false;
        }

        GeometryList receivers = rq.getShadowQueueContent(ShadowMode.Receive);

        // update frustum points based on current camera
        Camera viewCam = viewPort.getCamera();
        ShadowUtilities.updateFrustumPoints(viewCam,
                viewCam.getFrustumNear(),
                viewCam.getFrustumFar(),
                1.0f,
                points);

        Vector3f frustaCenter = new Vector3f();
        for (Vector3f point : points) {
            frustaCenter.addLocal(point);
        }
        frustaCenter.multLocal(1f / 8f);

        // update light direction
        shadowCam.setProjectionMatrix(null);
        shadowCam.setParallelProjection(true);
//        shadowCam.setFrustumPerspective(45, 1, 1, 20);

        shadowCam.lookAtDirection(direction, Vector3f.UNIT_Y);
        shadowCam.update();
        shadowCam.setLocation(frustaCenter);
        shadowCam.update();
        shadowCam.updateViewProjection();

        // render shadow casters to shadow map
        ShadowUtilities.updateShadowCamera(occluders, receivers, shadowCam, points, shadowMapSize);

        Renderer r = renderManager.getRenderer();
        renderManager.setCamera(shadowCam, false);
        renderManager.setForcedMaterial(preshadowMat);

        r.setFrameBuffer(shadowFB);
        r.clearBuffers(false, true, false);
        viewPort.getQueue().renderShadowQueue(ShadowMode.Cast, renderManager, shadowCam, true);
        r.setFrameBuffer(viewPort.getOutputFrameBuffer());

        renderManager.setForcedMaterial(null);
        renderManager.setCamera(viewCam, false);
    }

    /**
     * debug only
     * @return 
     */
    public Picture getDisplayPicture() {
        return dispPic;
    }

    public void postFrame(FrameBuffer out) {
        if (!noOccluders) {
            postshadowMat.setMatrix4("LightViewProjectionMatrix", shadowCam.getViewProjectionMatrix());
            renderManager.setForcedMaterial(postshadowMat);
            viewPort.getQueue().renderShadowQueue(ShadowMode.Receive, renderManager, viewPort.getCamera(), true);
            renderManager.setForcedMaterial(null);
        }
    }

    public void preFrame(float tpf) {
    }

    public void cleanup() {
    }

    public void reshape(Viewport vp, int w, int h) {
        dispPic.setPosition(w / 20f, h / 20f);
        dispPic.setWidth(w / 5f);
        dispPic.setHeight(h / 5f);
    }
}
