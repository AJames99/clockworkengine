
package com.clockwork.water;

import com.clockwork.math.Plane;
import com.clockwork.post.SceneProcessor;
import com.clockwork.renderer.Camera;
import com.clockwork.renderer.RenderingManager;
import com.clockwork.renderer.Viewport;
import com.clockwork.renderer.queue.RenderOrder;
import com.clockwork.texture.FrameBuffer;

/**
 * Reflection Processor
 * Used to render the reflected scene in an off view port
 */
public class ReflectionProcessor implements SceneProcessor {

    private RenderingManager rm;
    private Viewport vp;
    private Camera reflectionCam;
    private FrameBuffer reflectionBuffer;
    private Plane reflectionClipPlane;

    /**
     * Creates a ReflectionProcessor
     * @param reflectionCam the cam to use for reflection
     * @param reflectionBuffer the FrameBuffer to render to
     * @param reflectionClipPlane the clipping plane
     */
    public ReflectionProcessor(Camera reflectionCam, FrameBuffer reflectionBuffer, Plane reflectionClipPlane) {
        this.reflectionCam = reflectionCam;
        this.reflectionBuffer = reflectionBuffer;
        this.reflectionClipPlane = reflectionClipPlane;
    }

    public void initialise(RenderingManager rm, Viewport vp) {
        this.rm = rm;
        this.vp = vp;
    }

    public void reshape(Viewport vp, int w, int h) {
    }

    public boolean isinitialised() {
        return rm != null;
    }

    public void preFrame(float tpf) {
    }

    public void postQueue(RenderOrder rq) {
        //we need special treatement for the sky because it must not be clipped
        rm.getRenderer().setFrameBuffer(reflectionBuffer);
        reflectionCam.setProjectionMatrix(null);
        rm.setCamera(reflectionCam, false);
        rm.getRenderer().clearBuffers(true, true, true);
        //Rendering the sky whithout clipping
        rm.getRenderer().setDepthRange(1, 1);
        vp.getQueue().renderQueue(RenderOrder.Bucket.Sky, rm, reflectionCam, true);
        rm.getRenderer().setDepthRange(0, 1);
        //setting the clip plane to the cam
        reflectionCam.setClipPlane(reflectionClipPlane, Plane.Side.Positive);//,1
        rm.setCamera(reflectionCam, false);

    }

    public void postFrame(FrameBuffer out) {
    }

    public void cleanup() {
    }

    /**
     * Internal use only
     * returns the frame buffer
     * @return 
     */
    public FrameBuffer getReflectionBuffer() {
        return reflectionBuffer;
    }

    /**
     * Internal use only
     * sets the frame buffer
     * @param reflectionBuffer 
     */
    public void setReflectionBuffer(FrameBuffer reflectionBuffer) {
        this.reflectionBuffer = reflectionBuffer;
    }

    /**
     * returns the reflection cam
     * @return 
     */
    public Camera getReflectionCam() {
        return reflectionCam;
    }

    /**
     * sets the reflection cam
     * @param reflectionCam 
     */
    public void setReflectionCam(Camera reflectionCam) {
        this.reflectionCam = reflectionCam;
    }

    /**
     * returns the reflection clip plane
     * @return 
     */
    public Plane getReflectionClipPlane() {
        return reflectionClipPlane;
    }

    /**
     * Sets the reflection clip plane
     * @param reflectionClipPlane 
     */
    public void setReflectionClipPlane(Plane reflectionClipPlane) {
        this.reflectionClipPlane = reflectionClipPlane;
    }
}
