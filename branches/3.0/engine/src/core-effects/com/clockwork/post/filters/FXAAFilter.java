
package com.clockwork.post.filters;

import com.clockwork.material.Material;
import com.clockwork.post.Filter;
import com.clockwork.renderer.RenderingManager;
import com.clockwork.renderer.Viewport;
import com.clockwork.assets.AssetHandler;

/**
 * opengl</span>-test-radeon-geforce/3/
 * http://developer.download.nvidia.com/assets/gamedev/files/sdk/11/FXAA_WhitePaper.pdf
 *
 *
 */
public class FXAAFilter extends Filter {

    private float subPixelShift = 1.0f / 4.0f;
    private float vxOffset = 0.0f;
    private float spanMax = 8.0f;
    private float reduceMul = 1.0f / 8.0f;

    public FXAAFilter() {
        super("FXAAFilter");
    }

    @Override
    protected void initFilter(AssetHandler manager,
            RenderingManager renderManager, Viewport vp, int w, int h) {
        material = new Material(manager, "Common/MatDefs/Post/FXAA.mdef");   
        material.setFloat("SubPixelShift", subPixelShift);
        material.setFloat("VxOffset", vxOffset);
        material.setFloat("SpanMax", spanMax);
        material.setFloat("ReduceMul", reduceMul);
    }

    @Override
    protected Material getMaterial() {
        return material;
    }

    public void setSpanMax(float spanMax) {
        this.spanMax = spanMax;
        if (material != null) {
            material.setFloat("SpanMax", this.spanMax);
        }
    }

    /**
     * set to 0.0f for higher quality
     *
     * @param subPixelShift
     */
    public void setSubPixelShift(float subPixelShift) {
        this.subPixelShift = subPixelShift;
        if (material != null) {
            material.setFloat("SubPixelShif", this.subPixelShift);
        }
    }

    /**
     * set to 0.0f for higher quality
     *
     * @param reduceMul
     */
    public void setReduceMul(float reduceMul) {
        this.reduceMul = reduceMul;
        if (material != null) {
            material.setFloat("ReduceMul", this.reduceMul);
        }
    }

    public void setVxOffset(float vxOffset) {
        this.vxOffset = vxOffset;
        if (material != null) {
            material.setFloat("VxOffset", this.vxOffset);
        }
    }

    public float getReduceMul() {
        return reduceMul;
    }

    public float getSpanMax() {
        return spanMax;
    }

    public float getSubPixelShift() {
        return subPixelShift;
    }

    public float getVxOffset() {
        return vxOffset;
    }
}