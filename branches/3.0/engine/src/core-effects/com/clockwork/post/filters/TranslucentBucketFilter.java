
package com.clockwork.post.filters;

import com.clockwork.effect.ParticleEmitter;
import com.clockwork.material.Material;
import com.clockwork.math.ColorRGBA;
import com.clockwork.post.Filter;
import com.clockwork.renderer.RenderingManager;
import com.clockwork.renderer.Renderer;
import com.clockwork.renderer.Viewport;
import com.clockwork.renderer.queue.RenderOrder;
import com.clockwork.scene.SceneNode;
import com.clockwork.scene.Spatial;
import com.clockwork.texture.FrameBuffer;
import com.clockwork.texture.Texture;
import com.clockwork.texture.Texture2;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.clockwork.assets.AssetHandler;

/**
 * This filter handles translucent objects when rendering a scene with filters that use depth like WaterFilter and SSAOFilter
 * To use, instantiate a TranslucentBucketFilter and add it to the Filter list of a FilterPostProcessor
 */
public final class TranslucentBucketFilter extends Filter {

    private final static Logger logger = Logger.getLogger(TranslucentBucketFilter.class.getName());
    private RenderingManager renderManager;
    private boolean enabledSoftParticles = false;
    private Texture depthTexture;
    private Viewport viewPort;

    public TranslucentBucketFilter() {
        super("TranslucentBucketFilter");
    }

    public TranslucentBucketFilter(boolean enabledSoftParticles) {
        this();
        this.enabledSoftParticles = enabledSoftParticles;
    }

    @Override
    protected void initFilter(AssetHandler manager, RenderingManager rm, Viewport vp, int w, int h) {
        this.renderManager = rm;
        this.viewPort = vp;
        material = new Material(manager, "Common/MatDefs/Post/Overlay.mdef");
        material.setColor("Color", ColorRGBA.White);
        Texture2 tex = processor.getFilterTexture();
        material.setTexture("Texture", tex);
        if (tex.getImage().getMultiSamples() > 1) {
            material.setInt("NumSamples", tex.getImage().getMultiSamples());
        } else {
            material.clearParam("NumSamples");
        }
        renderManager.setHandleTranslucentBucket(false);
        if (enabledSoftParticles && depthTexture != null) {
            initSoftParticles(vp, true);
        }
    }

    private void initSoftParticles(Viewport vp, boolean enabledSP) {
        if (depthTexture != null) {
            for (Spatial scene : vp.getScenes()) {
                makeSoftParticleEmitter(scene, enabledSP && enabled);
            }
        }

    }

    @Override
    protected void setDepthTexture(Texture depthTexture) {
        this.depthTexture = depthTexture;
        if (enabledSoftParticles && depthTexture != null) {
            initSoftParticles(viewPort, true);
        }
    }

    /**
     * Override this method and return false if your Filter does not need the scene texture
     * @return
     */
    @Override
    protected boolean isRequiresSceneTexture() {
        return false;
    }

    @Override
    protected boolean isRequiresDepthTexture() {
        return enabledSoftParticles;
    }

    @Override
    protected void postFrame(RenderingManager renderManager, Viewport viewPort, FrameBuffer prevFilterBuffer, FrameBuffer sceneBuffer) {
        renderManager.setCamera(viewPort.getCamera(), false);
        if (prevFilterBuffer != sceneBuffer) {
            renderManager.getRenderer().copyFrameBuffer(prevFilterBuffer, sceneBuffer, false);
        }
        renderManager.getRenderer().setFrameBuffer(sceneBuffer);
        viewPort.getQueue().renderQueue(RenderOrder.Bucket.Translucent, renderManager, viewPort.getCamera());
    }

    @Override
    protected void cleanUpFilter(Renderer r) {
        if (renderManager != null) {
            renderManager.setHandleTranslucentBucket(true);
        }

        initSoftParticles(viewPort, false);
    }

    @Override
    protected Material getMaterial() {
        return material;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (renderManager != null) {
            renderManager.setHandleTranslucentBucket(!enabled);
        }
        initSoftParticles(viewPort, enabledSoftParticles);
    }

    private void makeSoftParticleEmitter(Spatial scene, boolean enabled) {
        if (scene instanceof SceneNode) {
            SceneNode n = (SceneNode) scene;
            for (Spatial child : n.getChildren()) {
                makeSoftParticleEmitter(child, enabled);
            }
        }
        if (scene instanceof ParticleEmitter) {
            ParticleEmitter emitter = (ParticleEmitter) scene;
            if (enabled) {
                enabledSoftParticles = enabled;

                if( processor.getNumSamples()>1){
                    emitter.getMaterial().selectTechnique("SoftParticles15", renderManager);
                    emitter.getMaterial().setInt("NumSamplesDepth", processor.getNumSamples());
                }else{
                    emitter.getMaterial().selectTechnique("SoftParticles", renderManager);
                }
                emitter.getMaterial().setTexture("DepthTexture", processor.getDepthTexture());               
                emitter.setQueueBucket(RenderOrder.Bucket.Translucent);

                logger.log(Level.FINE, "Made particle Emitter {0} soft.", emitter.getName());
            } else {
                emitter.getMaterial().clearParam("DepthTexture");
                emitter.getMaterial().selectTechnique("Default", renderManager);
               // emitter.setQueueBucket(RenderOrder.Bucket.Transparent);
                logger.log(Level.FINE, "Particle Emitter {0} is not soft anymore.", emitter.getName());
            }
        }
    }
}
