
package com.clockwork.bullet.debug;

import com.clockwork.bullet.objects.PhysicsVehicle;
import com.clockwork.bullet.objects.VehicleWheel;
import com.clockwork.math.Quaternion;
import com.clockwork.math.Vector3f;
import com.clockwork.renderer.RenderingManager;
import com.clockwork.renderer.Viewport;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.SceneNode;
import com.clockwork.scene.Spatial;
import com.clockwork.scene.debug.Arrow;

/**
 *
 */
public class BulletVehicleDebugControl extends AbstractPhysicsDebugControl {

    protected final PhysicsVehicle body;
    protected final SceneNode suspensionNode;
    protected final Vector3f location = new Vector3f();
    protected final Quaternion rotation = new Quaternion();

    public BulletVehicleDebugControl(BulletDebugAppState debugAppState, PhysicsVehicle body) {
        super(debugAppState);
        this.body = body;
        suspensionNode = new SceneNode("Suspension");
        createVehicle();
    }

    @Override
    public void setSpatial(Spatial spatial) {
        if (spatial != null && spatial instanceof SceneNode) {
            SceneNode node = (SceneNode) spatial;
            node.attachChild(suspensionNode);
        } else if (spatial == null && this.spatial != null) {
            SceneNode node = (SceneNode) this.spatial;
            node.detachChild(suspensionNode);
        }
        super.setSpatial(spatial);
    }

    private void createVehicle() {
        suspensionNode.detachAllChildren();
        for (int i = 0; i < body.getNumWheels(); i++) {
            VehicleWheel physicsVehicleWheel = body.getWheel(i);
            Vector3f location = physicsVehicleWheel.getLocation().clone();
            Vector3f direction = physicsVehicleWheel.getDirection().clone();
            Vector3f axle = physicsVehicleWheel.getAxle().clone();
            float restLength = physicsVehicleWheel.getRestLength();
            float radius = physicsVehicleWheel.getRadius();

            Arrow locArrow = new Arrow(location);
            Arrow axleArrow = new Arrow(axle.normalizeLocal().multLocal(0.3f));
            Arrow wheelArrow = new Arrow(direction.normalizeLocal().multLocal(radius));
            Arrow dirArrow = new Arrow(direction.normalizeLocal().multLocal(restLength));
            GeometrySpatial locGeom = new GeometrySpatial("WheelLocationDebugShape" + i, locArrow);
            GeometrySpatial dirGeom = new GeometrySpatial("WheelDirectionDebugShape" + i, dirArrow);
            GeometrySpatial axleGeom = new GeometrySpatial("WheelAxleDebugShape" + i, axleArrow);
            GeometrySpatial wheelGeom = new GeometrySpatial("WheelRadiusDebugShape" + i, wheelArrow);
            dirGeom.setLocalTranslation(location);
            axleGeom.setLocalTranslation(location.add(direction));
            wheelGeom.setLocalTranslation(location.add(direction));
            locGeom.setMaterial(debugAppState.DEBUG_MAGENTA);
            dirGeom.setMaterial(debugAppState.DEBUG_MAGENTA);
            axleGeom.setMaterial(debugAppState.DEBUG_MAGENTA);
            wheelGeom.setMaterial(debugAppState.DEBUG_MAGENTA);
            suspensionNode.attachChild(locGeom);
            suspensionNode.attachChild(dirGeom);
            suspensionNode.attachChild(axleGeom);
            suspensionNode.attachChild(wheelGeom);
        }
    }

    @Override
    protected void controlUpdate(float tpf) {
        for (int i = 0; i < body.getNumWheels(); i++) {
            VehicleWheel physicsVehicleWheel = body.getWheel(i);
            Vector3f location = physicsVehicleWheel.getLocation().clone();
            Vector3f direction = physicsVehicleWheel.getDirection().clone();
            Vector3f axle = physicsVehicleWheel.getAxle().clone();
            float restLength = physicsVehicleWheel.getRestLength();
            float radius = physicsVehicleWheel.getRadius();

            GeometrySpatial locGeom = (GeometrySpatial) suspensionNode.getChild("WheelLocationDebugShape" + i);
            GeometrySpatial dirGeom = (GeometrySpatial) suspensionNode.getChild("WheelDirectionDebugShape" + i);
            GeometrySpatial axleGeom = (GeometrySpatial) suspensionNode.getChild("WheelAxleDebugShape" + i);
            GeometrySpatial wheelGeom = (GeometrySpatial) suspensionNode.getChild("WheelRadiusDebugShape" + i);

            Arrow locArrow = (Arrow) locGeom.getMesh();
            locArrow.setArrowExtent(location);
            Arrow axleArrow = (Arrow) axleGeom.getMesh();
            axleArrow.setArrowExtent(axle.normalizeLocal().multLocal(0.3f));
            Arrow wheelArrow = (Arrow) wheelGeom.getMesh();
            wheelArrow.setArrowExtent(direction.normalizeLocal().multLocal(radius));
            Arrow dirArrow = (Arrow) dirGeom.getMesh();
            dirArrow.setArrowExtent(direction.normalizeLocal().multLocal(restLength));

            dirGeom.setLocalTranslation(location);
            axleGeom.setLocalTranslation(location.addLocal(direction));
            wheelGeom.setLocalTranslation(location);
            i++;
        }
        applyPhysicsTransform(body.getPhysicsLocation(location), body.getPhysicsRotation(rotation));
    }

    @Override
    protected void controlRender(RenderingManager rm, Viewport vp) {
    }
}
