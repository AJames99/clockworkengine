
package com.clockwork.bullet.collision;

import com.clockwork.animation.Joint;

/**
 *
 */
public interface RagdollCollisionListener {
    
    public void collide(Joint bone, PhysicsCollisionObject object, PhysicsCollisionEvent event);
    
}
