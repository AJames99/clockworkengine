
package com.clockwork.niftygui;

import com.clockwork.audio.AudioNode;
import com.clockwork.audio.AudioRenderer;
import de.lessvoid.nifty.sound.SoundSystem;
import de.lessvoid.nifty.spi.sound.SoundDevice;
import de.lessvoid.nifty.spi.sound.SoundHandle;
import de.lessvoid.nifty.tools.resourceloader.NiftyResourceLoader;
import com.clockwork.assets.AssetHandler;

public class SoundDeviceCW implements SoundDevice {

    protected AssetHandler assetHandler;
    protected AudioRenderer ar;

    public SoundDeviceCW(AssetHandler assetHandler, AudioRenderer ar){
        this.assetHandler = assetHandler;
        this.ar = ar;
    }

    public void setResourceLoader(NiftyResourceLoader niftyResourceLoader) {
    }

    public SoundHandle loadSound(SoundSystem soundSystem, String filename) {
        AudioNode an = new AudioNode(assetHandler, filename, false);
        an.setPositional(false);
        return new SoundHandleCW(ar, an);
    }

    public SoundHandle loadMusic(SoundSystem soundSystem, String filename) {
        return new SoundHandleCW(ar, assetHandler, filename);
    }

    public void update(int delta) {
    }
    
}
