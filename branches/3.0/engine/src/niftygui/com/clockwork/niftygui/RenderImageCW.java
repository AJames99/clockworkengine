
package com.clockwork.niftygui;

import com.clockwork.assets.TextureKey;
import com.clockwork.texture.Image;
import com.clockwork.texture.Texture.MagFilter;
import com.clockwork.texture.Texture.MinFilter;
import com.clockwork.texture.Texture2;
import de.lessvoid.nifty.spi.render.RenderImage;

public class RenderImageCW implements RenderImage {

    private Texture2 texture;
    private Image image;
    private int width;
    private int height;

    public RenderImageCW(String filename, boolean linear, NiftyCWDisplay display){
        TextureKey key = new TextureKey(filename, true);

        key.setAnisotropy(0);
        key.setAsCube(false);
        key.setGenerateMips(false);
        
        texture = (Texture2) display.getAssetHandler().loadTexture(key);
        texture.setMagFilter(linear ? MagFilter.Bilinear : MagFilter.Nearest);
        texture.setMinFilter(linear ? MinFilter.BilinearNoMipMaps : MinFilter.NearestNoMipMaps);
        image = texture.getImage();

        width = image.getWidth();
        height = image.getHeight();
    }

    public RenderImageCW(Texture2 texture){
        if (texture.getImage() == null) {
            throw new IllegalArgumentException("texture.getImage() cannot be null");
        }
        
        this.texture = texture;
        this.image = texture.getImage();
        width = image.getWidth();
        height = image.getHeight();
    }

    public Texture2 getTexture(){
        return texture;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void dispose() {
    }
}
