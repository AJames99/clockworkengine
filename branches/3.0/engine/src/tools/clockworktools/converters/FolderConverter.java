

package clockworktools.converters;

import com.clockwork.system.EngineSystem;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import com.clockwork.assets.AssetHandler;

public class FolderConverter {

    private static AssetHandler assetHandler;
    private static File sourceRoot;
    private static JarOutputStream jarOut;
    private static long time;

    private static void process(File file) throws IOException{
        String name = file.getName().replaceAll("[\\/\\.]", "_");
        JarEntry entry = new JarEntry(name);
        entry.setTime(time);

        jarOut.putNextEntry(entry);
    }

    public static void main(String[] args) throws IOException{
        if (args.length == 0){
            System.out.println("Usage: java -jar FolderConverter <input folder>");
            System.out.println();
            System.out.println("  Converts files from input to output");
            System.exit(1);
        }

        sourceRoot = new File(args[0]);
        
        File jarFile = new File(sourceRoot.getParent(), sourceRoot.getName()+".jar");
        FileOutputStream out = new FileOutputStream(jarFile);
        jarOut = new JarOutputStream(out);

        assetHandler = EngineSystem.newAssetHandler();
        assetHandler.registerLocator(sourceRoot.toString(), 
                                     "com.clockwork.assets.plugins.FileSystemLocator");
        for (File f : sourceRoot.listFiles()){
             process(f);
        }
    }

}
