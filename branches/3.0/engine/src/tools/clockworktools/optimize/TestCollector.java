

package clockworktools.optimize;

import com.clockwork.math.Vector3f;
import com.clockwork.scene.GeometrySpatial;
import com.clockwork.scene.shape.Quad;
import java.util.ArrayList;
import java.util.List;

public class TestCollector {

    public static void main(String[] args){
        Vector3f z = Vector3f.ZERO;
        GeometrySpatial g  = new GeometrySpatial("quad", new Quad(2,2));
        GeometrySpatial g2 = new GeometrySpatial("quad", new Quad(2,2));
        List<OCTTriangle> tris = new ArrayList<OCTTriangle>();
        tris.add(new OCTTriangle(z, z, z, 1, 0));
        tris.add(new OCTTriangle(z, z, z, 0, 1));
        List<GeometrySpatial> firstOne = TriangleCollector.gatherTris(new GeometrySpatial[]{ g, g2 }, tris);
        System.out.println(firstOne.get(0).getMesh());
    }

}
