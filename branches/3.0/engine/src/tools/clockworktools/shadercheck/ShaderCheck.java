package clockworktools.shadercheck;

import com.clockwork.assets.plugins.ClasspathLocator;
import com.clockwork.assets.plugins.FileLocator;
import com.clockwork.material.MaterialDef;
import com.clockwork.material.TechniqueDef;
import com.clockwork.material.plugins.MATLoader;
import com.clockwork.shader.DefineList;
import com.clockwork.shader.Shader;
import com.clockwork.shader.ShaderKey;
import com.clockwork.shader.plugins.GLSLLoader;
import com.clockwork.system.EngineSystem;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.clockwork.assets.AssetHandler;

public class ShaderCheck {
    
    private static final Logger logger = Logger.getLogger(ShaderCheck.class.getName());
    private static AssetHandler assetHandler;
    
    private static Validator[] validators = new Validator[]{
        new CgcValidator(),
//        new GpuAnalyzerValidator()
    };
    
    private static void initAssetHandler(){
        assetHandler = EngineSystem.newAssetHandler();
        assetHandler.registerLocator(".", FileLocator.class);
        assetHandler.registerLocator("/", ClasspathLocator.class);
        assetHandler.registerLoader(MATLoader.class, "mat");
        assetHandler.registerLoader(MATLoader.class, "mdef");
        assetHandler.registerLoader(GLSLLoader.class, "vert", "frag", "glsllib");
    }
    
    private static void checkMatDef(String matdefName){
        MaterialDef def = (MaterialDef) assetHandler.loadAsset(matdefName);
        for (TechniqueDef techDef : def.getDefaultTechniques()){
            if (techDef.isUsingShaders()){
                DefineList dl = new DefineList();
                dl.addFrom(techDef.getShaderPresetDefines());
                ShaderKey shaderKey = new ShaderKey(techDef.getVertexShaderName(),
                                                    techDef.getFragmentShaderName(),
                                                    dl,
                                                    techDef.getVertexShaderLanguage(),
                                                    techDef.getFragmentShaderLanguage());
                Shader shader = assetHandler.loadShader(shaderKey);
                
                for (Validator validator : validators){
                    StringBuilder sb = new StringBuilder();
                    validator.validate(shader, sb);
                    System.out.println("==== Validator: " + validator.getName() + " " + 
                                            validator.getInstalledVersion() + " ====");
                    System.out.println(sb.toString());
                }
            }
        }
    }
          
    public static void main(String[] args){
        Logger.getLogger(MaterialDef.class.getName()).setLevel(Level.OFF);
        initAssetHandler();
        checkMatDef("Common/MatDefs/Blur/HGaussianBlur.mdef");
        checkMatDef("Common/MatDefs/Blur/RadialBlur.mdef");
        checkMatDef("Common/MatDefs/Blur/VGaussianBlur.mdef");
        checkMatDef("Common/MatDefs/Gui/Gui.mdef");
        checkMatDef("Common/MatDefs/Hdr/LogLum.mdef");
        checkMatDef("Common/MatDefs/Hdr/ToneMap.mdef");
        checkMatDef("Common/MatDefs/Light/Lighting.mdef");
        checkMatDef("Common/MatDefs/Misc/ColoredTextured.mdef");
        checkMatDef("Common/MatDefs/Misc/Particle.mdef");
        checkMatDef("Common/MatDefs/Misc/ShowNormals.mdef");
        checkMatDef("Common/MatDefs/Misc/Sky.mdef");
        checkMatDef("Common/MatDefs/Misc/Unshaded.mdef");
        
        checkMatDef("Common/MatDefs/Post/BloomExtract.mdef");
        checkMatDef("Common/MatDefs/Post/BloomFinal.mdef");
        checkMatDef("Common/MatDefs/Post/CartoonEdge.mdef");
        checkMatDef("Common/MatDefs/Post/CrossHatch.mdef");
        checkMatDef("Common/MatDefs/Post/DepthOfField.mdef");
        checkMatDef("Common/MatDefs/Post/FXAA.mdef");
        checkMatDef("Common/MatDefs/Post/Fade.mdef");
        checkMatDef("Common/MatDefs/Post/Fog.mdef");
        checkMatDef("Common/MatDefs/Post/GammaCorrection.mdef");
        checkMatDef("Common/MatDefs/Post/LightScattering.mdef");
        checkMatDef("Common/MatDefs/Post/Overlay.mdef");
        checkMatDef("Common/MatDefs/Post/Posterization.mdef");
        
        checkMatDef("Common/MatDefs/SSAO/ssao.mdef");
        checkMatDef("Common/MatDefs/SSAO/ssaoBlur.mdef");
        checkMatDef("Common/MatDefs/Shadow/PostShadow.mdef");
        checkMatDef("Common/MatDefs/Shadow/PostShadowPSSM.mdef");
        checkMatDef("Common/MatDefs/Shadow/PreShadow.mdef");
        
        checkMatDef("Common/MatDefs/Water/SimpleWater.mdef");
        checkMatDef("Common/MatDefs/Water/Water.mdef");
    }
}
