
package com.clockwork.system;

import java.awt.Canvas;

public interface CWCanvasContext extends EngineContext {
    public Canvas getCanvas();
}
